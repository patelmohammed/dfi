/*
 * Author:  Mohammed
 * Created: 21, 09, 2020
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'SMS Template', 'SMS_TEMPLATE', 'admin/Template/smsTemplate', NULL, 'fal fa-comment-alt-dots', '5', 'admin', 'Live');

CREATE TABLE `dfi`.`tbl_sms_template` ( `sms_template_id` INT NOT NULL AUTO_INCREMENT , `template_for` VARCHAR(250) NOT NULL , `message` TEXT NOT NULL , `is_active` INT NOT NULL DEFAULT '1' , `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live' , PRIMARY KEY (`sms_template_id`)) ENGINE = InnoDB;


/*
 * Author:  Mohammed
 * Created: 22, 09, 2020
 */

CREATE TABLE `dfi`.`tbl_voucher` ( `voucher_id` INT NOT NULL AUTO_INCREMENT , `transaction_id` VARCHAR(250) NOT NULL , `client_id` INT NOT NULL , `agreement_id` INT NULL DEFAULT NULL , `voucher_amount` DECIMAL(14,2) NOT NULL , `is_deleted` VARCHAR(50) NOT NULL DEFAULT 'Live' , PRIMARY KEY (`voucher_id`)) ENGINE = InnoDB;
ALTER TABLE `tbl_voucher` ADD `InsUser` INT NULL AFTER `voucher_amount`, ADD `InsTerminal` VARCHAR(250) NULL AFTER `InsUser`, ADD `InsDateTime` DATETIME NULL AFTER `InsTerminal`, ADD `UpdUser` INT NULL AFTER `InsDateTime`, ADD `UpdTerminal` VARCHAR(250) NULL AFTER `UpdUser`, ADD `UpdDateTime` DATETIME NULL AFTER `UpdTerminal`;

ALTER TABLE `tbl_voucher` CHANGE `is_deleted` `del_status` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Live';

CREATE TABLE `dfi`.`tbl_payment_method` ( `payment_id` INT NOT NULL AUTO_INCREMENT , `payment_name` VARCHAR(250) NOT NULL , `payment_desc` TEXT NOT NULL , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live' , PRIMARY KEY (`payment_id`)) ENGINE = InnoDB;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Payment Method', 'PAYMENT_METHOD', 'admin/Master/paymentMethod', '2', NULL, '3', 'admin', 'Live');
ALTER TABLE `tbl_payment_method` CHANGE `payment_desc` `payment_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_voucher`  ADD `date` DATE NOT NULL  AFTER `voucher_amount`;
ALTER TABLE `tbl_voucher`  ADD `ref_payment_id` INT NOT NULL  AFTER `date`;

/*
 * Author:  Mohammed
 * Created: 23, 09, 2020
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(NULL, 'Voucher', 'VOUCHER', '#', NULL, 'fal fa-ticket-alt', 6, 'admin', 'Live'),
(NULL, 'Create Voucher', 'CREATE_VOUCHER', 'admin/Voucher/addEditVoucher', 14, NULL, 1, 'admin', 'Live'),
(NULL, 'ManageVoucher', 'MANAGE_VOUCHER', 'admin/Voucher', 14, NULL, 2, 'admin', 'Live');

ALTER TABLE `tbl_voucher`  ADD `voucher_status` VARCHAR(50) NOT NULL DEFAULT 'Pending'  AFTER `ref_payment_id`;

ALTER TABLE `tbl_contracts`  ADD `staff_id` INT NULL DEFAULT NULL  AFTER `client`;
ALTER TABLE `tbl_voucher`  ADD `staff_id` INT NULL DEFAULT NULL  AFTER `client_id`;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Voucher', 'VOUCHER', '#', NULL, 'fal fa-ticket-alt', '6', 'customer', 'Live'), (NULL, 'Create Voucher', 'CREATE_VOUCHER', 'admin/Voucher/addEditVoucher', '17', NULL, '1', 'customer', 'Live'), (NULL, 'ManageVoucher', 'MANAGE_VOUCHER', 'admin/Voucher', '17', NULL, '2', 'customer', 'Live');
UPDATE `tbl_side_menu` SET `menu_url` = 'Voucher/addEditVoucher' WHERE `tbl_side_menu`.`menu_id` = 18;
UPDATE `tbl_side_menu` SET `menu_url` = 'Voucher' WHERE `tbl_side_menu`.`menu_id` = 19;

/*
 * Author:  Mohammed
 * Created: 24, 09, 2020
 */

ALTER TABLE `tbl_voucher`  ADD `cancel_remark` VARCHAR(250) NOT NULL  AFTER `voucher_status`;
INSERT INTO `tbl_sms_template` (`sms_template_id`, `template_for`, `message`, `is_active`, `del_status`) VALUES (NULL, 'voucher_rejected', 'just rejected your voucher {agreement_name}.', '1', 'Live');
INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES (NULL, 'voucher_rejected', 'Voucher rejected', '<h2><br />\r\njust rejected&nbsp;your&nbsp;voucher {agreement_name}.</h2>\r\n', 'Design Factory interiors', 'info@designfactoryinteriors.in', '1', 'Live');

ALTER TABLE `tbl_voucher` CHANGE `cancel_remark` `reject_remark` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `tbl_voucher` CHANGE `reject_remark` `reject_remark` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_voucher` CHANGE `voucher_status` `voucher_status` ENUM('Pending','Rejected','Approved') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Pending';

ALTER TABLE `company_information`  ADD `smtp_host` TEXT NOT NULL  AFTER `agreement_code`,  ADD `smtp_user` TEXT NOT NULL  AFTER `smtp_host`,  ADD `smtp_pass` TEXT NOT NULL  AFTER `smtp_user`,  ADD `smtp_port` INT NOT NULL  AFTER `smtp_pass`,  ADD `smtp_crypto` TEXT NOT NULL  AFTER `smtp_port`;

ALTER TABLE `company_information`  ADD `voucher_code` VARCHAR(250) NOT NULL  AFTER `agreement_code`;

ALTER TABLE `tbl_voucher`  ADD `voucher_no` INT NOT NULL  AFTER `voucher_id`;

ALTER TABLE `tbl_contracts` CHANGE `agreement_date` `agreement_date` DATETIME NULL DEFAULT NULL;
ALTER TABLE `tbl_voucher` CHANGE `date` `datetime` DATETIME NOT NULL;

ALTER TABLE `tbl_voucher`  ADD `description` TEXT NULL DEFAULT NULL  AFTER `datetime`;



/*
 * Author:  Mohammed
 * Created: 02, 10, 2020
 */

ALTER TABLE `company_information`  ADD `gift_voucher_code` VARCHAR(50) NOT NULL  AFTER `voucher_code`;

DROP TABLE IF EXISTS `tbl_gift_voucher`;
CREATE TABLE IF NOT EXISTS `tbl_gift_voucher` (
  `gift_voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_voucher_no` int(11) NOT NULL,
  `transaction_id` varchar(250) NOT NULL,
  `client_id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `agreement_id` int(11) DEFAULT NULL,
  `gift_voucher_amount` decimal(14,2) NOT NULL,
  `datetime` datetime NOT NULL,
  `description` text,
  `ref_payment_id` int(11) NOT NULL,
  `gift_voucher_status` enum('Pending','Rejected','Approved') NOT NULL DEFAULT 'Pending',
  `reject_remark` varchar(250) DEFAULT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`gift_voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_gift_voucher`  ADD `redeem_code` TEXT NOT NULL  AFTER `gift_voucher_no`;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(NULL, 'Gift Voucher', 'GIFT_VOUCHER', '#', NULL, 'fal fa-ticket-alt', 7, 'admin', 'Live'),
(NULL, 'Create Gift Voucher', 'CREATE_GIFT_VOUCHER', 'admin/Gift_voucher/addEditGiftVoucher', 23, NULL, 1, 'admin', 'Live'),
(NULL, 'Manage Gift Voucher', 'MANAGE_GIFT_VOUCHER', 'admin/Gift_voucher', 23, NULL, 2, 'admin', 'Live'),
(NULL, 'Gift Voucher', 'MANAGE_GIFT_VOUCHER', 'Gift_voucher', NULL, 'fal fa-ticket-alt', 4, 'customer', 'Live');


/*
 * Author:  Mohammed
 * Created: 13, 10, 2020
 */

DROP TABLE IF EXISTS `tbl_agreement_document`;
CREATE TABLE IF NOT EXISTS `tbl_agreement_document` (
  `ducument_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_agreement_id` int(11) NOT NULL,
  `document_path` text NOT NULL,
  `document_name` text NOT NULL,
  `document_orig_name` text NOT NULL,
  `document_view_name` varchar(250) NOT NULL,
  `document_ext` varchar(250) NOT NULL,
  `document_size` varchar(50) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`ducument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(NULL, 'upload_agreement_document', 'Gift Voucher', '<h2><br />\r\nNew Client registered.<br />\r\n{client_name} {contact_number} {email_address}.</h2>\r\n', 'Design Factory interiors', 'info@designfactoryinteriors.in', 1, 'Live');

INSERT INTO `tbl_sms_template` (`sms_template_id`, `template_for`, `message`, `is_active`, `del_status`) VALUES
(NULL, 'upload_agreement_document', 'New Document Uploaded for {agreement_name}.', 1, 'Live');


/*
 * Author:  Mohammed
 * Created: 05, 11, 2020
 */

INSERT INTO `tbl_sms_template` (`sms_template_id`, `template_for`, `message`, `is_active`, `del_status`) VALUES
(NULL, 'create_agreement', 'New agreement {agreement_name} from Design Factory Interiors.', 1, 'Live');

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(NULL, 'create_agreement', 'Design Factory Login Detail', '<h1 style=\"text-align:center\"><br />\r\nDesign Factory&nbsp;Interiors</h1>\r\nNew Agreement&nbsp;{agreement_name}&nbsp;from&nbsp;Design Factory Interiors.', 'Design Factory interiors', 'info@designfactoryinteriors.in', 1, 'Live');


/*
 * Author:  Mohammed
 * Created: 09, 11, 2020
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Send SMS', 'SEND_SMS', 'admin/Send_sms', NULL, 'fal fa-comment-alt-dots', '9', 'admin', 'Live');

DROP TABLE IF EXISTS `tbl_send_sms`;
CREATE TABLE IF NOT EXISTS `tbl_send_sms` (
  `sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_client_id` int(11) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `response` longtext,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_send_sms`  ADD `message_to` VARCHAR(50) NOT NULL  AFTER `response`;



/*
 * Author:  Mohammed
 * Created: 10, 11, 2020
 */
ALTER TABLE `tbl_gift_voucher` CHANGE `gift_voucher_amount` `gift_voucher_amount` DECIMAL(14,2) NULL DEFAULT NULL;
ALTER TABLE `tbl_gift_voucher` CHANGE `ref_payment_id` `ref_payment_id` INT(11) NULL DEFAULT NULL;

/*
 * Author:  Mohammed
 * Created: 11, 11, 2020
 */

DROP TABLE IF EXISTS `tbl_cost`;
CREATE TABLE IF NOT EXISTS `tbl_cost` (
  `cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_client_id` int(11) NOT NULL,
  `ref_agreement_id` int(11) NOT NULL,
  `cost_value` decimal(14,2) NOT NULL,
  `cost_date` datetime NOT NULL,
  `description` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`cost_id`)   
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(NULL, 'Cost', 'COST', '#', NULL, 'fal fa-handshake', 10, 'admin', 'Live'),
(NULL, 'Create Cost', 'CREATE_COST', 'admin/Cost/addEditCost', 25, NULL, 1, 'admin', 'Live'),
(NULL, 'Manage Cost', 'MANAGE_COST', 'admin/Cost', 25, NULL, 2, 'admin', 'Live');

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(NULL, 'Attendance', 'ATTENDANCE', '#', NULL, 'fal fa-user-check', 11, 'admin', 'Live'),
(NULL, 'Manage Attendance', 'MANAGE_ATTENDANCE', 'admin/Attendance', 28, NULL, 1, 'admin', 'Live');

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE IF NOT EXISTS `attendance` (
  `attendance_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_staff_id` int(11) NOT NULL,
  `attendance_date` date NOT NULL,
  `status` enum('Pending','Approved','Rejected') NOT NULL DEFAULT 'Pending',
  `reject_remark` text,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`attendance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_leave_note`;
CREATE TABLE IF NOT EXISTS `tbl_leave_note` (
  `leave_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_staff_id` int(11) NOT NULL,
  `leave_date` date NOT NULL,
  `staff_reason` text NOT NULL,
  `status` enum('Pending','Approved','Rejected') NOT NULL DEFAULT 'Pending',
  `reject_remark` text,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`leave_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(NULL, 'Leave Note', 'LEAVE_NOTE', '#', NULL, 'fal fa-user-check', 12, 'admin', 'Live'),
(NULL, 'Create Leave Note', 'CREATE_LEAVE_NOTE', 'admin/Leave_note/addEditLeaveNote', 30, NULL, 1, 'admin', 'Live'),
(NULL, 'Manage Leave Note', 'MANAGE_LEAVE_NOTE', 'admin/Leave_note', 30, NULL, 2, 'admin', 'Live');

ALTER TABLE `company_information`  ADD `gift_card_bg_image` TEXT NOT NULL  AFTER `pdf_header_image`;


/*
 * Author:  Mohammed
 * Created: 12, 11, 2020
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Cost', 'MANAGE_COST', 'Cost', NULL, 'fal fa-money-bill-wave', '5', 'customer', 'Live');

UPDATE `tbl_side_menu` SET `menu_icon` = 'fal fa-money-bill-wave' WHERE `tbl_side_menu`.`menu_id` = 25;

UPDATE `tbl_side_menu` SET `menu_icon` = 'fal fa-user-times' WHERE `tbl_side_menu`.`menu_id` = 30;



INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Agency', 'AGENCY', 'admin/Master/agency', '2', NULL, '3', 'admin', 'Live');
UPDATE `tbl_side_menu` SET `menu_order_no` = '4' WHERE `tbl_side_menu`.`menu_id` = 16;
ALTER TABLE `tbl_voucher`  ADD `voucher_for` VARCHAR(50) NOT NULL  AFTER `reject_remark`;
ALTER TABLE `tbl_voucher`  ADD `agency_id` INT NULL  AFTER `client_id`;
ALTER TABLE `tbl_voucher` CHANGE `client_id` `client_id` INT NULL;
UPDATE `tbl_voucher` SET `voucher_for`= 'client'

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Third Party', 'THIRD_PARTY', 'admin/Master/thirdParty', '2', NULL, '4', 'admin', 'Live');
UPDATE `tbl_side_menu` SET `menu_order_no` = '5' WHERE `tbl_side_menu`.`menu_id` = 16;
ALTER TABLE `tbl_contracts`  ADD `third_party_id` INT NULL  AFTER `client`;


ALTER TABLE `tbl_gift_voucher`  ADD `gift_voucher_for` VARCHAR(50) NOT NULL  AFTER `reject_remark`;
UPDATE `tbl_gift_voucher` SET `gift_voucher_for`= 'client'
ALTER TABLE `tbl_gift_voucher` CHANGE `client_id` `client_id` INT(11) NULL;
ALTER TABLE `tbl_gift_voucher`  ADD `agency_id` INT NULL  AFTER `client_id`;
ALTER TABLE `tbl_cost`  ADD `ref_agency_id` INT NULL  AFTER `ref_client_id`;
ALTER TABLE `tbl_cost`  ADD `cost_for` VARCHAR(50) NOT NULL  AFTER `description`;
ALTER TABLE `tbl_cost` CHANGE `ref_client_id` `ref_client_id` INT(11) NULL;


/*
 * Author:  Mohammed
 * Created: 03, 09, 2021
 */
ALTER TABLE `tbl_sms_template`  ADD `flow_id` TEXT NOT NULL  AFTER `template_for`;


ALTER TABLE `tbl_client_info`  ADD `reset_password_token` TEXT NULL  AFTER `email_token`;


ALTER TABLE `tbl_contracts`  ADD `third_party_signed` TINYINT NULL  AFTER `acceptance_ip`,  ADD `third_party_signature` VARCHAR(40) NULL  AFTER `third_party_signed`,  ADD `third_party_signature_path` TEXT NULL  AFTER `third_party_signature`,  ADD `third_party_marked_as_signed` TINYINT NULL  AFTER `third_party_signature_path`,  ADD `third_party_acceptance_firstname` VARCHAR(50) NULL  AFTER `third_party_marked_as_signed`,  ADD `third_party_acceptance_lastname` VARCHAR(50) NULL  AFTER `third_party_acceptance_firstname`,  ADD `third_party_acceptance_email` VARCHAR(100) NULL  AFTER `third_party_acceptance_lastname`,  ADD `third_party_acceptance_date` DATETIME NULL  AFTER `third_party_acceptance_email`,  ADD `third_party_acceptance_ip` VARCHAR(250) NULL  AFTER `third_party_acceptance_date`;
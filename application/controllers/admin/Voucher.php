<?php

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Voucher extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'VOUCHER';
        $this->load->model('Agreement_model');
        $this->load->model('Voucher_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_VOUCHER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_VOUCHER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_VOUCHER', 'VIEW');
        $data['voucher_data'] = $this->Voucher_model->getVoucher();
        $view = 'admin/voucher/voucher';
        $this->page_title = 'MANAGE VOUCHER';
        $this->load_admin_view($view, $data);
    }

    public function addEditVoucher($encrypted_id = "") {
        $this->menu_id = 'CREATE_VOUCHER';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $date = $this->input->post('date');
            $insert_data['datetime'] = isset($date) && !empty($date) ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');

            $insert_data['voucher_for'] = $voucher_for = $this->input->post('voucher_for');
            if ($voucher_for == 'client') {
                $insert_data['client_id'] = $this->input->post('client_id');
            } else if ($voucher_for == 'agency') {
                $insert_data['agency_id'] = $this->input->post('agency_id');
            }

            $insert_data['agreement_id'] = $this->input->post('agreement_id');
            $insert_data['ref_payment_id'] = $this->input->post('payment_id');
            $insert_data['voucher_amount'] = $this->input->post('voucher_amount');
            $insert_data['description'] = $this->input->post('description');

            if ($id == "" || $id == '' || $id == NULL) {
                if ($voucher_for == 'client') {
                    $insert_data['voucher_no'] = $this->Common_model->generateNoIndividualVoucher('voucher_no', 'tbl_voucher', $insert_data['client_id'], 'client_id', 'client_id', 'del_status||Live');
                } else if ($voucher_for == 'agency') {
                    $insert_data['voucher_no'] = $this->Common_model->generateNoIndividualVoucher('voucher_no', 'tbl_voucher', $insert_data['agency_id'], 'agency_id', 'agency_id', 'del_status||Live');
                }
                $transection_id = '';
                $chk_uniq = FALSE;
                while ($chk_uniq == FALSE) {
                    $transection_id = generator(15, 'tbl_voucher', 'transaction_id');
                    $chk_uniq = $this->Common_model->chkUniqueCode('tbl_voucher', 'transaction_id', $transection_id, 'Live');
                }
                $insert_data['transaction_id'] = $transection_id;

                if ($this->role == 'Admin') {
                    $insert_data['voucher_status'] = 'Approved';
//                    $insert_data['staff_id'] = NULL;
                } else {
//                    $insert_data['staff_id'] = $this->user_id;
                }

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $id = $this->Common_model->insertInformation($insert_data, 'tbl_voucher');
                if (isset($id) && !empty($id)) {
                    if ($voucher_for == 'client') {
                        $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $insert_data['client_id'], 'Live');
                    } else if ($voucher_for == 'agency') {
                        $agency_data = $this->Common_model->getDataById2('tbl_agency_info', 'agency_id', $insert_data['agency_id'], 'Live');
                        $client_data = new stdClass();
                        $client_data->client_number = (isset($agency_data->agency_number) && !empty($agency_data->agency_number) ? $agency_data->agency_number : '');
                        $client_data->country_id = (isset($agency_data->country_id) && !empty($agency_data->country_id) ? $agency_data->country_id : '');
                        $client_data->client_email = (isset($agency_data->agency_email) && !empty($agency_data->agency_email) ? $agency_data->agency_email : '');
                    }

                    $template = $this->Common_model->getSmsTemplate('voucher_create');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array(number_format($insert_data['voucher_amount'], 2), COMPANY_NAME);
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                    }

                    if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                        $data['template'] = $this->Common_model->getEmailTemplate('voucher_create');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{voucher_amount}');
                            $replace = array(number_format($insert_data['voucher_amount'], 2));
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($client_data->client_email);
                        $this->email->send();
                    }
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'voucher_id', $id, 'tbl_voucher');
            }
            redirect('admin/Voucher');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CREATE_VOUCHER', 'ADD');
                $data = [];
                $view = 'admin/voucher/addVoucher';
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $data['payment_data'] = $this->Common_model->geAlldata('tbl_payment_method');
                $this->page_title = 'CREATE VOUCHER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CREATE_VOUCHER', 'EDIT');
                $data = [];
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $data['payment_data'] = $this->Common_model->geAlldata('tbl_payment_method');
                $data['voucher_data'] = $this->Common_model->getDataById2('tbl_voucher', 'voucher_id', $id, 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/voucher/editVoucher';
                $this->page_title = 'CREATE VOUCHER';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function getAgreementByClient() {
        $client_id = $this->input->post('client_id');
        $data['agreement'] = $this->Voucher_model->getAgreementByClient($client_id);
        if (isset($data['agreement']) && !empty($data['agreement'])) {
            $data['result'] = true;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deleteVoucher() {
        $this->Common_model->check_menu_access('MANAGE_VOUCHER', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('voucher_id', $id);
            $this->db->update('tbl_voucher');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function approveVoucher() {
        $this->Common_model->check_menu_access('MANAGE_VOUCHER', 'EDIT');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('voucher_status', "Approved");
            $this->db->where('voucher_id', $id);
            $this->db->update('tbl_voucher');
            if ($this->db->affected_rows() > 0) {

                $voucher_data = $this->Common_model->getDataById2('tbl_voucher', 'voucher_id', $id, 'Live');
                if (isset($voucher_data->agreement_id) && !empty($voucher_data->agreement_id)) {
                    $agreement_data = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $voucher_data->agreement_id, 'Live');
                }
                if (isset($voucher_data) && !empty($voucher_data) && isset($voucher_data->voucher_for) && !empty($voucher_data->voucher_for) && $voucher_data->voucher_for == 'client') {
                    $client_data = $this->Common_model->getClientOrAgency('client', $voucher_data->client_id);
                } else if (isset($voucher_data) && !empty($voucher_data) && isset($voucher_data->voucher_for) && !empty($voucher_data->voucher_for) && $voucher_data->voucher_for == 'agency') {
                    $client_data = $this->Common_model->getClientOrAgency('agency', $voucher_data->agency_id);
                }

                $template = $this->Common_model->getSmsTemplate('voucher_approved');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array('Approve', (isset($agreement_data->subject) && !empty($agreement_data->subject) ? $agreement_data->subject : ''));
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                }

                if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                    $data['template'] = $this->Common_model->getEmailTemplate('voucher_approved');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{agreement_name}');
                        $replace = array((isset($agreement_data->subject) && !empty($agreement_data->subject) ? '(Agreement : ' . $agreement_data->subject . ')' : ''));
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($client_data->client_email);
                    $this->email->send();
                }

                $this->_show_message("Information has been approved successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function rejectVoucher() {
        $show_message = '';
        $show_message_type = '';
        $this->Common_model->check_menu_access('MANAGE_VOUCHER', 'EDIT');
        $id = $this->input->post('hidden_voucher_id');
        $remark = $this->input->post('reject_remark');
        if (isset($id) && !empty($id) && isset($remark) && !empty($remark)) {
            $this->db->set('voucher_status', "Rejected");
            $this->db->set('reject_remark', $remark);
            $this->db->where('voucher_id', $id);
            $this->db->update('tbl_voucher');
            if ($this->db->affected_rows() > 0) {

                $voucher_data = $this->Common_model->getDataById2('tbl_voucher', 'voucher_id', $id, 'Live');
                if (isset($voucher_data->agreement_id) && !empty($voucher_data->agreement_id)) {
                    $agreement_data = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $voucher_data->agreement_id, 'Live');
                }
                if (isset($voucher_data) && !empty($voucher_data) && isset($voucher_data->voucher_for) && !empty($voucher_data->voucher_for) && $voucher_data->voucher_for == 'client') {
                    $client_data = $this->Common_model->getClientOrAgency('client', $voucher_data->client_id);
                } else if (isset($voucher_data) && !empty($voucher_data) && isset($voucher_data->voucher_for) && !empty($voucher_data->voucher_for) && $voucher_data->voucher_for == 'agency') {
                    $client_data = $this->Common_model->getClientOrAgency('agency', $voucher_data->agency_id);
                }

                $template = $this->Common_model->getSmsTemplate('voucher_rejected');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array('Rejected', (isset($agreement_data->subject) && !empty($agreement_data->subject) ? $agreement_data->subject : ''));
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                }

                if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                    $data['template'] = $this->Common_model->getEmailTemplate('voucher_rejected');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{agreement_name}');
                        $replace = array((isset($agreement_data->subject) && !empty($agreement_data->subject) ? '(Agreement : ' . $agreement_data->subject . ')' : ''));
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($client_data->client_email);
                    $this->email->send();
                }
                $this->_show_message('Information has been rejected successfully!', 'success');
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    function printPdfVoucher($transaction_id) {
        $data = array();
        $payment_data = $data['payment_data'] = $this->Voucher_model->getVoucherDataById($transaction_id);
        $data['transaction_type'] = $this->Voucher_model->selected_transaction_type($transaction_id);
        $data['user_detail'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $data['transaction_type']->InsUser, 'Live');
        if (isset($payment_data) && !empty($payment_data) && isset($payment_data->voucher_for) && !empty($payment_data->voucher_for) && $payment_data->voucher_for == 'client') {
            $data['customer_detail'] = $this->Common_model->getClientOrAgency('client', $payment_data->client_id);
            $data['totalDue'] = $this->Voucher_model->getDueChargesById($data['transaction_type']->client_id, $data['payment_data']->datetime, 'client');
        } else if (isset($payment_data) && !empty($payment_data) && isset($payment_data->voucher_for) && !empty($payment_data->voucher_for) && $payment_data->voucher_for == 'agency') {
            $data['customer_detail'] = $this->Common_model->getClientOrAgency('agency', $payment_data->agency_id);
            $data['totalDue'] = $this->Voucher_model->getDueChargesById($data['transaction_type']->agency_id, $data['payment_data']->datetime, 'agency');
        }
        $data['payment_method'] = $this->Common_model->geAlldata('tbl_payment_method');

        $code = $this->Common_model->getCompanyCode('voucher_code');
        $company_code = strtoupper($code);
        $data['payment_data']->voucher_no = $company_code . "-" . sprintf('%06d', $data['payment_data']->voucher_no);

        if (!empty($data)) {
            try {
                $pdfhtml = $this->load->view('pdf/voucher', $data, true);
                $pdfName = cleanString($data['payment_data']->voucher_no) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

}

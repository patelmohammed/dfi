<?php

class Setting extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'SETTING';
        $this->load->model('Setting_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'SETTING';
        $data = [];
        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
        $id = $data['company_data']->company_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['company_name'] = $this->input->post('company_name');
            $insert_data['tag_line'] = $this->input->post('tag_line');
            $insert_data['email'] = $this->input->post('email');
            $insert_data['email_alt'] = $this->input->post('email_alt');
            $insert_data['address'] = $this->input->post('address');
            $insert_data['company_country'] = $this->input->post('company_country');
            $insert_data['company_country_alt'] = $this->input->post('company_country_alt');
            $insert_data['company_state'] = $this->input->post('company_state');
            $insert_data['company_city'] = $this->input->post('company_city');
            $insert_data['mobile'] = $this->input->post('mobile');
            $insert_data['mobile_alt'] = $this->input->post('mobile_alt');
            $insert_data['company_code'] = $this->input->post('company_code');
            $insert_data['agreement_code'] = $this->input->post('agreement_code');
            $insert_data['voucher_code'] = $this->input->post('voucher_code');
            $insert_data['gift_voucher_code'] = $this->input->post('gift_voucher_code');

            $insert_data['smtp_host'] = $this->input->post('smtp_host');
            $insert_data['smtp_port'] = $this->input->post('smtp_port');
            $insert_data['smtp_user'] = $this->input->post('smtp_user');
            $insert_data['smtp_pass'] = $this->input->post('smtp_pass');
            $insert_data['smtp_crypto'] = $this->input->post('smtp_crypto');

            $new_path = 'assets/img/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['company_logo']['name']) && isset($_FILES['company_logo']['name'])) {
                if ($_FILES['company_logo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('company_logo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Setting');
                    } else {
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_company_logo');
                }
            } else {
                $image_url = $this->input->post('hidden_company_logo');
            }
            $insert_data['company_logo'] = $image_url;

            if (!empty($_FILES['pdf_header_image']['name']) && isset($_FILES['pdf_header_image']['name'])) {
                if ($_FILES['pdf_header_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('pdf_header_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Setting');
                    } else {
                        $image = $this->upload->data();
                        $pdf_header_image = $new_path . $image['file_name'];
                    }
                } else {
                    $pdf_header_image = $this->input->post('hidden_pdf_header_image');
                }
            } else {
                $pdf_header_image = $this->input->post('hidden_pdf_header_image');
            }
            $insert_data['pdf_header_image'] = $pdf_header_image;

            if (!empty($_FILES['gift_card_bg_image']['name']) && isset($_FILES['gift_card_bg_image']['name'])) {
                if ($_FILES['gift_card_bg_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('gift_card_bg_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Setting');
                    } else {
                        $image = $this->upload->data();
                        $gift_card_bg_image = $new_path . $image['file_name'];
                    }
                } else {
                    $gift_card_bg_image = $this->input->post('hidden_gift_card_bg_image');
                }
            } else {
                $gift_card_bg_image = $this->input->post('hidden_gift_card_bg_image');
            }
            $insert_data['gift_card_bg_image'] = $gift_card_bg_image;

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'company_id', $id, 'company_information');
            }
            redirect('admin/Setting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SETTING', 'EDIT');
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $view = 'admin/setting/editCompany';
                $this->page_title = 'SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

}

<?php

class Template extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'MAIL_TEMPLATE';
//        $this->load->model('Template_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MAIL_TEMPLATE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MAIL_TEMPLATE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MAIL_TEMPLATE', 'VIEW');
        $data['template_data'] = $this->Common_model->geAlldata('tbl_mail_template');
        $view = 'admin/template/mail/mail_template';
        $this->page_title = 'MAIL TEMPLATE';
        $this->load_admin_view($view, $data);
    }

    public function addEditMailTemplate($encrypted_id = "") {
        $this->menu_id = 'MAIL_TEMPLATE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['subject'] = $this->input->post('subject');
            $insert_data['message'] = $this->input->post('message');
            $insert_data['fromname'] = $this->input->post('fromname');
            $insert_data['fromemail'] = $this->input->post('fromemail');

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'mail_template_id', $id, 'tbl_mail_template');
            }
            redirect('admin/Template');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('MAIL_TEMPLATE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['template_data'] = $this->Common_model->getDataById2('tbl_mail_template', 'mail_template_id', $id, 'Live');
                $view = 'admin/template/mail/editMailTemplate';
                $this->page_title = 'MAIL TEMPLATE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function smsTemplate() {
        $data = [];
        $this->page_id = 'SMS_TEMPLATE';
        $this->menu_id = 'SMS_TEMPLATE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SMS_TEMPLATE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SMS_TEMPLATE', 'VIEW');
        $data['template_data'] = $this->Common_model->geAlldata('tbl_sms_template');
        $view = 'admin/template/sms/sms_template';
        $this->page_title = 'SMS TEMPLATE';
        $this->load_admin_view($view, $data);
    }

    public function addEditSmsTemplate($encrypted_id = "") {
        $this->page_id = 'SMS_TEMPLATE';
        $this->menu_id = 'SMS_TEMPLATE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['flow_id'] = $this->input->post('flow_id');

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'sms_template_id', $id, 'tbl_sms_template');
            }

            $this->Common_model->deleteInformation('ref_template_id', $id, 'tbl_sms_template_variable');
            $variable = $this->input->post('variable');
            $variable_value = $this->input->post('variable_value');
            if (isset($variable) && !empty($variable)) {
                foreach ($variable as $key => $value) {
                    $insert_data = array();
                    $insert_data['ref_template_id'] = $id;
                    $insert_data['variable_name'] = $value;
                    $insert_data['variable_value'] = $variable_value[$key];
                    $this->Common_model->insertInformation($insert_data, 'tbl_sms_template_variable');
                }
            }
            redirect('admin/Template/smsTemplate');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SMS_TEMPLATE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['template_data'] = $this->Common_model->getDataById2('tbl_sms_template', 'sms_template_id', $id, 'Live');
                $data['template_variable_data'] = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $id);
                $view = 'admin/template/sms/editSmsTemplate';
                $this->page_title = 'SMS TEMPLATE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function mailTemplateById() {
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $data['template'] = $this->Common_model->getDataById2('tbl_mail_template', 'mail_template_id', $id, 'Live');
            if (isset($data['template']) && !empty($data['template'])) {
                $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                $data['template']->message = $this->load->view('email_template', $data, true);
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}

<?php

class Leave_note extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'LEAVE_NOTE';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_LEAVE_NOTE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_LEAVE_NOTE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_LEAVE_NOTE', 'VIEW');
        $data['leave_note_all_data'] = $this->Common_model->geAlldata('tbl_leave_note');
        $view = 'admin/leave_note/leave_note';
        $this->page_title = 'MANAGE LEAVE NOTE';
        $this->load_admin_view($view, $data);
    }

    public function addEditLeaveNote($encrypted_id = "") {
        $this->menu_id = 'CREATE_LEAVE_NOTE';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $insert_data = array();
            if ($this->role == 'Admin') {
                $insert_data['ref_staff_id'] = $this->input->post('staff_id');
            } else {
                $insert_data['ref_staff_id'] = $this->user_id;
            }
            $date = $this->input->post('date');
            $insert_data['leave_date'] = (isset($date) && !empty($date) ? date('Y-m-d', strtotime($date)) : date('Y-m-d'));
            $insert_data['staff_reason'] = $this->input->post('description');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $id = $this->Common_model->insertInformation($insert_data, 'tbl_leave_note');
//                if (isset($id) && !empty($id)) {
//                    $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $insert_data['client'], 'Live');
//
//                    $msg = $this->Common_model->getSmsTemplate('create_cost');
//                    $word = array('{agreement_name}');
//                    $replace = array($insert_data['subject']);
//                    $msg = urlencode(str_replace($word, $replace, $msg));
//
//                    $this->Common_model->send_message($client_data->client_number, $msg, $client_data->country_id);
//
//                    if (isset($client_data->client_email) && !empty($client_data->client_email)) {
//                        $data = array();
//                        $data['template'] = $this->Common_model->getEmailTemplate('create_cost');
//                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
//                        $html_template = $this->load->view('email_template', $data, true);
//
//                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
//                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
//                        $this->email->from($from_email, $from_name);
//                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
//                        if (isset($data['template']) && !empty($data['template'])) {
//                            $word = array('{agreement_name}');
//                            $replace = array($insert_data['subject']);
//                            $body = str_replace($word, $replace, $html_template);
//                        }
//                        $this->email->message($body);
//                        $this->email->to($client_data->client_email);
//                        $this->email->send();
//                    }
//                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'leave_note_id', $id, 'tbl_leave_note');
            }
            redirect('admin/Leave_note');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CREATE_LEAVE_NOTE', 'ADD');
                $data = [];
                $view = 'admin/leave_note/addLeaveNote';
                $data['staff_data'] = $this->Common_model->getStaff();
                $this->page_title = 'CREATE LEAVE NOTE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CREATE_LEAVE_NOTE', 'EDIT');
                $data = [];
                $data['staff_data'] = $this->Common_model->getStaff();
                $data['leave_note_data'] = $this->Common_model->getDataById2('tbl_leave_note', 'leave_note_id', $id, 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/leave_note/editLeaveNote';
                $this->page_title = 'EDIT LEAVE NOTE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteLeaveNote() {
        $this->Common_model->check_menu_access('MANAGE_LEAVE_NOTE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('leave_note_id', $id);
            $this->db->update('tbl_leave_note');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function approveLeaveNote() {
        $this->Common_model->check_menu_access('MANAGE_LEAVE_NOTE', 'EDIT');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $update_data = array();
            $update_data['status'] = 'Approved';
            $update_data['UpdUser'] = $this->user_id;
            $update_data['UpdTerminal'] = $this->input->ip_address();
            $update_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if ($this->Common_model->updateInformation2($update_data, 'leave_note_id', $id, 'tbl_leave_note')) {
                $this->_show_message("Attendance has been approved successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function rejectLeaveNote() {
        $this->Common_model->check_menu_access('MANAGE_LEAVE_NOTE', 'EDIT');
        $id = $this->input->post('hidden_leave_note_id');
        $remark = $this->input->post('reject_remark');
        if (isset($id) && !empty($id)) {
            $update_data = array();
            $update_data['reject_remark'] = $remark;
            $update_data['status'] = 'Rejected';
            $update_data['UpdUser'] = $this->user_id;
            $update_data['UpdTerminal'] = $this->input->ip_address();
            $update_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if ($this->Common_model->updateInformation2($update_data, 'leave_note_id', $id, 'tbl_leave_note')) {
                $this->_show_message("Attendance has been rejected successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}

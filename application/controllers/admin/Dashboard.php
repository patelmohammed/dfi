<?php

class Dashboard extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect();
        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('DASHBOARD');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW');
        $view = 'admin/home/index';
        $this->page_title = 'DASHBOARD';
        $data['attendance_data'] = $this->Dashboard_model->getAttendanceData();
        $data['payment_info'] = $this->Dashboard_model->getPaymentInfoForPieChart();
        $this->load_admin_view($view, $data);
    }

    public function readOrderStatus() {
        $id = $this->user_id;
        if (isset($id) && !empty($id)) {
            $status = $this->Dashboard_model->readOrderStatus($id);
        }
        echo json_encode($status);
        die;
    }

    function getotificationCount() {
        $cnt = 0;
        $id = $this->user_id;
        $cnt = $this->Dashboard_model->getotificationCount($id, 'client');
        echo json_encode($cnt);
        die;
    }

    public function getOrderNotificationDetail() {
        $id = $this->user_id;
        $view = 'admin/home/order_notification';
        if (isset($id) && !empty($id)) {
            $data['notification_data'] = $this->Dashboard_model->getOrderNotificationDetail($id, 'client');
            $content = $this->load->view($view, $data, true);
        }
        if (isset($content) && !empty($content)) {
            $data['result'] = true;
            $data['content'] = $content;
        }
        echo json_encode($data);
        die;
    }

    public function deleteNotification() {
        $id = $this->input->post('id');
        $response = FALSE;
        if (isset($id) && !empty($id)) {
            $response = $this->Dashboard_model->deleteNotification($id);
        }
        echo json_encode($response);
        die;
    }

    public function addEditProfile() {
        $this->menu_id = 'PROFILE';
        $id = $this->user_id;
        if ($this->input->post()) {
            $user_info = $this->Dashboard_model->getUserDataById($this->user_id);
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');
            if ((isset($old_password) && !empty($old_password))) {
                if (md5($old_password) == $user_info->user_password) {
                    if ((isset($new_password) && !empty($new_password)) && (isset($confirm_new_password) && !empty($confirm_new_password))) {
                        if ($new_password == $confirm_new_password) {
                            $insert_data['user_password'] = md5($new_password);
                            $insert_data['raw'] = $new_password;
                        } else {
                            $this->_show_message("New Password And Confirm Password not matched", "error");
                            redirect('admin/Dashboard/addEditProfile');
                        }
                    }
                } else {
                    $this->_show_message("Old Password not matched", "error");
                    redirect('admin/Dashboard/addEditProfile');
                }
            }
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['user_number'] = $this->input->post('user_number');
            $user_name = $this->input->post('user_name');
            $insert_data['user_name'] = $user_name;
            if (isset($user_name) && !empty($user_name)) {
                $user_data['user_name'] = $user_name;
                $this->session->set_userdata($user_data);
            }

            $new_path = 'assets/img/demo/avatars/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['profile_photo']['name']) && isset($_FILES['profile_photo']['name'])) {
                if ($_FILES['profile_photo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('profile_photo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Dashboard/addEditProfile');
                    } else {
                        $image = $this->upload->data();
                        $profile_photo_url = $new_path . $image['file_name'];

                        $user_data['profile_photo'] = $profile_photo_url;
                        $this->session->set_userdata($user_data);
                    }
                } else {
                    $profile_photo_url = $this->input->post('hidden_profile_photo');
                }
            } else {
                $profile_photo_url = $this->input->post('hidden_profile_photo');
            }
            $insert_data['profile_photo'] = $profile_photo_url;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->_show_message("Your profile updated successfully", "success");
                $this->Common_model->updateInformation2($insert_data, 'user_id', $id, 'tbl_user_info');
            }
            redirect('admin/Dashboard');
        } else {
            if (isset($id) && !empty($id)) {
                $data = [];
                $data['user_data'] = $this->Dashboard_model->getUserDataById($this->user_id);
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $view = 'admin/home/editProfile';
                $this->page_title = 'PROFILE';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("You cant insert new profile", "error");
                redirect('admin/Dashboard');
            }
        }
    }

    public function fillStaffAttendance() {
        if ($this->role == 'Admin') {
            $id = $this->input->post('id');
        } else {
            $id = $this->user_id;
        }
        if (isset($id) && !empty($id)) {
            $sql = "SELECT IFNULL(COUNT(*), 0) AS today_attendance FROM attendance a WHERE a.ref_staff_id = $this->user_id AND DATE_FORMAT(attendance_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')";
            $attendance_cnt = $this->db->query($sql)->row()->today_attendance;
            if ($attendance_cnt == 0) {
                $insert_data = array();
                $insert_data['ref_staff_id'] = $id;
                $insert_data['attendance_date'] = date('Y-m-d');
                if ($this->role == 'Admin') {
                    $insert_data['status'] = 'Approved';
                } else {
                    $insert_data['status'] = 'Pending';
                }
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y-m-d H:i:s');
                $insert_id = $this->Common_model->insertInformation($insert_data, 'attendance');
                if (isset($insert_id) && !empty($insert_id)) {
                    $this->_show_message("Attendance add successfully.", "success");
                    $data['result'] = true;
                    $data['message'] = 'Attendance add successfully.';
                } else {
                    $data['result'] = false;
                }
            } else {
                $data['result'] = false;
                $data['message'] = 'Attendance already filled.';
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function pie_chart_payment() {
        $data = array();
        $payment_info = $this->Dashboard_model->getPaymentInfoForPieChart();

        $data[] = ['Total Amount', (isset($payment_info->AgreementTotal) && !empty($payment_info->AgreementTotal) ? $payment_info->AgreementTotal : 0) + (isset($payment_info->TotalCost) && !empty($payment_info->TotalCost) ? $payment_info->TotalCost : 0)];
        $data[] = ['Paid', (isset($payment_info->TotalVoucher) && !empty($payment_info->TotalVoucher) ? $payment_info->TotalVoucher : 0)];
        $data[] = ['Pending', $data[0][1] - $data[1][1]];
        $data[] = ['Gift Voucher', (isset($payment_info->TotalGiftVoucher) && !empty($payment_info->TotalGiftVoucher) ? $payment_info->TotalGiftVoucher : 0)];

//            $new_result = array();
//            $new_result['TotalAmount'] = (isset($payment_info->AgreementTotal) && !empty($payment_info->AgreementTotal) ? $payment_info->AgreementTotal : 0) + (isset($payment_info->TotalCost) && !empty($payment_info->TotalCost) ? $payment_info->TotalCost : 0);
//            $new_result['TotalPaid'] = (isset($payment_info->TotalVoucher) && !empty($payment_info->TotalVoucher) ? $payment_info->TotalVoucher : 0);
//            $new_result['TotalPending'] = $new_result['TotalAmount'] - $new_result['TotalPaid'];
//            $data['result'] = $new_result;
        echo json_encode($data);
        die;
    }

}

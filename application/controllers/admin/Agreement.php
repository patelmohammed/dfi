<?php

class Agreement extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'AGREEMENT';
        $this->load->model('Agreement_model');
        $this->load->model('Auth_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_AGREEMENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_AGREEMENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_AGREEMENT', 'VIEW');
//        if ($this->role == 'Admin') {
//            $data['agreement_all_data'] = $this->Common_model->geAlldata('tbl_contracts');
//        } else {
//            $data['agreement_all_data'] = $this->Common_model->getDataById('tbl_contracts', 'staff_id', $this->user_id, 'Live');
//        }
        $data['agreement_all_data'] = $this->Agreement_model->getAgreement();
        $view = 'admin/agreement/agreement';
        $this->page_title = 'MANAGE AGREEMENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditAgreement($encrypted_id = "") {
        $this->menu_id = 'CREATE_AGREEMENT';
        $id = $encrypted_id;
        if (isset($id) && !empty($id)) {
            $data['agreement_data'] = $agreement_data = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $id, 'Live');
        }
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['client'] = $this->input->post('client_id');
            $insert_data['third_party_id'] = $third_party_id = $this->input->post('third_party_id');
            $insert_data['subject'] = $this->input->post('subject');
            $insert_data['contract_value'] = $this->input->post('agreement_value');
            $agreement_date = $this->input->post('agreement_date');
            $insert_data['agreement_date'] = (isset($agreement_date) && !empty($agreement_date) ? date('Y-m-d H:i:s', strtotime($agreement_date)) : date('Y-m-d'));
            $insert_data['description'] = $this->input->post('description');
            $insert_data['not_visible_to_client'] = $this->input->post('not_visible_to_client') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['agreement_no'] = $this->Common_model->generateNoIndividual('agreement_no', 'tbl_contracts', $insert_data['client'], 'del_status||Live');
                $insert_data['hash'] = $hash = sha1(mt_rand(1, 90000) . 'SALT');
                if ($this->role == 'Admin') {
                    $insert_data['staff_id'] = NULL;
                } else {
                    $insert_data['staff_id'] = $this->user_id;
                }

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $id = $agreement_id = $this->Common_model->insertInformation($insert_data, 'tbl_contracts');
                if (isset($agreement_id) && !empty($agreement_id) && $insert_data['not_visible_to_client'] == 0) {
                    $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $insert_data['client'], 'Live');

                    $template = $this->Common_model->getSmsTemplate('create_agreement');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array($insert_data['subject'], $client_data->client_name);
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                    }

                    if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                        $data = array();
                        $data['template'] = $this->Common_model->getEmailTemplate('create_agreement');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{agreement_name}');
                            $replace = array($insert_data['subject']);
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($client_data->client_email);
                        $this->email->send();
                    }
                }
            } else {
                $hash = $agreement_data->hash;
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'agreement_id', $id, 'tbl_contracts');
            }

            if (isset($third_party_id) && !empty($third_party_id) && isset($id) && !empty($id)) {
                $third_party_data = $this->Common_model->getDataById2('tbl_third_party_info', 'third_party_id', $third_party_id, 'Live');
                $template = $this->Common_model->getSmsTemplate('create_agreement_third_party');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array($insert_data['subject'], base_url(), 'Without_login/' . $id . '/' . $hash);
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($third_party_data->third_party_number, $template->flow_id, $variable);
                }

                if (isset($third_party_data->third_party_email) && !empty($third_party_data->third_party_email)) {
                    $data = array();
                    $data['template'] = $this->Common_model->getEmailTemplate('create_agreement_third_party');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{agreement_name}', '{base_url}', '{agreement_url}');
                        $replace = array($insert_data['subject'], base_url(), 'Without_login/' . $id . '/' . $hash);
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($third_party_data->third_party_email);
                    $this->email->send();
                }
            }

            redirect('admin/Agreement');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CREATE_AGREEMENT', 'ADD');
                $data = [];
                $view = 'admin/agreement/addAgreement';
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['third_party_data'] = $this->Common_model->geAlldata('tbl_third_party_info');
                $this->page_title = 'CREATE AGREEMENT';
                $this->load_admin_view($view, $data);
            } else {
                $data = array();
                $this->Common_model->check_menu_access('CREATE_AGREEMENT', 'EDIT');
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['agreement_data'] = $agreement_data;
                $data['third_party_data'] = $this->Common_model->geAlldata('tbl_third_party_info');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/agreement/editAgreement';
                $this->page_title = 'CREATE AGREEMENT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function agreementDetail($id = NULL, $hash = NULL) {
        $this->menu_id = 'MANAGE_AGREEMENT';
        $data = [];
        if (isset($id) && !empty($id) && isset($hash) && !empty($hash)) {
            $data['agreement_data'] = $this->Agreement_model->getAgreementbyId($id, $hash);
            $data['client_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $data['agreement_data']->client, 'Live');
            if (isset($data['agreement_data']) && !empty($data['agreement_data'])) {
                $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_AGREEMENT');
                if (empty($data['menu_rights'])) {
                    redirect('admin/Auth/Unauthorized');
                }
                $this->Common_model->check_menu_access('MANAGE_AGREEMENT', 'VIEW');
                $view = 'admin/agreement/agreement_detail';
                $this->page_title = 'AGREEMENT DETAIL';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("Agreement not fount.", "error");
                redirect('admin/Agreement');
            }
        } else {
            $this->_show_message("Agreement not fount.", "error");
            redirect('admin/Agreement');
        }
    }

    public function agreementSign($id = NULL, $hash = NULL) {
        $this->menu_id = 'MANAGE_AGREEMENT';
        $data = [];
        if (isset($id) && !empty($id) && isset($hash) && !empty($hash)) {
            if ($this->input->post()) {
                $response = process_digital_signature_image($this->input->post('signature', false), 'assets/img/signature/admin/' . $this->user_id);
                if ($response['retval'] == TRUE) {
                    $agreement_data = $this->Agreement_model->getAgreementbyId($id, $hash);
                    $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $agreement_data->client, 'Live');
//                    $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $agreement_data->InsUser, 'Live');
                    $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->user_id, 'Live');

                    $insert_data['admin_signature'] = $response['filename'];
                    $insert_data['admin_signature_path'] = $response['filename_path'];
                    $insert_data['admin_acceptance_firstname'] = $this->input->post('first_name');
                    $insert_data['admin_acceptance_lastname'] = $this->input->post('last_name');
                    $insert_data['admin_acceptance_email'] = $this->input->post('email');
                    $insert_data['admin_acceptance_date'] = date('Y/m/d H:i:s');
                    $insert_data['admin_acceptance_ip'] = $this->input->ip_address();
                    $insert_data['admin_signed'] = 1;
                    $this->Common_model->updateInformation2($insert_data, 'agreement_id', $id, 'tbl_contracts', 'Live', 'hash', $hash);

                    $template = $this->Common_model->getSmsTemplate('staff_agreement_signed');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array($user_data->user_name, $agreement_data->subject);
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                    }

                    if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                        $data = array();
                        $data['template'] = $this->Common_model->getEmailTemplate('staff_agreement_signed');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{staff_name}', '{agreement_name}');
                            $replace = array($user_data->user_name, $agreement_data->subject);
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($client_data->client_email);
                        $this->email->send();
                    }

                    $notification_data = array();
                    $notification_data['datetime'] = date('Y-m-d H:i:s');
                    $notification_data['ref_client_id'] = $client_data->client_id;
                    $notification_data['ref_client_name'] = $client_data->client_name;
                    $notification_data['ref_staff_id'] = $user_data->user_id;
                    $notification_data['ref_staff_name'] = $user_data->user_name;
                    $notification_data['ref_agreement_id'] = $id;
                    $notification_data['ref_agreement_hash'] = $hash;
                    $notification_data['ref_agreement_subject'] = $agreement_data->subject;
                    $notification_data['noti_from'] = 'staff';
                    $this->Common_model->insertInformation($notification_data, 'sign_notification');

                    $this->_show_message("Agreement singed successfully.", "success");
                    redirect('admin/Agreement/agreementDetail/' . $id . '/' . $hash);
                } else {
                    $this->_show_message("Something went wrong please try again.", "error");
                    redirect('admin/Agreement/agreementDetail/' . $id . '/' . $hash);
                }
            } else {
                $this->_show_message("Something went wrong please try again.", "error");
                redirect('admin/Agreement/agreementDetail/' . $id . '/' . $hash);
            }
        } else {
            $this->_show_message("Agreement not fount.", "error");
            redirect('admin/Agreement');
        }
    }

    public function deleteAgreement() {
        $this->Common_model->check_menu_access('MANAGE_AGREEMENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('agreement_id', $id);
            $this->db->update('tbl_contracts');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function agreementDocument($agreement_id = NULL) {
        $data = [];
        $this->menu_id = 'MANAGE_AGREEMENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_AGREEMENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_AGREEMENT', 'VIEW');
        $data['agreement_document'] = $this->Agreement_model->getAgreementDocument($agreement_id);
        $data['agreement_id'] = $agreement_id;
        if (isset($agreement_id) && !empty($agreement_id)) {
            $data['agreement_all_data'] = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $agreement_id, 'Live');
            $data['client_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $data['agreement_all_data']->client, 'Live');
        }
        $view = 'admin/agreement/agreement_document/agreement_document';
        $this->page_title = 'MANAGE AGREEMENT DOCUMENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditAgreementDocument($agreement_id = "") {
        $this->menu_id = 'CREATE_AGREEMENT';
        if ($this->input->post()) {

            $insert_data = array();
            $agreement_id = $this->input->post('agreement_id');

            $document_path = 'assets/documents/agreement/' . $agreement_id . '/';
            if (!is_dir($document_path)) {
                if (!mkdir($document_path, 0777, true)) {
                    die('Not Created');
                }
            }

            $upload_document = $_FILES['document_name'];
            $document_name = $this->input->post('document_view_name');
            $is_active_document = $this->input->post('is_active');
            if (isset($upload_document) && !empty($upload_document)) {
                foreach ($upload_document['name'] as $key => $value) {
                    if (isset($value) && !empty($value)) {
                        $_FILES['document_name']['name'] = $upload_document['name'][$key];
                        $_FILES['document_name']['type'] = $upload_document['type'][$key];
                        $_FILES['document_name']['tmp_name'] = $upload_document['tmp_name'][$key];
                        $_FILES['document_name']['error'] = $upload_document['error'][$key];
                        $_FILES['document_name']['size'] = $upload_document['size'][$key];

                        $config['upload_path'] = $document_path;
                        $config['allowed_types'] = 'PDF|pdf';
                        $config['max_size'] = "*";
                        $config['max_width'] = "*";
                        $config['max_height'] = "*";
                        $config['encrypt_name'] = FALSE;

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('document_name')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->_show_message((isset($error['error']) && !empty($error['error']) ? $error['error'] : 'Somethig wrong'), "error");
                            redirect('admin/Agreement/agreementDocument');
                        } else {
                            $file = $this->upload->data();
                            $insert_data = array();

                            $insert_data['ref_agreement_id'] = $agreement_id;

                            $insert_data['document_orig_name'] = $file['orig_name'];
                            $insert_data['document_path'] = $document_path . $file['file_name'];
                            $insert_data['document_name'] = $file['file_name'];
                            $insert_data['document_ext'] = $file['file_ext'];
                            $insert_data['document_size'] = $file['file_size'];
                            $insert_data['document_view_name'] = (isset($document_name[$key]) && !empty($document_name[$key]) ? $document_name[$key] : 0);
                            $insert_data['is_active'] = (isset($is_active_document[$key]) && !empty($is_active_document[$key]) ? ($is_active_document[$key] == 'on' ? 1 : 0) : 0);

                            $insert_data['InsUser'] = $this->user_id;
                            $insert_data['InsTerminal'] = $this->input->ip_address();
                            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                            $this->Common_model->insertInformation($insert_data, 'tbl_agreement_document');
                        }
                    }
                }
            }

            $agreement_data = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $agreement_id, 'Live');
            $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $agreement_data->client, 'Live');

            $template = $this->Common_model->getSmsTemplate('upload_agreement_document');
            if (isset($template->flow_id) && !empty($template->flow_id)) {
                $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                $variable = '';
                if (isset($template_veriables) && !empty($template_veriables)) {
                    $variable .= '{';
                    foreach ($template_veriables as $key => $value) {
                        if ($key != 0) {
                            $variable .= ', ';
                        }
                        $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                    }
                    $variable .= '}';

                    $word = array_column($template_veriables, 'variable_value');
                    $replace = array($agreement_data->subject);
                    $variable = str_replace($word, $replace, $variable);
                }
                $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
            }

            if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                $data = array();
                $data['template'] = $this->Common_model->getEmailTemplate('upload_agreement_document');
                $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{agreement_name}');
                    $replace = array($agreement_data->subject);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($client_data->client_email);
                $this->email->send();
            }

            redirect('admin/Agreement/agreementDocument/' . (isset($agreement_id) && !empty($agreement_id) ? $agreement_id : ''));
        } else {
            $this->Common_model->check_menu_access('CREATE_AGREEMENT', 'ADD');
            $data = [];
            $data['agreement_id'] = $agreement_id;
            $data['agreement'] = $this->Common_model->geAlldataById('tbl_contracts', 'del_status', 'Live', 'del_status = "Live"');
            $view = 'admin/agreement/agreement_document/addAgreementDocument';
            $this->page_title = 'CREATE AGREEMENT DOCUMENT';
            $this->load_admin_view($view, $data);
        }
    }

    public function deleteAgreementDocument() {
        $this->Common_model->check_menu_access('MANAGE_AGREEMENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->trans_start();

            $this->db->set('del_status', "Deleted");
            $this->db->where('ducument_id', $id);
            $this->db->update('tbl_agreement_document');
            if ($this->db->affected_rows() > 0) {

                $agreement_document = $this->Common_model->getDataById2('tbl_agreement_document', 'ducument_id', $id);
                $document_path = 'assets/documents/agreement/' . $agreement_document->ref_agreement_id . '/';

                rename($document_path . $agreement_document->document_name, $document_path . '_' . $agreement_document->document_name);

                $this->db->set('document_name', '_' . $agreement_document->document_name);
                $this->db->set('document_path', $document_path . '_' . $agreement_document->document_name);
                $this->db->where('ducument_id', $id);
                $this->db->update('tbl_agreement_document');

                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $data['result'] = false;
            } else {
                $data['result'] = true;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveAgreementDocument() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('ducument_id', $id)->update('tbl_agreement_document');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('ducument_id', $id)->update('tbl_agreement_document');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}

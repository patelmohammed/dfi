<?php

class Send_sms extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'SEND_SMS';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'SEND_SMS';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SEND_SMS');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SEND_SMS', 'VIEW');
        $data['sms_data'] = $this->Common_model->geAlldata('tbl_send_sms');
        $view = 'admin/send_sms/send_sms';
        $this->page_title = 'SMS';
        $this->load_admin_view($view, $data);
    }

    public function addEditSendSms() {
        $this->menu_id = 'SEND_SMS';
        if ($this->input->post()) {
            $client_id = $this->input->post('client_id');
            $message_body = urlencode(strip_tags($this->input->post('message_body')));

            if (isset($client_id) && !empty($client_id)) {
                foreach ($client_id as $key => $value) {
                    $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $value, 'Live');
                    $response = $this->Common_model->send_message($client_data->client_number, $message_body, $client_data->country_id);

                    $insert_data = array();
                    $insert_data['ref_client_id'] = $client_data->client_id;
                    $insert_data['contact_number'] = $country_code = getCountryCode($client_data->country_id) . ' ' . $client_data->client_number;
                    $insert_data['message'] = urldecode($message_body);
                    $insert_data['response'] = $response;
                    $insert_data['message_to'] = 'client';
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
//                    $this->Common_model->insertInformation($insert_data, 'tbl_send_sms');
                }
            }

            if ($this->input->post('send_me_copy') == 'on' && $this->role != 'Admin') {
                $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->user_id, 'Live');
                $response = $this->Common_model->send_message($user_data->user_number, $message_body, $user_data->country_id);

                $insert_data = array();
                $insert_data['ref_client_id'] = $user_data->user_id;
                $insert_data['contact_number'] = getCountryCode($user_data->country_id) . ' ' . $user_data->user_number;
                $insert_data['message'] = urldecode($message_body);
                $insert_data['response'] = $response;
                $insert_data['message_to'] = 'admin';
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
//                $this->Common_model->insertInformation($insert_data, 'tbl_send_sms');
            }
            $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', 2, 'Live');
            $response = $this->Common_model->send_message($user_data->user_number, $message_body, $user_data->country_id);

            $insert_data = array();
            $insert_data['ref_client_id'] = $user_data->user_id;
            $insert_data['contact_number'] = getCountryCode($user_data->country_id) . ' ' . $user_data->user_number;
            $insert_data['message'] = urldecode($message_body);
            $insert_data['response'] = $response;
            $insert_data['message_to'] = 'admin';
            $insert_data['InsUser'] = $this->user_id;
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
//            $this->Common_model->insertInformation($insert_data, 'tbl_send_sms');

            redirect('admin/Send_sms');
        } else {
            $this->Common_model->check_menu_access('SEND_SMS', 'ADD');
            $data = [];
            $view = 'admin/send_sms/addSendSms';
            $data['client_data'] = $this->Common_model->geAlldataById('tbl_client_info', 'del_status', 'Live', 'del_status = "Live"', 'is_message_verified', 1);
            $this->page_title = 'SEND SMS';
            $this->load_admin_view($view, $data);
        }
    }

}

<?php

class Attendance extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'ATTENDANCE';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_ATTENDANCE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_ATTENDANCE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_ATTENDANCE', 'VIEW');
        $data['attendance_all_data'] = $this->Common_model->geAlldata('attendance');
        $view = 'admin/attendance/attendance';
        $this->page_title = 'MANAGE ATTENDANCE';
        $this->load_admin_view($view, $data);
    }

    public function deleteAttendance() {
        $this->Common_model->check_menu_access('MANAGE_COST', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('attendance_id', $id);
            $this->db->update('attendance');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function approveAttendance() {
        $this->Common_model->check_menu_access('MANAGE_ATTENDANCE', 'EDIT');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $update_data = array();
            $update_data['status'] = 'Approved';
            $update_data['UpdUser'] = $this->user_id;
            $update_data['UpdTerminal'] = $this->input->ip_address();
            $update_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if ($this->Common_model->updateInformation2($update_data, 'attendance_id', $id, 'attendance')) {
                $this->_show_message("Attendance has been approved successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function rejectAttendance() {
        $this->Common_model->check_menu_access('MANAGE_ATTENDANCE', 'EDIT');
        $id = $this->input->post('hidden_attendance_id');
        $remark = $this->input->post('reject_remark');
        if (isset($id) && !empty($id)) {
            $update_data = array();
            $update_data['reject_remark'] = $remark;
            $update_data['status'] = 'Rejected';
            $update_data['UpdUser'] = $this->user_id;
            $update_data['UpdTerminal'] = $this->input->ip_address();
            $update_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if ($this->Common_model->updateInformation2($update_data, 'attendance_id', $id, 'attendance')) {
                $this->_show_message("Attendance has been rejected successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}

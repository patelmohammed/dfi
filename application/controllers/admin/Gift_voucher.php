<?php

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Gift_voucher extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'GIFT_VOUCHER';
        $this->load->model('Agreement_model');
        $this->load->model('Gift_voucher_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_GIFT_VOUCHER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_GIFT_VOUCHER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_GIFT_VOUCHER', 'VIEW');
        $data['gift_voucher_data'] = $this->Gift_voucher_model->getGiftVoucher();
        $view = 'admin/gift_voucher/gift_voucher';
        $this->page_title = 'MANAGE GIFT VOUCHER';
        $this->load_admin_view($view, $data);
    }

    public function addEditGiftVoucher($encrypted_id = "") {
        $this->menu_id = 'CREATE_GIFT_VOUCHER';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $date = $this->input->post('date');
            $insert_data['datetime'] = isset($date) && !empty($date) ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');
            $gift_voucher_for = $insert_data['gift_voucher_for'] = $this->input->post('gift_voucher_for');
            if (isset($gift_voucher_for) && !empty($gift_voucher_for) && $gift_voucher_for == 'client') {
                $insert_data['client_id'] = $this->input->post('client_id');
            } else if (isset($gift_voucher_for) && !empty($gift_voucher_for) && $gift_voucher_for == 'agency') {
                $insert_data['agency_id'] = $this->input->post('agency_id');
            }
            $insert_data['agreement_id'] = $this->input->post('agreement_id');
            $insert_data['ref_payment_id'] = $this->input->post('payment_id');
            $insert_data['gift_voucher_amount'] = $this->input->post('gift_voucher_amount');
            $insert_data['description'] = $this->input->post('description');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['gift_voucher_no'] = $this->Common_model->generateNoIndividualVoucher('gift_voucher_no', 'tbl_gift_voucher', '', '', 'del_status||Live');

                $chk_uniq_redeem = FAlSE;
                $redeem_code = '';
                while ($chk_uniq_redeem == FALSE) {
                    $redeem_code = redeeme_code_generat();
                    $chk_uniq_redeem = $this->Common_model->chkUniqueCode('tbl_gift_voucher', 'redeem_code', $redeem_code, 'Live');
                }
                $insert_data['redeem_code'] = $redeem_code;

                $transection_id = '';
                $chk_uniq = FALSE;
                while ($chk_uniq == FALSE) {
                    $transection_id = generator(15, 'tbl_gift_voucher', 'transaction_id');
                    $chk_uniq = $this->Common_model->chkUniqueCode('tbl_gift_voucher', 'transaction_id', $transection_id, 'Live');
                }
                $insert_data['transaction_id'] = $transection_id;

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $gift_voucher_id = $this->Common_model->insertInformation($insert_data, 'tbl_gift_voucher');
                if (isset($gift_voucher_id) && !empty($gift_voucher_id)) {
                    $data['payment_data'] = $this->Gift_voucher_model->getGiftVoucherDataById($transection_id);
                    if (isset($gift_voucher_for) && !empty($gift_voucher_for) && $gift_voucher_for == 'client') {
                        $data['customer_detail'] = $client_data = $this->Common_model->getClientOrAgency('client', $insert_data['client_id']);
                    } else if (isset($gift_voucher_for) && !empty($gift_voucher_for) && $gift_voucher_for == 'agency') {
                        $data['customer_detail'] = $client_data = $this->Common_model->getClientOrAgency('agency', $insert_data['agency_id']);
                    }

                    $template = $this->Common_model->getSmsTemplate('gift_voucher_created');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array((isset($data['payment_data']->gift_voucher_amount) && !empty($data['payment_data']->gift_voucher_amount) ? $data['payment_data']->gift_voucher_amount : ''));
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                    }

                    if (isset($client_data->client_email) && !empty($client_data->client_email)) {
                        $data['template'] = $this->Common_model->getEmailTemplate('gift_voucher_created');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');

                        $data['transaction_type'] = $this->Gift_voucher_model->selected_transaction_type($transection_id);

                        $code = $this->Common_model->getCompanyCode('gift_voucher_code');
                        $company_code = strtoupper($code);
                        $data['payment_data']->gift_voucher_no = $company_code . "-" . sprintf('%06d', $data['payment_data']->gift_voucher_no);

                        $html_template = $this->load->view('email_template', $data, true);

                        if (!empty($data)) {
                            try {
                                $new_path = 'assets/documents/gift_voucher/';
                                if (!is_dir($new_path)) {
                                    if (!mkdir($new_path, 0777, true)) {
                                        die('Not Created');
                                    }
                                }

                                $pdfhtml = $this->load->view('pdf/gift_voucher', $data, true);
                                $pdfName = cleanString($data['payment_data']->gift_voucher_no) . '.pdf';
                                $attachment = array(getcwd() . '/' . $pdfName);
                                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                                $html2pdf->pdf->SetDisplayMode('fullpage');
                                $html2pdf->writeHTML($pdfhtml);
                                $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                                $attachment[] = getcwd() . '/' . $pdfName;
                            } catch (Html2PdfException $e) {
                                $data = [];
                                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                                echo json_encode($data);
                                die;
                            }
                        }

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        $this->email->attach($pdfName);
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{client_name}', '{gift_voucher_amount}', '{redeem_code}');
                            $replace = array($client_data->client_name, (isset($data['payment_data']->gift_voucher_amount) && !empty($data['payment_data']->gift_voucher_amount) ? $data['payment_data']->gift_voucher_amount : ''), (isset($data['payment_data']->redeem_code) && !empty($data['payment_data']->redeem_code) ? $data['payment_data']->redeem_code : ''));
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($client_data->client_email);
                        $this->email->send();
                    }
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'gift_voucher_id', $id, 'tbl_gift_voucher');
            }
            redirect('admin/Gift_voucher');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CREATE_GIFT_VOUCHER', 'ADD');
                $data = [];
                $view = 'admin/gift_voucher/addGiftVoucher';
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $data['payment_data'] = $this->Common_model->geAlldata('tbl_payment_method');
                $this->page_title = 'CREATE GIFT VOUCHER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CREATE_GIFT_VOUCHER', 'EDIT');
                $data = [];
                $data['customer_data'] = $this->Agreement_model->getClient();
                $data['payment_data'] = $this->Common_model->geAlldata('tbl_payment_method');
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $data['gift_voucher_data'] = $this->Common_model->getDataById2('tbl_gift_voucher', 'gift_voucher_id', $id, 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/gift_voucher/editGiftVoucher';
                $this->page_title = 'CREATE GIFT_VOUCHER';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteGiftVoucher() {
        $this->Common_model->check_menu_access('MANAGE_GIFT_VOUCHER', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('gift_voucher_id', $id);
            $this->db->update('tbl_gift_voucher');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    function printPdfGiftVoucher($transaction_id) {
        $data = array();
        $data['payment_data'] = $payment_data = $this->Gift_voucher_model->getGiftVoucherDataById($transaction_id);
        $data['transaction_type'] = $this->Gift_voucher_model->selected_transaction_type($transaction_id);
        if (isset($payment_data->gift_voucher_for) && !empty($payment_data->gift_voucher_for) && $payment_data->gift_voucher_for == 'client') {
            $data['customer_detail'] = $this->Common_model->getClientOrAgency('client', $data['transaction_type']->client_id);
        } else if (isset($payment_data->gift_voucher_for) && !empty($payment_data->gift_voucher_for) && $payment_data->gift_voucher_for == 'agency') {
            $data['customer_detail'] = $this->Common_model->getClientOrAgency('agency', $data['transaction_type']->agency_id);
        }
        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');

        $code = $this->Common_model->getCompanyCode('gift_voucher_code');
        $company_code = strtoupper($code);
        $data['payment_data']->gift_voucher_no = $company_code . "-" . sprintf('%06d', $data['payment_data']->gift_voucher_no);

        if (!empty($data)) {
            try {
                $pdfhtml = $this->load->view('pdf/gift_voucher', $data, true);
                $pdfName = cleanString($data['payment_data']->gift_voucher_no) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

    public function mailGiftVoucher() {
        $transection_id = $this->input->post('id');
        if (isset($transection_id) && !empty($transection_id)) {
            $data['payment_data'] = $payment_data = $this->Gift_voucher_model->getGiftVoucherDataById($transection_id);
            $data['template'] = $this->Common_model->getEmailTemplate('gift_voucher_created');
            $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
            $data['transaction_type'] = $this->Gift_voucher_model->selected_transaction_type($transection_id);

            if (isset($payment_data->gift_voucher_for) && !empty($payment_data->gift_voucher_for) && $payment_data->gift_voucher_for == 'client') {
                $data['customer_detail'] = $this->Common_model->getClientOrAgency('client', $data['transaction_type']->client_id);
            } else if (isset($payment_data->gift_voucher_for) && !empty($payment_data->gift_voucher_for) && $payment_data->gift_voucher_for == 'agency') {
                $data['customer_detail'] = $this->Common_model->getClientOrAgency('agency', $data['transaction_type']->agency_id);
            }

            $code = $this->Common_model->getCompanyCode('gift_voucher_code');
            $company_code = strtoupper($code);
            $data['payment_data']->gift_voucher_no = $company_code . "-" . sprintf('%06d', $data['payment_data']->gift_voucher_no);

            $html_template = $this->load->view('email_template', $data, true);

            if (!empty($data)) {
                try {
                    $new_path = 'assets/documents/gift_voucher/';
                    if (!is_dir($new_path)) {
                        if (!mkdir($new_path, 0777, true)) {
                            die('Not Created');
                        }
                    }

                    $pdfhtml = $this->load->view('pdf/gift_voucher', $data, true);
                    $pdfName = cleanString($data['payment_data']->gift_voucher_no) . '.pdf';
                    $attachment = array(getcwd() . '/' . $pdfName);
                    $html2pdf = new Html2Pdf('L', 'A5', 'en');
                    $html2pdf->pdf->SetDisplayMode('fullpage');
                    $html2pdf->writeHTML($pdfhtml);
                    $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                    $attachment[] = getcwd() . '/' . $pdfName;
                } catch (Html2PdfException $e) {
                    $data = [];
                    $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                    echo json_encode($data);
                    die;
                }
            }

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            $this->email->attach($pdfName);
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{client_name}', '{gift_voucher_amount}', '{redeem_code}');
                $replace = array($data['customer_detail']->client_name, (isset($data['payment_data']->gift_voucher_amount) && !empty($data['payment_data']->gift_voucher_amount) ? $data['payment_data']->gift_voucher_amount : ''), (isset($data['payment_data']->redeem_code) && !empty($data['payment_data']->redeem_code) ? $data['payment_data']->redeem_code : ''));
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);
            $this->email->to($data['customer_detail']->client_email);
            if ($this->email->send()) {
                $result['result'] = true;
            } else {
                $result['result'] = false;
            }
        } else {
            $result['result'] = false;
        }
        echo json_encode($result);
        die;
    }

}

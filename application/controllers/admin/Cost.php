<?php

class Cost extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'COST';
        $this->load->model('Common_model');
        $this->load->model('Cost_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MANAGE_COST';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_COST');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_COST', 'VIEW');
        $data['cost_all_data'] = $this->Cost_model->getCost();
        $view = 'admin/cost/cost';
        $this->page_title = 'MANAGE COST';
        $this->load_admin_view($view, $data);
    }

    public function addEditCost($encrypted_id = "") {
        $this->menu_id = 'CREATE_COST';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $insert_data = array();
            $cost_for = $insert_data['cost_for'] = $this->input->post('cost_for');
            if (isset($cost_for) && !empty($cost_for) && $cost_for == 'client') {
                $insert_data['ref_client_id'] = $this->input->post('client_id');
            } else if (isset($cost_for) && !empty($cost_for) && $cost_for == 'agency') {
                $insert_data['ref_agency_id'] = $this->input->post('agency_id');
            }

            $insert_data['ref_agreement_id'] = $this->input->post('agreement_id');
            $insert_data['cost_value'] = $this->input->post('cost_value');
            $date = $this->input->post('date');
            $insert_data['cost_date'] = (isset($date) && !empty($date) ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d'));
            $insert_data['description'] = $this->input->post('description');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $id = $this->Common_model->insertInformation($insert_data, 'tbl_cost');
                if (isset($id) && !empty($id)) {
                    if (isset($cost_for) && !empty($cost_for) && $cost_for == 'client') {
                        $client_data = $this->Common_model->getClientOrAgency('client', $insert_data['ref_client_id']);
                    } else if (isset($cost_for) && !empty($cost_for) && $cost_for == 'agency') {
                        $client_data = $this->Common_model->getClientOrAgency('agency', $insert_data['ref_agency_id']);
                    }
                    if (isset($insert_data['ref_agreement_id']) && !empty($insert_data['ref_agreement_id'])) {
                        $agreement_data = $this->Common_model->getDataById2('tbl_contracts', 'agreement_id', $id, 'Live');
                    }

                    $template = $this->Common_model->getSmsTemplate('create_cost');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array((isset($agreement_data->subject) && !empty($agreement_data->subject) ? $agreement_data->subject : ''), (isset($insert_data['cost_value']) && !empty($insert_data['cost_value']) ? $insert_data['cost_value'] : ''));
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($client_data->client_number, $template->flow_id, $variable);
                    }

                    $data = array();
                    $data['template'] = $template = $this->Common_model->getEmailTemplate('create_cost');
                    if (isset($client_data->client_email) && !empty($client_data->client_email) && isset($template) && !empty($template)) {
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(((isset($data['template']->subject) && !empty($data['template']->subject) ? $data['template']->subject : '') . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{agreement_name}', '{cost_amount}');
                            $replace = array((isset($agreement_data->subject) && !empty($agreement_data->subject) ? $agreement_data->subject : ''), (isset($insert_data['cost_value']) && !empty($insert_data['cost_value']) ? $insert_data['cost_value'] : ''));
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($client_data->client_email);
                        $this->email->send();
                    }
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'cost_id', $id, 'tbl_cost');
            }
            redirect('admin/Cost');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CREATE_COST', 'ADD');
                $data = [];
                $view = 'admin/cost/addCost';
                $data['customer_data'] = $this->Cost_model->getClient();
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $this->page_title = 'CREATE COST';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CREATE_COST', 'EDIT');
                $data = [];
                $data['customer_data'] = $this->Cost_model->getClient();
                $data['agency_data'] = $this->Common_model->geAlldata('tbl_agency_info');
                $data['cost_data'] = $this->Common_model->getDataById2('tbl_cost', 'cost_id', $id, 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/cost/editCost';
                $this->page_title = 'EDIT COST';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteCost() {
        $this->Common_model->check_menu_access('MANAGE_COST', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('cost_id', $id);
            $this->db->update('tbl_cost');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}

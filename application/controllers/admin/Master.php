<?php

class Master extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'MASTER';
        $this->load->model('Master_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'STAFF';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('STAFF');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('STAFF', 'VIEW');
        $data['user_data'] = $this->Master_model->getStaff();
        $view = 'admin/master/staff/staff';
        $this->page_title = 'STAFF';
        $this->load_admin_view($view, $data);
    }

    public function staff() {
        $data = [];
        $this->menu_id = 'STAFF';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('STAFF');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('STAFF', 'VIEW');
        $data['user_data'] = $this->Master_model->getStaff();
        $view = 'admin/master/staff/staff';
        $this->page_title = 'STAFF';
        $this->load_admin_view($view, $data);
    }

    public function addEditStaff($encrypted_id = "") {
        $this->menu_id = 'STAFF';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = $user_menu_data = array();
            $insert_data['user_name'] = $this->input->post('user_name');
            $insert_data['user_email'] = $this->input->post('user_email');
            $insert_data['user_number'] = $this->input->post('mobile');
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['user_address'] = $this->input->post('address');
            $insert_data['user_pincode'] = $this->input->post('pincode');
            $insert_data['user_state'] = $this->input->post('state');
            $insert_data['user_city'] = $this->input->post('city');

            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['user_password'] = md5($password);
                    $insert_data['raw'] = $password;

                    if (isset($id) && !empty($id)) {
                        $template = $this->Common_model->getSmsTemplate('change_password');
                        if (isset($template->flow_id) && !empty($template->flow_id)) {
                            $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                            $variable = '';
                            if (isset($template_veriables) && !empty($template_veriables)) {
                                $variable .= '{';
                                foreach ($template_veriables as $key => $value) {
                                    if ($key != 0) {
                                        $variable .= ', ';
                                    }
                                    $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                                }
                                $variable .= '}';

                                $word = array_column($template_veriables, 'variable_value');
                                $replace = array($insert_data['user_number'], (isset($insert_data['user_email']) && !empty($insert_data['user_email']) ? ' OR ' . $insert_data['user_email'] : ''), $password, (base_url()));
                                $variable = str_replace($word, $replace, $variable);
                            }
                            $this->sendMessage($insert_data['user_number'], $template->flow_id, $variable);
                        }

                        if (isset($insert_data['user_email']) && !empty($insert_data['user_email'])) {
                            $data = array();
                            $data['template'] = $this->Common_model->getEmailTemplate('change_password');
                            $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                            $html_template = $this->load->view('email_template', $data, true);

                            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                            $this->email->from($from_email, $from_name);
                            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                            if (isset($data['template']) && !empty($data['template'])) {
                                $word = array('{user_name}', '{user_email}', '{password}', '{website_link}');
                                $replace = array($insert_data['user_number'], (isset($insert_data['user_email']) && !empty($insert_data['user_email']) ? ' OR ' . $insert_data['user_email'] : ''), $password, (base_url()));
                                $body = str_replace($word, $replace, $html_template);
                            }
                            $this->email->message($body);
                            $this->email->to($insert_data['user_email']);
                            $this->email->send();
                        }
                    }
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Master/addEditStaff');
                    } else {
                        redirect('admin/Master/addEditStaff/' . $encrypted_id);
                    }
                }
            }

            $full_access = $this->input->post('full_access');
            $view = $this->input->post('view');
            $add = $this->input->post('add');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');

            $ins_menu = [];
            if (!empty($full_access)) {
                foreach ($full_access as $key => $value) {
                    $ins_menu[$key]['full_access'] = $value == 'on' ? 1 : 0;
                }
            }
            if (!empty($view)) {
                foreach ($view as $vi_key => $vi_value) {
                    $ins_menu[$vi_key]['view_right'] = $vi_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($add)) {
                foreach ($add as $ad_key => $ad_value) {
                    $ins_menu[$ad_key]['add_right'] = $ad_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($edit)) {
                foreach ($edit as $ed_key => $ed_value) {
                    $ins_menu[$ed_key]['edit_right'] = $ed_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($delete)) {
                foreach ($delete as $del_key => $del_value) {
                    $ins_menu[$del_key]['delete_right'] = $del_value == 'on' ? 1 : 0;
                }
            }

            if (!empty($ins_menu)) {
                foreach ($ins_menu as $key => $value) {
                    $ins_menu[$key]['ref_menu_id'] = $key;
                    $ins_menu[$key]['access_for'] = 'admin';
                    $user_menu_data[] = $ins_menu[$key];
                }
            }

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $user_id = $this->Common_model->insertInformation($insert_data, 'tbl_user_info');

                $template = $this->Common_model->getSmsTemplate('create_staff');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array($insert_data['user_number'], (isset($insert_data['user_email']) && !empty($insert_data['user_email']) ? ' OR ' . $insert_data['user_email'] : ''), $password, (base_url()));
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($insert_data['user_number'], $template->flow_id, $variable);
                }

                if (isset($insert_data['user_email']) && !empty($insert_data['user_email'])) {
                    $data = array();
                    $data['template'] = $this->Common_model->getEmailTemplate('create_staff');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{user_name}', '{user_email}', '{password}', '{website_link}');
                        $replace = array($insert_data['user_number'], (isset($insert_data['user_email']) && !empty($insert_data['user_email']) ? ' OR ' . $insert_data['user_email'] : ''), $password, (base_url()));
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($insert_data['user_email']);
                    $this->email->send();
                }

                $this->Auth_model->insertSideMenuAccess($user_menu_data, $user_id);
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'user_id', $id, 'tbl_user_info');
                $this->Auth_model->updateSideMenuAccess($user_menu_data, $id, 'admin');
            }
            redirect('admin/Master/staff');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('STAFF', 'ADD');
                $data = [];
                $view = 'admin/master/staff/addStaff';
                $data['side_menu'] = $this->Auth_model->getMenuMSTData('admin');
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $this->page_title = 'STAFF';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('STAFF', 'EDIT');
                $data = [];
                $data['side_menu'] = $this->Auth_model->getMenuAccessById($encrypted_id, 'admin');
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $id, 'Live');
                $view = 'admin/master/staff/editStaff';
                $this->page_title = 'STAFF';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkUserName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getUserName($id);
            $respone = $this->Master_model->checkUserName($this->input->get('user_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkUserName($this->input->get('user_name'));
            echo $response;
            die;
        }
    }

    public function checkUserEmail($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getUserEmail($id);
            $respone = $this->Master_model->checkUserEmail($this->input->get('user_email'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkUserEmail($this->input->get('user_email'));
            echo $response;
            die;
        }
    }

    public function checkUserPhone($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getUserPhone($id);
            $respone = $this->Master_model->checkUserPhone($this->input->get('mobile'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkUserPhone($this->input->get('mobile'));
            echo $response;
            die;
        }
    }

    public function activeDeactiveStaff() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('status', 1)->where('user_id', $id)->update('tbl_user_info');
                $data['result'] = TRUE;
            } else {
                $this->db->set('status', 0)->where('user_id', $id)->update('tbl_user_info');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function deleteStaff() {
        $this->Common_model->check_menu_access('STAFF', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('user_id', $id);
            $this->db->update('tbl_user_info');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function client() {
        $data = [];
        $this->menu_id = 'CLIENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CLIENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CLIENT', 'VIEW');
        $data['user_data'] = $this->Common_model->geAlldata('tbl_client_info');
        $view = 'admin/master/client/client';
        $this->page_title = 'CLIENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditClient($encrypted_id = "") {
        $this->menu_id = 'CLIENT';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = $user_menu_data = array();
            $insert_data['company_name'] = $this->input->post('company_name');
            $insert_data['client_name'] = $this->input->post('client_name');
            $insert_data['client_email'] = $this->input->post('client_email');
            $insert_data['client_number'] = $this->input->post('mobile');
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['client_address'] = $this->input->post('address');
            $insert_data['client_pincode'] = $this->input->post('pincode');
            $insert_data['client_state'] = $this->input->post('state');
            $insert_data['client_city'] = $this->input->post('city');
            $insert_data['is_message_verified'] = 1;
            $insert_data['is_email_verified'] = 1;

            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['client_password'] = md5($password);
                    $insert_data['raw'] = $password;

                    $template = $this->Common_model->getSmsTemplate('change_password');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array($insert_data['client_number'], (isset($insert_data['client_email']) && !empty($insert_data['client_email']) ? ' OR ' . $insert_data['client_email'] : ''), $password, (base_url()));
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($insert_data['client_number'], $template->flow_id, $variable);
                    }

                    if (isset($insert_data['client_email']) && !empty($insert_data['client_email'])) {
                        $data = array();
                        $data['template'] = $this->Common_model->getEmailTemplate('change_password');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{user_name}', '{user_email}', '{password}', '{website_link}');
                            $replace = array($insert_data['client_number'], (isset($insert_data['client_email']) && !empty($insert_data['client_email']) ? ' OR ' . $insert_data['client_email'] : ''), $password, (base_url()));
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($insert_data['client_email']);
                        $this->email->send();
                    }
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Master/addEditClient');
                    } else {
                        redirect('admin/Master/addEditClient/' . $encrypted_id);
                    }
                }
            }

            $full_access = $this->input->post('full_access');
            $view = $this->input->post('view');
            $add = $this->input->post('add');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');

            $ins_menu = [];
            if (!empty($full_access)) {
                foreach ($full_access as $key => $value) {
                    $ins_menu[$key]['full_access'] = $value == 'on' ? 1 : 0;
                }
            }
            if (!empty($view)) {
                foreach ($view as $vi_key => $vi_value) {
                    $ins_menu[$vi_key]['view_right'] = $vi_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($add)) {
                foreach ($add as $ad_key => $ad_value) {
                    $ins_menu[$ad_key]['add_right'] = $ad_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($edit)) {
                foreach ($edit as $ed_key => $ed_value) {
                    $ins_menu[$ed_key]['edit_right'] = $ed_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($delete)) {
                foreach ($delete as $del_key => $del_value) {
                    $ins_menu[$del_key]['delete_right'] = $del_value == 'on' ? 1 : 0;
                }
            }

            if (!empty($ins_menu)) {
                foreach ($ins_menu as $key => $value) {
                    $ins_menu[$key]['ref_menu_id'] = $key;
                    $ins_menu[$key]['access_for'] = 'customer';
                    $user_menu_data[] = $ins_menu[$key];
                }
            }

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $user_id = $this->Common_model->insertInformation($insert_data, 'tbl_client_info');

                $template = $this->Common_model->getSmsTemplate('create_client');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array($insert_data['client_number'], (isset($insert_data['client_email']) && !empty($insert_data['client_email']) ? ' OR ' . $insert_data['client_email'] : ''), $password, (base_url()));
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($insert_data['client_number'], $template->flow_id, $variable);
                }

                if (isset($insert_data['client_email']) && !empty($insert_data['client_email'])) {
                    $data = array();
                    $data['template'] = $this->Common_model->getEmailTemplate('create_client');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{client_name}', '{client_email}', '{password}', '{website_link}');
                        $replace = array($insert_data['client_number'], (isset($insert_data['client_email']) && !empty($insert_data['client_email']) ? ' OR ' . $insert_data['client_email'] : ''), $password, (base_url()));
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($insert_data['client_email']);
                    $this->email->send();
                }

                $this->Auth_model->insertSideMenuAccess($user_menu_data, $user_id);
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'client_id', $id, 'tbl_client_info');
                $this->Auth_model->updateSideMenuAccess($user_menu_data, $id, 'customer');
            }
            redirect('admin/Master/client');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CLIENT', 'ADD');
                $data = [];
                $view = 'admin/master/client/addClient';
                $data['side_menu'] = $this->Auth_model->getMenuMSTData('customer');
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $this->page_title = 'CLIENT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CLIENT', 'EDIT');
                $data = [];
                $data['side_menu'] = $this->Auth_model->getMenuAccessById($encrypted_id, 'customer');
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $id, 'Live');
                $view = 'admin/master/client/editClient';
                $this->page_title = 'CLIENT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkClientName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientName($id);
            $respone = $this->Master_model->checkClientName($this->input->get('client_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkClientName($this->input->get('client_name'));
            echo $response;
            die;
        }
    }

    public function checkcClientPhone($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientPhone($id);
            $respone = $this->Master_model->checkcClientPhone($this->input->get('mobile'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcClientPhone($this->input->get('mobile'));
            echo $response;
            die;
        }
    }

    public function checkcCientEmail($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientEmail($id);
            $respone = $this->Master_model->checkcCientEmail($this->input->get('client_email'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcCientEmail($this->input->get('client_email'));
            echo $response;
            die;
        }
    }

    public function deleteClient() {
        $this->Common_model->check_menu_access('CLIENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('client_id', $id);
            $this->db->update('tbl_client_info');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveClient() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('status', 1)->where('client_id', $id)->update('tbl_client_info');
                $data['result'] = TRUE;
            } else {
                $this->db->set('status', 0)->where('client_id', $id)->update('tbl_client_info');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function getCityByState() {
        $state_id = $this->input->post('state_id');
        $data['city'] = $this->Master_model->getCityByState($state_id);
        if (isset($data['city']) && !empty($data['city'])) {
            $data['result'] = true;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function paymentMethod() {
        $data = [];
        $this->menu_id = 'PAYMENT_METHOD';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PAYMENT_METHOD');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PAYMENT_METHOD', 'VIEW');
        $data['payment_data'] = $this->Common_model->geAlldata('tbl_payment_method');
        $view = 'admin/master/payment/payment';
        $this->page_title = 'PAYMENT METHOD';
        $this->load_admin_view($view, $data);
    }

    public function addEditPaymentMethod($encrypted_id = "") {
        $this->menu_id = 'PAYMENT_METHOD';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['payment_name'] = $this->input->post('payment_name');
            $insert_data['payment_desc'] = $this->input->post('payment_desc');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'tbl_payment_method');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'payment_id', $id, 'tbl_payment_method');
            }
            redirect('admin/Master/paymentMethod');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('PAYMENT_METHOD', 'ADD');
                $data = [];
                $view = 'admin/master/payment/addPayment';
                $this->page_title = 'PAYMENT METHOD';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('PAYMENT_METHOD', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['payment_data'] = $this->Common_model->getDataById2('tbl_payment_method', 'payment_id', $id, 'Live');
                $view = 'admin/master/payment/editPayment';
                $this->page_title = 'PAYMENT METHOD';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkPaymentName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getPaymentName($id);
            $respone = $this->Master_model->checkPaymentName($this->input->get('payment_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkPaymentName($this->input->get('payment_name'));
            echo $response;
            die;
        }
    }

    public function deletePaymentMethod() {
        $this->Common_model->check_menu_access('PAYMENT_METHOD', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('payment_id', $id);
            $this->db->update('tbl_payment_method');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function agency() {
        $data = [];
        $this->menu_id = 'AGENCY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights($this->menu_id);
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access($this->menu_id, 'VIEW');
        $data['user_data'] = $this->Common_model->geAlldata('tbl_agency_info');
        $view = 'admin/master/agency/agency';
        $this->page_title = $this->menu_id;
        $this->load_admin_view($view, $data);
    }

    public function addEditAgency($encrypted_id = "") {
        $this->menu_id = 'AGENCY';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = array();
            $insert_data['agency_name'] = $this->input->post('agency_name');
            $insert_data['agency_email'] = $this->input->post('agency_email');
            $insert_data['agency_number'] = $this->input->post('mobile');
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['agency_address'] = $this->input->post('address');
            $insert_data['agency_pincode'] = $this->input->post('pincode');
            $insert_data['agency_state'] = $this->input->post('state');
            $insert_data['agency_city'] = $this->input->post('city');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'tbl_agency_info');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'agency_id', $id, 'tbl_agency_info');
            }
            redirect('admin/Master/agency');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('AGENCY', 'ADD');
                $data = [];
                $view = 'admin/master/agency/addAgency';
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['agreement_data'] = $this->Master_model->getAgreement();
                $this->page_title = 'AGENCY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('AGENCY', 'EDIT');
                $data = [];
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('tbl_agency_info', 'agency_id', $id, 'Live');
                $view = 'admin/master/agency/editAgency';
                $this->page_title = 'AGENCY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkAgencyName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getAgencyName($id);
            $respone = $this->Master_model->checkAgencyName($this->input->get('agency_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkAgencyName($this->input->get('agency_name'));
            echo $response;
            die;
        }
    }

    public function checkcAgencyPhone($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getAgencyPhone($id);
            $respone = $this->Master_model->checkcAgencyPhone($this->input->get('mobile'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcAgencyPhone($this->input->get('mobile'));
            echo $response;
            die;
        }
    }

    public function checkAgencyEmail($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getAgencyEmail($id);
            $respone = $this->Master_model->checkcAgencyEmail($this->input->get('agency_email'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcAgencyEmail($this->input->get('agency_email'));
            echo $response;
            die;
        }
    }

    public function deleteAgency() {
        $this->Common_model->check_menu_access('CLIENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('agency_id', $id);
            $this->db->update('tbl_agency_info');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveAgency() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('status', 1)->where('agency_id', $id)->update('tbl_agency_info');
                $data['result'] = TRUE;
            } else {
                $this->db->set('status', 0)->where('agency_id', $id)->update('tbl_agency_info');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function thirdParty() {
        $data = [];
        $this->menu_id = 'THIRD_PARTY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights($this->menu_id);
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access($this->menu_id, 'VIEW');
        $data['user_data'] = $this->Common_model->geAlldata('tbl_third_party_info');
        $view = 'admin/master/third_party/third_party';
        $this->page_title = str_replace('_', ' ', $this->menu_id);
        $this->load_admin_view($view, $data);
    }

    public function addEditThirdParty($encrypted_id = "") {
        $this->menu_id = 'THIRD_PARTY';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = array();
            $insert_data['third_party_name'] = $this->input->post('third_party_name');
            $insert_data['company_name'] = $this->input->post('company_name');
            $insert_data['third_party_designation'] = $this->input->post('third_party_designation');
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['third_party_number'] = $this->input->post('mobile');
            $insert_data['third_party_address'] = $this->input->post('address');
            $insert_data['third_party_pincode'] = $this->input->post('pincode');
            $insert_data['third_party_state'] = $this->input->post('state');
            $insert_data['third_party_city'] = $this->input->post('city');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'tbl_third_party_info');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'third_party_id', $id, 'tbl_third_party_info');
            }
            redirect('admin/Master/thirdParty');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('THIRD_PARTY', 'ADD');
                $data = [];
                $view = 'admin/master/third_party/addThirdParty';
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $this->page_title = str_replace('_', ' ', $this->menu_id);
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('THIRD_PARTY', 'EDIT');
                $data = [];
                $data['state'] = $this->Common_model->geAlldata('all_states');
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('tbl_third_party_info', 'third_party_id', $id, 'Live');
                $view = 'admin/master/third_party/editThirdParty';
                $this->page_title = str_replace('_', ' ', $this->menu_id);
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkThirdPartyName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getThirdPartyName($id);
            $respone = $this->Master_model->checkThirdPartyName($this->input->get('third_party_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkThirdPartyName($this->input->get('third_party_name'));
            echo $response;
            die;
        }
    }

    public function checkcThirdPartyPhone($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getThirdPartyPhone($id);
            $respone = $this->Master_model->checkcThirdPartyPhone($this->input->get('mobile'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcThirdPartyPhone($this->input->get('mobile'));
            echo $response;
            die;
        }
    }

    public function deleteThirdParty() {
        $this->Common_model->check_menu_access('THIRD_PARTY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('third_party_id', $id);
            $this->db->update('tbl_third_party_info');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveThirdParty() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('status', 1)->where('third_party_id', $id)->update('tbl_third_party_info');
                $data['result'] = TRUE;
            } else {
                $this->db->set('status', 0)->where('third_party_id', $id)->update('tbl_third_party_info');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}

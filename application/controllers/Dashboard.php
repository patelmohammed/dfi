<?php

class Dashboard extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('client')) {
            redirect();
        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('DASHBOARD');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW');
        $view = 'home/index';
        $data['agreement_count_data'] = $this->Dashboard_model->agreementCount();
        $data['voucher_count_data'] = $this->Dashboard_model->voucherCount();
        $data['client_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $this->user_id, 'Live');
        $this->page_title = 'DASHBOARD';
        $this->load_view($view, $data);
    }

    public function readOrderStatus() {
        $id = $this->user_id;
        if (isset($id) && !empty($id)) {
            $status = $this->Dashboard_model->readOrderStatus($id);
        }
        echo json_encode($status);
        die;
    }

    function getotificationCount() {
        $cnt = 0;
        $id = $this->user_id;
        $cnt = $this->Dashboard_model->getotificationCount($id, 'staff');
        echo json_encode($cnt);
        die;
    }

    public function getOrderNotificationDetail() {
        $id = $this->user_id;
        $view = 'home/order_notification';
        if (isset($id) && !empty($id)) {
            $data['notification_data'] = $this->Dashboard_model->getOrderNotificationDetail($id, 'staff');
            $content = $this->load->view($view, $data, true);
        }
        if (isset($content) && !empty($content)) {
            $data['result'] = true;
            $data['content'] = $content;
        }
        echo json_encode($data);
        die;
    }

    public function deleteNotification() {
        $id = $this->input->post('id');
        $response = FALSE;
        if (isset($id) && !empty($id)) {
            $response = $this->Dashboard_model->deleteNotification($id);
        }
        echo json_encode($response);
        die;
    }

    public function addEditProfile() {
        $this->menu_id = 'PROFILE';
        $id = $this->user_id;
        if ($this->input->post()) {
            $user_info = $this->Dashboard_model->getClientDataById($this->user_id);
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');
            if ((isset($old_password) && !empty($old_password))) {
                if (md5($old_password) == $user_info->user_password) {
                    if ((isset($new_password) && !empty($new_password)) && (isset($confirm_new_password) && !empty($confirm_new_password))) {
                        if ($new_password == $confirm_new_password) {
                            $insert_data['user_password'] = md5($new_password);
                            $insert_data['raw'] = $new_password;
                        } else {
                            $this->_show_message("New Password And Confirm Password not matched", "error");
                            redirect('Dashboard/addEditProfile');
                        }
                    }
                } else {
                    $this->_show_message("Old Password not matched", "error");
                    redirect('Dashboard/addEditProfile');
                }
            }
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['client_number'] = $this->input->post('user_number');
            $user_name = $this->input->post('user_name');
            $insert_data['client_name'] = $user_name;
            if (isset($user_name) && !empty($user_name)) {
                $user_data['user_name'] = $user_name;
                $this->session->set_userdata($user_data);
            }

            $new_path = 'assets/img/demo/avatars/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['profile_photo']['name']) && isset($_FILES['profile_photo']['name'])) {
                if ($_FILES['profile_photo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('profile_photo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('Dashboard/addEditProfile');
                    } else {
                        $image = $this->upload->data();
                        $profile_photo_url = $new_path . $image['file_name'];

                        $user_data['profile_photo'] = $profile_photo_url;
                        $this->session->set_userdata($user_data);
                    }
                } else {
                    $profile_photo_url = $this->input->post('hidden_profile_photo');
                }
            } else {
                $profile_photo_url = $this->input->post('hidden_profile_photo');
            }
            $insert_data['profile_photo'] = $profile_photo_url;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->_show_message("Your profile updated successfully", "success");
                $this->Common_model->updateInformation2($insert_data, 'client_id', $id, 'tbl_client_info');
            }
            redirect('Dashboard');
        } else {
            if (isset($id) && !empty($id)) {
                $data = [];
                $data['user_data'] = $this->Dashboard_model->getClientDataById($this->user_id);
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $view = 'home/editProfile';
                $this->page_title = 'PROFILE';
                $this->load_view($view, $data);
            } else {
                $this->_show_message("You cant insert new profile", "error");
                redirect('Dashboard');
            }
        }
    }

}

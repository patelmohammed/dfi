<?php

class Agreement extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('client')) {
            redirect();
        }
        $this->page_id = 'AGREEMENT';
        $this->load->model('Agreement_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'AGREEMENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('AGREEMENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('AGREEMENT', 'VIEW');
        $data['agreement_all_data'] = $this->Agreement_model->geClientAgreement();
        $view = 'agreement/agreement';
        $this->page_title = 'AGREEMENT';
        $this->load_view($view, $data);
    }

    public function agreementDetail($id = NULL, $hash = NULL) {
        $this->menu_id = 'AGREEMENT';
        $data = [];
        if (isset($id) && !empty($id) && isset($hash) && !empty($hash)) {
            $data['agreement_data'] = $this->Agreement_model->getAgreementbyId($id, $hash);
            $data['client_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $data['agreement_data']->client, 'Live');
            if (isset($data['agreement_data']) && !empty($data['agreement_data'])) {
                $data['menu_rights'] = $this->Common_model->get_menu_rights('AGREEMENT');
                if (empty($data['menu_rights'])) {
                    redirect('admin/Auth/Unauthorized');
                }
                $this->Common_model->check_menu_access('AGREEMENT', 'VIEW');
                $view = 'agreement/agreement_detail';
                $this->page_title = 'AGREEMENT DETAIL';
                $this->load_view($view, $data);
            } else {
                $this->_show_message("Agreement not fount.", "error");
                redirect('Agreement');
            }
        } else {
            $this->_show_message("Agreement not fount.", "error");
            redirect('Agreement');
        }
    }

    public function agreementSign($id = NULL, $hash = NULL) {
        $this->menu_id = 'AGREEMENT';
        $data = [];
        if (isset($id) && !empty($id) && isset($hash) && !empty($hash)) {
            if ($this->input->post()) {
                $response = process_digital_signature_image($this->input->post('signature', false), 'assets/img/signature/client/' . $this->user_id);
                if ($response['retval'] == TRUE) {
                    $agreement_data = $this->Agreement_model->getAgreementbyId($id, $hash);
                    $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $agreement_data->client, 'Live');
//                    $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $agreement_data->InsUser, 'Live');
                    $user_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', 2, 'Live');

                    $insert_data['signature'] = $response['filename'];
                    $insert_data['signature_path'] = $response['filename_path'];
                    $insert_data['acceptance_firstname'] = $this->input->post('first_name');
                    $insert_data['acceptance_lastname'] = $this->input->post('last_name');
                    $insert_data['acceptance_email'] = $this->input->post('email');
                    $insert_data['acceptance_date'] = date('Y/m/d H:i:s');
                    $insert_data['acceptance_ip'] = $this->input->ip_address();
                    $insert_data['signed'] = 1;
                    $this->Common_model->updateInformation2($insert_data, 'agreement_id', $id, 'tbl_contracts', 'Live', 'hash', $hash);

                    $body = '';
                    $template = $this->Common_model->getSmsTemplate('client_agreement_signed');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array($client_data->client_name, $agreement_data->subject);
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($user_data->user_number, $template->flow_id, $variable);
                    }

                    if (isset($user_data->user_email) && !empty($user_data->user_email)) {
                        $data = array();
                        $data['template'] = $this->Common_model->getEmailTemplate('client_agreement_signed');
                        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                        $html_template = $this->load->view('email_template', $data, true);

                        $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                        $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                        $this->email->from($from_email, $from_name);
                        $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                        if (isset($data['template']) && !empty($data['template'])) {
                            $word = array('{client_name}', '{agreement_name}');
                            $replace = array($client_data->client_name, $agreement_data->subject);
                            $body = str_replace($word, $replace, $html_template);
                        }
                        $this->email->message($body);
                        $this->email->to($user_data->user_email);
                        $this->email->send();
                    }

                    $notification_data = array();
                    $notification_data['datetime'] = date('Y-m-d H:i:s');
                    $notification_data['ref_client_id'] = $client_data->client_id;
                    $notification_data['ref_client_name'] = $client_data->client_name;
                    $notification_data['ref_staff_id'] = $agreement_data->InsUser;
                    $notification_data['ref_staff_name'] = getUserName($agreement_data->InsUser);
                    $notification_data['ref_agreement_id'] = $id;
                    $notification_data['ref_agreement_hash'] = $hash;
                    $notification_data['ref_agreement_subject'] = $agreement_data->subject;
                    $notification_data['noti_from'] = 'client';
                    $this->Common_model->insertInformation($notification_data, 'sign_notification');

                    $this->_show_message("Agreement singed successfully.", "success");
                    redirect('Agreement/agreementDetail/' . $id . '/' . $hash);
                } else {
                    $this->_show_message("Something went wrong please try again.", "error");
                    redirect('Agreement/agreementDetail/' . $id . '/' . $hash);
                }
            } else {
                $this->_show_message("Something went wrong please try again.", "error");
                redirect('Agreement/agreementDetail/' . $id . '/' . $hash);
            }
        } else {
            $this->_show_message("Agreement not fount.", "error");
            redirect('Agreement');
        }
    }

    public function agreementDocument($agreement_id = NULL) {
        $data = [];
        $this->menu_id = 'AGREEMENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('AGREEMENT');
        if (empty($data['menu_rights'])) {
            redirect('Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('AGREEMENT', 'VIEW');
        $data['agreement_document'] = $this->Agreement_model->getAgreementDocumentClient($agreement_id);
        $data['agreement_id'] = $agreement_id;
        $view = 'agreement/agreement_document/agreement_document';
        $this->page_title = 'MANAGE AGREEMENT DOCUMENT';
        $this->load_admin_view($view, $data);
    }

}

<?php

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Master_model');
        $this->load->model('Dashboard_model');
        $this->data['page_id'] = '';
    }

    public function index() {
        if ($this->is_login('client')) {
            $this->session->set_flashdata('message', $this->session->flashdata('message'));
            redirect(base_url('dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $usr_data = $this->Auth_model->getUserByEmailClient($username);
            if (!empty($usr_data)) {
                if ($usr_data->is_message_verified == 1) {
                    if ($this->check_pwd($password, $usr_data->password)) {
                        $user_data = array();
                        $user_data['user_id'] = $usr_data->user_id;
                        $user_data['user_name'] = $usr_data->name;
                        $user_data['company_name'] = $usr_data->company_name;
                        $user_data['user_email'] = $usr_data->email;
                        $user_data['user_phone'] = $usr_data->contact_number;
                        $user_data['login_type'] = 'client';
                        $user_data['profile_photo'] = $usr_data->profile_photo;

                        $menu_data = $this->Auth_model->get_assigned_menu($usr_data->user_id, $usr_data->user_role, 'customer');
                        $user_data['side_menu'] = $menu_data;
                        $this->session->set_userdata($user_data);
                        if ($usr_data->is_email_verified == 0 && isset($usr_data->email) && !empty($usr_data->email)) {
                            $this->_show_message("Verify your email address as soon as possible.", "warning");
                        }
                        redirect(base_url('Dashboard'));
                    } else {
                        $this->session->sess_destroy();
                        $this->_show_message("wrong password", "error");
                    }
                } else {
                    if (isset($usr_data->country_id) && !empty($usr_data->country_id)) {
                        $country_code = getCountryCode($usr_data->country_id);
                    } else {
                        $country_code = '91';
                    }
                    $contact_number = $country_code . $usr_data->contact_number;

                    $otp_response = $this->sendOtp($contact_number);
                    $otp_response = json_decode($otp_response);
                    if (isset($otp_response->type) && !empty($otp_response->type) && $otp_response->type == 'success') {
                        $user_data['contact_number'] = $contact_number;
                        $user_data['contact_id'] = $usr_data->user_id;
                        $this->session->set_userdata($user_data);
                        $this->_show_message("OTP sent your contact number.", "success");
                        redirect('Auth/otpVerify');
                    } else {
                        $this->_show_message($otp_response->message, "error");
                    }
                }
            } else {
                $this->session->sess_destroy();
                $this->_show_message("wrong username", "error");
            }
        }
        $this->page_id = 'login_pg';
        $this->page_title = 'Login';
        $view = 'auth/login';
        $this->load_view($view, $data, false);
    }

    public function registerCustomer() {
        if ($this->is_login('client')) {
            redirect(base_url('dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $insert_data['company_name'] = $this->input->post('company_name');
            $insert_data['client_name'] = $this->input->post('client_name');
            $client_email = $this->input->post('client_email');
            $insert_data['client_email'] = $client_email;
            $client_number = $this->input->post('mobile');
            $insert_data['client_number'] = $client_number;
            $country_id = $this->input->post('country_id');
            $insert_data['country_id'] = $country_id;
            $insert_data['client_address'] = $this->input->post('address');
            $insert_data['client_pincode'] = $this->input->post('pincode');
            $insert_data['client_state'] = $this->input->post('state');
            $insert_data['client_city'] = $this->input->post('city');
            $insert_data['is_message_verified'] = 0;
            $insert_data['is_email_verified'] = 0;

            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['client_password'] = md5($password);
                    $insert_data['raw'] = $password;
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    redirect('Auth/registerCustomer');
                }
            }
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
            $user_id = $this->Common_model->insertInformation($insert_data, 'tbl_client_info');

            if (isset($user_id) && !empty($user_id)) {
                $this->Common_model->insertClientSideMenuRight($user_id);
                $token = sha1(mt_rand(1, 90000) . 'SALT');

                if (isset($client_email) && !empty($client_email)) {
                    $data['template'] = $this->Common_model->getEmailTemplate('client_email_verification');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $this->email->from($from_email, COMPANY_NAME);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{base_url}', '{client_email}', '{token}');
                        $replace = array(base_url(), $client_email, $token);
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($client_email);
                    if ($this->email->send()) {
                        $insert_data = array();
                        $insert_data['InsUser'] = $user_id;
                        $insert_data['email_token'] = $token;
                        $this->Common_model->updateInformation2($insert_data, 'client_id', $user_id, 'tbl_client_info');
                    }
                }

                if (isset($country_id) && !empty($country_id)) {
                    $country_code = getCountryCode($country_id);
                } else {
                    $country_code = '91';
                }
                $contact_number = $country_code . $client_number;

                $otp_response = $this->sendOtp($contact_number);
                $otp_response = json_decode($otp_response);
                if (isset($otp_response->type) && !empty($otp_response->type) && $otp_response->type == 'success') {
                    $user_data['contact_number'] = $contact_number;
                    $user_data['contact_id'] = $user_id;
                    $this->session->set_userdata($user_data);
                    redirect('Auth/otpVerify');
                } else {
                    $this->_show_message($otp_response->message, "error");
                }
            }
            redirect('Auth');
        }
        $data['state'] = $this->Common_model->geAlldata('all_states');
        $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
        $this->page_id = 'register_pg';
        $this->page_title = 'Register';
        $view = 'auth/register';
        $this->load_view($view, $data, false);
    }

    public function otpVerify() {
        if ($this->is_login('client')) {
            redirect(base_url('dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $contact_number = $this->input->post('contact_number');
            $contact_id = $this->input->post('contact_id');
            $otp = $this->input->post('verify_otp');

            $verify_response = $this->verifyOtp($contact_number, $otp);
            $verify_response = json_decode($verify_response);
            if (isset($verify_response->type) && !empty($verify_response->type) && $verify_response->type == 'success') {
                $insert_data = array();
                $insert_data['is_message_verified'] = 1;
                $this->Common_model->updateInformation2($insert_data, 'client_id', $contact_id, 'tbl_client_info');
                $user_data = $this->Dashboard_model->getClientDataById($contact_id);

                $body = '';
                $template = $this->Common_model->getSmsTemplate('client_welcome');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array($client_data->client_name, $agreement_data->subject);
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($user_data->user_number, $template->flow_id, $variable);
                }

                $admin_data = $this->Common_model->getDataById2('tbl_user_info', 'user_id', 2, 'Live');

                $body = '';
                $template = $this->Common_model->getSmsTemplate('client_registered');
                if (isset($template->flow_id) && !empty($template->flow_id)) {
                    $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                    $variable = '';
                    if (isset($template_veriables) && !empty($template_veriables)) {
                        $variable .= '{';
                        foreach ($template_veriables as $key => $value) {
                            if ($key != 0) {
                                $variable .= ', ';
                            }
                            $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                        }
                        $variable .= '}';

                        $word = array_column($template_veriables, 'variable_value');
                        $replace = array('Client', $user_data->user_name, $user_data->user_number, (isset($user_data->user_email) && !empty($user_data->user_email) ? $user_data->user_email : ''));
                        $variable = str_replace($word, $replace, $variable);
                    }
                    $this->sendMessage($admin_data->user_number, $template->flow_id, $variable);
                }

                if (isset($admin_data->user_email) && !empty($admin_data->user_email)) {
                    $data = array();
                    $data['template'] = $this->Common_model->getEmailTemplate('client_registered');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{client_name}', '{contact_number}', '{email_address}');
                        $replace = array($user_data->user_name, $user_data->user_number, (isset($user_data->user_email) && !empty($user_data->user_email) ? $user_data->user_email : ''));
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($admin_data->user_email);
                    $this->email->send();
                }



                $session_data = array();
                $session_data['user_id'] = $user_data->user_id;
                $session_data['user_name'] = $user_data->user_name;
                $session_data['user_email'] = $user_data->user_email;
                $session_data['user_phone'] = $user_data->user_number;
                $session_data['login_type'] = 'client';
                $session_data['profile_photo'] = $user_data->profile_photo;

                $menu_data = $this->Auth_model->get_assigned_menu($user_data->user_id, $user_data->user_role, 'customer');
                $session_data['side_menu'] = $menu_data;
                $this->session->set_userdata($session_data);
                if ($user_data->is_email_verified == 0 && isset($user_data->user_email) && !empty($user_data->user_email)) {
                    $this->_show_message("Verify your email address as soon as possible.", "warning");
                }
                redirect(base_url('dashboard'));
            } else {
                $session_data = array();
                $session_data['contact_number'] = $contact_number;
                $session_data['contact_id'] = $contact_id;
                $this->session->set_userdata($session_data);
                $this->_show_message($verify_response->message, "error");
                redirect('Auth/otpVerify');
            }
        }
        $data['contact_number'] = $this->session->userdata('contact_number');
        $data['contact_id'] = $this->session->userdata('contact_id');
        if (isset($data['contact_number']) && !empty($data['contact_number']) && isset($data['contact_id']) && !empty($data['contact_id'])) {
            $this->session->unset_userdata('contact_number');
            $this->session->unset_userdata('contact_id');
            $this->page_id = 'verify_pg';
            $this->page_title = 'Verify OTP';
            $view = 'auth/verifyotp';
            $this->load_view($view, $data, false);
        } else {
            $this->_show_message("Sometjing went wrong.", "error");
            redirect('Auth');
        }
    }

    public function resend_otp() {
        $data = array();
        $contact_number = $this->input->post('contact_number');
        $resend_response = $this->resendOtp($contact_number);
        $resend_response = json_decode($resend_response);
        if (isset($resend_response->type) && !empty($resend_response->type) && $resend_response->type == 'success') {
            $data['message'] = $resend_response->message;
            $data['result'] = true;
        } else {
            $data['message'] = $resend_response->message;
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function verifyEmail() {
        $email = $this->input->get('email');
        $hash = $this->input->get('hash');
        if (isset($email) && !empty($email) && isset($hash) && !empty($hash)) {
            $usr_data = $this->Auth_model->getUserByEmailClient($email);
            if ($usr_data->is_email_verified == 0) {
                if ($hash == $usr_data->email_token) {
                    $insert_data = array();
                    $insert_data['is_email_verified'] = 1;
                    $insert_data['email_token'] = NULL;
                    $this->Common_model->updateInformation2($insert_data, 'client_id', $usr_data->user_id, 'tbl_client_info');
                    $this->_show_message("Email Varification successful.", "success");
                    redirect('Auth');
                } else {
                    $this->_show_message("Something went wrong, please try again.", "error");
                    redirect('Auth');
                }
            } else {
                $this->_show_message("Already verified.", "success");
                redirect('Auth');
            }
        } else {
            $this->_show_message("Something went wrong.", "error");
            redirect('Auth');
        }
    }

    public function resendVarificationMail() {
        if (!$this->is_login('client')) {
            redirect();
        }
        $data = [];
        $client_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $this->user_id, 'Live');
        if (isset($client_data) && !empty($client_data) && isset($client_data->client_email) && !empty($client_data->client_email) && $client_data->is_email_verified == 0) {
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            $data['template'] = $this->Common_model->getEmailTemplate('client_email_verification');
            $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $this->email->from($from_email, COMPANY_NAME);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{base_url}', '{client_email}', '{token}');
                $replace = array(base_url(), $client_data->client_email, $token);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);
            $this->email->to($client_data->client_email);
            if ($this->email->send()) {
                $insert_data = array();
                $insert_data['email_token'] = $token;
                $this->Common_model->updateInformation2($insert_data, 'client_id', $client_data->client_id, 'tbl_client_info');

                log_message('ERROR', print_r($this->email->print_debugger(), true));

                $data['message'] = "Varification Email successfully sent to your email.";
                $data['result'] = true;
            } else {
                $data['message'] = "Something went wrong";
                $data['result'] = false;
            }
        } else {
            $data['message'] = "Something went wrong";
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function checkClientName($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientName($id);
            $respone = $this->Master_model->checkClientName($this->input->get('client_name'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkClientName($this->input->get('client_name'));
            echo $response;
            die;
        }
    }

    public function checkcClientPhone($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientPhone($id);
            $respone = $this->Master_model->checkcClientPhone($this->input->get('mobile'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcClientPhone($this->input->get('mobile'));
            echo $response;
            die;
        }
    }

    public function checkcCientEmail($id = '') {
        if ($id != '') {
            $name = $this->Master_model->getClientEmail($id);
            $respone = $this->Master_model->checkcCientEmail($this->input->get('client_email'), $name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkcCientEmail($this->input->get('client_email'));
            echo $response;
            die;
        }
    }

    function check_pwd($password, $db) {
        return md5($password) == $db;
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('Auth');
    }

    public function forgotPassword() {
        if ($this->is_login('client')) {
            redirect(base_url('dashboard'));
        }
        $data = [];
        $show_message = '';
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $usr_data = $this->Auth_model->getUserByEmailClient($username);
            if (!empty($usr_data)) {
                $is_mail_or_sms_sent = FALSE;
                $token = sha1(mt_rand(1, 90000) . 'SALT');
                if (isset($usr_data->email) && !empty($usr_data->email)) {

                    $data['template'] = $this->Common_model->getEmailTemplate('client_forgot_password');
                    $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
                    $html_template = $this->load->view('email_template', $data, true);

                    $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                    $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                    $this->email->from($from_email, $from_name);
                    $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));

                    if (isset($data['template']) && !empty($data['template'])) {
                        $word = array('{base_url}', '{client_id}', '{token}');
                        $replace = array(base_url(), $usr_data->user_id, $token);
                        $body = str_replace($word, $replace, $html_template);
                    }
                    $this->email->message($body);
                    $this->email->to($usr_data->email);
                    if ($this->email->send()) {
                        $is_mail_or_sms_sent = TRUE;
                        $show_message .= 'Forgot password link sent to your mail.';
                    }
                }

                if (isset($usr_data->contact_number) && !empty($usr_data->contact_number) && isset($usr_data->country_id) && !empty($usr_data->country_id)) {
                    $template = $this->Common_model->getSmsTemplate('client_forgot_password');
                    if (isset($template->flow_id) && !empty($template->flow_id)) {
                        $template_veriables = $this->Common_model->geAlldataById('tbl_sms_template_variable', 'ref_template_id', $template->sms_template_id);

                        $variable = '';
                        if (isset($template_veriables) && !empty($template_veriables)) {
                            $variable .= '{';
                            foreach ($template_veriables as $key => $value) {
                                if ($key != 0) {
                                    $variable .= ', ';
                                }
                                $variable .= '"' . $value->variable_name . '": "' . $value->variable_value . '"';
                            }
                            $variable .= '}';

                            $word = array_column($template_veriables, 'variable_value');
                            $replace = array(base_url(), $usr_data->user_id, $token);
                            $variable = str_replace($word, $replace, $variable);
                        }
                        $this->sendMessage($usr_data->contact_number, $template->flow_id, $variable);
                        $is_mail_or_sms_sent = TRUE;
                        $show_message .= '<br>Forgot password link sent to your contact number.';
                    }
                }

                if ($is_mail_or_sms_sent == TRUE) {
                    $insert_data = array();
                    $insert_data['reset_password_token'] = $token;
                    $this->Common_model->updateInformation2($insert_data, 'client_id', $usr_data->user_id, 'tbl_client_info');
                }

                $this->_show_message($show_message, "success");
                redirect(base_url('Auth'));
            } else {
                $this->session->sess_destroy();
                $this->_show_message("wrong username", "error");
            }
        }
        $this->page_id = 'forgot_pass';
        $this->page_title = 'Forgot Password';
        $view = 'auth/forgot_password';
        $this->load_view($view, $data, false);
    }

    public function resetPassword($id = NULL, $token = NULL) {
        if ($this->is_login('client')) {
            redirect(base_url('dashboard'));
        }
        if (isset($id) && !empty($id)) {
            $user_data = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $id, 'Live');
            if (isset($user_data) && !empty($user_data)) {
                if (isset($token) && !empty($token) && isset($user_data->reset_password_token) && !empty($user_data->reset_password_token) && $user_data->reset_password_token == $token) {
                    $data = [];
                    if ($this->input->post()) {
                        $password = $this->input->post('password');
                        $confirm_password = $this->input->post('confirm_password');
                        if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                            if ($password == $confirm_password) {
                                $insert_data['client_password'] = md5($password);
                                $insert_data['raw'] = $password;
                                $insert_data['reset_password_token'] = NULL;
                                $this->Common_model->updateInformation2($insert_data, 'client_id', $id, 'tbl_client_info');
                                $this->_show_message("Password reset successfully.", "success");
                                redirect(base_url('Auth'));
                            } else {
                                $this->_show_message("Password And Confirm Password not matched", "error");
                                redirect(base_url('Auth/resetPassword/') . $id . '/' . $token);
                            }
                        }
                    }
                    $data['id'] = $id;
                    $data['reset_password_token'] = $token;
                    $this->page_id = 'reset_password';
                    $this->page_title = 'Reset Password';
                    $view = 'auth/reset_password';
                    $this->load_view($view, $data, false);
                } else {
                    $this->_show_message("Token mismatched.", "error");
                    redirect(base_url());
                }
            } else {
                $this->_show_message("User not found.", "error");
                redirect(base_url());
            }
        } else {
            $this->_show_message("User not found.", "error");
            redirect(base_url());
        }
    }

    public function Unauthorized() {
        $data['title'] = 'Access Denied';
        $this->load_view("home/access_denied", $data);
    }

    public function getCityByState() {
        $state_id = $this->input->post('state_id');
        $data['city'] = $this->Master_model->getCityByState($state_id);
        if (isset($data['city']) && !empty($data['city'])) {
            $data['result'] = true;
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function printAgreement($id = NULL, $hash = NULL) {
        $this->load->model('Agreement_model');
        $this->menu_id = 'AGREEMENT';
        $data = [];
        if (isset($id) && !empty($id) && isset($hash) && !empty($hash)) {
            $data['agreement_data'] = $this->Agreement_model->getAgreementbyId($id, $hash);
            $code = $this->Common_model->getCompanyCode('agreement_code');
            $company_code = strtoupper($code);
            $data['agreement_data']->agreement_no = $company_code . "-" . sprintf('%06d', $data['agreement_data']->agreement_no);
            $data['client_data'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $data['agreement_data']->client, 'Live');
            $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
            if (isset($data['agreement_data']) && !empty($data['agreement_data'])) {
                try {
                    $pdfhtml = $this->load->view('pdf/Agreement', $data, true);
                    $pdfName = cleanString($data['agreement_data']->agreement_no) . '.pdf';
                    $html2pdf = new Html2Pdf('P', 'A4', 'en');
                    $html2pdf->pdf->SetDisplayMode('fullpage');
                    $html2pdf->writeHTML($pdfhtml);
                    $html2pdf->output($pdfName, 'D');
                } catch (Html2PdfException $e) {
                    $data = [];
                    $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                    echo json_encode($data);
                    die;
                }
            } else {
                $this->_show_message("Agreement not fount.", "error");
                redirect('Agreement');
            }
        } else {
            $this->_show_message("Agreement not fount.", "error");
            redirect('Agreement');
        }
    }

}

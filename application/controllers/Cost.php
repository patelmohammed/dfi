<?php

class Cost extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('client')) {
            redirect();
        }
        $this->page_id = 'COST';
        $this->load->model('Common_model');
        $this->load->model('Cost_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'COST';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('COST');
        if (empty($data['menu_rights'])) {
            redirect('Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('COST', 'VIEW');
        $data['cost_all_data'] = $this->Cost_model->getCost($this->user_id);
        $view = 'cost/cost';
        $this->page_title = 'COST';
        $this->load_view($view, $data);
    }

}

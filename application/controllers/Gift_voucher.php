<?php

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Gift_voucher extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('client')) {
            redirect();
        }
        $this->page_id = 'GIFT_VOUCHER';
        $this->load->model('Agreement_model');
        $this->load->model('Gift_voucher_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'GIFT_VOUCHER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MANAGE_GIFT_VOUCHER');
        if (empty($data['menu_rights'])) {
            redirect('Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MANAGE_GIFT_VOUCHER', 'VIEW');
        $data['voucher_data'] = $this->Gift_voucher_model->getGiftVoucher();
        $view = 'gift_voucher/gift_voucher';
        $this->page_title = 'MANAGE GIFT VOUCHER';
        $this->load_view($view, $data);
    }

    function printPdfVoucher($transaction_id) {
        $data = array();
        $data['payment_data'] = $this->Gift_voucher_model->getVoucherDataById($transaction_id);
        $data['transaction_type'] = $this->Gift_voucher_model->selected_transaction_type($transaction_id);
        $data['user_detail'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $data['transaction_type']->InsUser, 'Live');
        $data['customer_detail'] = $this->Common_model->getDataById2('tbl_client_info', 'client_id', $data['transaction_type']->client_id, 'Live');
        $data['totalDue'] = $this->Gift_voucher_model->getDueChargesById($data['transaction_type']->client_id, $data['payment_data']->datetime);
        $data['payment_method'] = $this->Common_model->geAlldata('tbl_payment_method');

        $code = $this->Common_model->getCompanyCode('voucher_code');
        $company_code = strtoupper($code);
        $data['payment_data']->voucher_no = $company_code . "-" . sprintf('%06d', $data['payment_data']->voucher_no);

        if (!empty($data)) {
            try {
                $pdfhtml = $this->load->view('pdf/voucher', $data, true);
                $pdfName = cleanString($data['payment_data']->voucher_no) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

}

<?php

Class My_Controller extends CI_Controller {

    public $page_id;
    public $page_title;
    public $menu_id;
    public $sub_menu_id;
    public $add_message;
    public $user_id;
    public $user_type;

    public function __construct() {
        parent::__construct();
        $this->page_id = '';
        $this->page_title = '';
        $this->menu_id = '';
        $this->sub_menu_id = '';
        $this->user_id = $this->session->userdata('user_id');
        $this->user_type = $this->session->userdata('login_type');
        $this->role = $this->session->userdata('role');
        $this->load->model('Common_model');
        $smtp_setting = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $smtp_setting->smtp_host,
            'smtp_port' => $smtp_setting->smtp_port,
            'smtp_user' => $smtp_setting->smtp_user,
            'smtp_pass' => $smtp_setting->smtp_pass,
            'smtp_crypto' => $smtp_setting->smtp_crypto,
            'mailtype' => 'html',
            'smtp_timeout' => '100',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
    }

    public function load_view($view, $data, $flag = true) {
        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
        $data['add_message'] = $this->session->flashdata('message');
        if ($flag) {
            $this->load->view('home/header', $data);
            $this->load->view('home/side_menu', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('home/footer', $data);
        }
    }

    public function load_admin_view($view, $data, $flag = true) {
        $data['company_data'] = $this->Common_model->getDataById2('company_information', 'del_status', 'Live', 'Live');
        $data['add_message'] = $this->session->flashdata('message');
        $data['user_info'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->user_id, 'Live');
        if ($flag) {
            $this->load->view('admin/home/header', $data);
            $this->load->view('admin/home/side_menu', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('admin/home/footer', $data);
        }
    }

    public function _preprint($data, $flag = true) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if ($flag) {
            die;
        }
    }

    public function is_login($type = NULL) {
        $dat = $this->user_id;
        $user_type = $this->user_type;
//        if (!empty($dat)) {
        if (!empty($dat) && $user_type == $type) {
            return true;
        }
        return false;
    }

    public function _show_message($message, $type = 'message') {
        $message = 'toastr["' . $type . '"]("' . $message . '");';
        $this->session->set_flashdata('message', $message);
    }

    public function sendMessage($contact_number, $flow_id, $extra_param) {
//        $sms_setting_data = $this->Common_model->getDataById2('tbl_sms_setting', 'del_status', 'Live', 'Live');
        $post_data = array(
            "authkey" => '341380A94flRwVU5f5b3930P1',
            "sender" => 'WEBOIT',
            "template_id" => $flow_id,
            "mobiles" => '91' . $contact_number
        );

        if (isset($extra_param) && !empty($extra_param)) {
            if (isJSON($extra_param)) {
                $extra_param = json_decode($extra_param, true);
                $post_data = array_merge($post_data, $extra_param);
            } else {
                $post_data = array_merge($post_data, $extra_param);
            }
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://control.msg91.com/api/v5/flow",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post_data),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: a44a7dd0-b972-3d2d-e390-ba58a72dac69"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $response = json_encode(array("type" => "error", "message" => $err));
        }
        if (isset($response) && !empty($response)) {
            $tmp_response = json_decode($response);

            $insert_data = array();
            $insert_data['contact_number'] = $contact_number;
            $insert_data['message'] = 'Message';
            $insert_data['response'] = $response;
            $insert_data['response_message'] = (isset($tmp_response->message) && !empty($tmp_response->message) ? $tmp_response->message : NULL);
            $insert_data['response_type'] = isset($tmp_response->type) && !empty($tmp_response->type) ? $tmp_response->type : NULL;
            $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
            $this->db->insert('tbl_sms_history', $insert_data);
        }
        return $response;
    }

    public function sendOtp($contact_number) {
        $contact_number = substr($contact_number, -10);
        if (isset($contact_number) && !empty($contact_number) && strlen($contact_number) == 10) {
            $curl = curl_init();

            $post_fields = array(
            );

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.msg91.com/api/v5/otp?template_id=5f6302e863583b5a106feb47&mobile=91$contact_number&authkey=341380A94flRwVU5f5b3930P1",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => json_encode($post_fields),
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $response = json_encode(array("type" => "error", "message" => $err));
            }

            if (isset($response) && !empty($response)) {
                $tmp_response = json_decode($response);

                $insert_data = array();
                $insert_data['contact_number'] = $contact_number;
                $insert_data['message'] = 'OTP Message';
                $insert_data['response'] = $response;
                $insert_data['response_message'] = (isset($tmp_response->message) && !empty($tmp_response->message) ? $tmp_response->message : (isset($tmp_response->request_id) && !empty($tmp_response->request_id) ? $tmp_response->request_id : NULL));
                $insert_data['response_type'] = isset($tmp_response->type) && !empty($tmp_response->type) ? $tmp_response->type : NULL;
                $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->db->insert('tbl_sms_history', $insert_data);
            }
        } else {
            $response = json_encode(array("type" => "error", "message" => 'Please enter valid number.'));
        }
        return $response;
    }

    public function verifyOtp($contact_number, $otp) {
        $contact_number = substr($contact_number, -10);
        if (isset($contact_number) && !empty($contact_number) && strlen($contact_number) == 10) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.msg91.com/api/v5/otp/verify?otp=$otp&authkey=341380A94flRwVU5f5b3930P1&mobile=91$contact_number",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "postman-token: 5e0f7165-6cbe-0686-a977-fd104ac31056"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $response = json_encode(array("type" => "error", "message" => $err));
            }
        } else {
            $response = json_encode(array("type" => "error", "message" => 'Please enter valid number.'));
        }
        return $response;
    }

    public function resendOtp($contact_number) {
        $contact_number = substr($contact_number, -10);
        if (isset($contact_number) && !empty($contact_number) && strlen($contact_number) == 10) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.msg91.com/api/v5/otp/retry?authkey=341380A94flRwVU5f5b3930P1&mobile=91$contact_number&retrytype=voice",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: PHPSESSID=bm8a3cqbih70rl36rhf0ie6042'
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $response = json_encode(array("type" => "error", "message" => $err));
            }

            if (isset($response) && !empty($response)) {
                $tmp_response = json_decode($response);

                $insert_data = array();
                $insert_data['contact_number'] = $contact_number;
                $insert_data['message'] = 'Resend OTP Message';
                $insert_data['response'] = $response;
                $insert_data['response_message'] = (isset($tmp_response->message) && !empty($tmp_response->message) ? $tmp_response->message : (isset($tmp_response->request_id) && !empty($tmp_response->request_id) ? $tmp_response->request_id : NULL));
                $insert_data['response_type'] = isset($tmp_response->type) && !empty($tmp_response->type) ? $tmp_response->type : NULL;
                $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->db->insert('tbl_sms_history', $insert_data);
            }
        } else {
            $response = json_encode(array("type" => "error", "message" => 'Please enter valid number.'));
        }
        return $response;
    }

}

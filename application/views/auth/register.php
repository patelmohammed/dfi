<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            Register - Design Factory Interior
        </title>
        <meta name="description" content="Register Design Factory Interior">
        <meta name="author" content="Weborative IT Consultancy LLP">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Design Factory Interior" />
        <meta property="og:description" content="" />
        <meta property="og:site_name" content="Design Factory Interior" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>favicon-16x16.png">
        <link rel="mask-icon" href="<?= base_url('assets') ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/fa-brands.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/formplugins/select2/select2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/notifications/toastr/toastr.css">
        <style>
            .bg-transparent{
                background-color: #5b5b5b !important;
            }
        </style>
    </head>
    <body>
        <div class="page-wrapper">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="flex-1" style="background: url(<?= base_url('assets') ?>/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">
                        <div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
                            <div class="row">
                                <div class="col-xl-12 ml-auto mr-auto">
                                    <div class="m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
                                        <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                                            <!--<img src="<?= base_url() ?>assets/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo" style="margin: 0 auto;">-->
                                            <img src="<?= base_url() ?><?= isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? $company_data->company_logo : '' ?>" alt="Design Factory Client Panel" aria-roledescription="logo" style="margin: 0 auto;">
                                        </a>
                                    </div>
                                    <div class="card p-4 rounded-plus bg-faded">
                                        <?php echo form_open(base_url() . 'Auth/registerCustomer', $arrayName = array('id' => 'registerCustomer', 'enctype' => 'multipart/form-data')) ?>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="company_name">Company Name / Type of Project <span class="text-danger">*</span></label>
                                                <input tabindex="2" type="text" class="form-control textonly" name="company_name" id="company_name" placeholder="Company Name" required>
                                                <div class="invalid-feedback">
                                                    Company Name / Type of Project Required / Already Exist
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="client_name">Client Name <span class="text-danger">*</span></label>
                                                <input tabindex="2" type="text" class="form-control textonly" name="client_name" id="client_name" placeholder="Client Name" required>
                                                <div class="invalid-feedback">
                                                    Client Name Required / Already Exist
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="country_id">Country Code <span class="text-danger">*</span></label>
                                                <select class="select2 form-control" name="country_id" id="country_id" required="">
                                                    <option></option>
                                                    <?php
                                                    if (isset($country_data) && !empty($country_data)) {
                                                        foreach ($country_data as $k1 => $v1) {
                                                            ?>
                                                            <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($v1->id) && !empty($v1->id) ? ($v1->id == 99 ? 'selected' : '') : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Country Code Required
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                                <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required>
                                                <div class="invalid-feedback">
                                                    Contact Number Required / Already Exist
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="client_email">Email</label>
                                                <input tabindex="2" type="email" class="form-control" name="client_email" id="client_email" placeholder="Email">
                                                <div class="invalid-feedback">
                                                    Email Required / Already Exist
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="password">Password <span class="text-danger">*</span></label>
                                                <input tabindex="2" type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                                <div class="invalid-feedback">
                                                    Password Required
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="confirm_password">Confirm Password</label>
                                                <input tabindex="2" type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required="">
                                                <div class="invalid-feedback">
                                                    Confirm Password Required
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                                <input type="text" tabindex="4" class="form-control address" name="address" id="address" required="" placeholder="address">
                                                <div class="invalid-feedback">
                                                    Address Required
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="pincode">Pincode <span class="text-danger">*</span></label>
                                                <input type="text" tabindex="4" class="form-control numbersonly" name="pincode" id="pincode" required="" placeholder="Pincode">
                                                <div class="invalid-feedback">
                                                    Pincode Required
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="state">State <span class="text-danger">*</span></label>
                                                <select tabindex="7" class="select2 form-control" name="state" id="state" required="">
                                                    <option></option>
                                                    <?php
                                                    if (isset($state) && !empty($state)) {
                                                        foreach ($state as $key => $state_val) {
                                                            ?>
                                                            <option value="<?= $state_val->id ?>"><?= $state_val->state_name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="invalid-feedback">
                                                    State Required
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="city">City <span class="text-danger">*</span></label>
                                                <select  tabindex="8" class="select2 form-control" name="city" id="city" required="">
                                                    <option></option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    City Required
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-md-12 text-right">
                                                <a href="<?= base_url() ?>Auth" class="btn btn-success waves-effect waves-themed btn-sm mt-3 mr-2"><span class="fal fa-sign-in mr-1"></span>Login</a>
                                                <button id="js-login-btn" type="submit" class="btn btn-danger waves-effect waves-themed btn-danger btn-sm mt-3">Register</button>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN Color profile -->
        <!-- this area is hidden and will not be seen on screens or screen readers -->
        <!-- we use this only for CSS color refernce for JS stuff -->
        <p id="js-color-profile" class="d-none">
            <span class="color-primary-50"></span>
            <span class="color-primary-100"></span>
            <span class="color-primary-200"></span>
            <span class="color-primary-300"></span>
            <span class="color-primary-400"></span>
            <span class="color-primary-500"></span>
            <span class="color-primary-600"></span>
            <span class="color-primary-700"></span>
            <span class="color-primary-800"></span>
            <span class="color-primary-900"></span>
            <span class="color-info-50"></span>
            <span class="color-info-100"></span>
            <span class="color-info-200"></span>
            <span class="color-info-300"></span>
            <span class="color-info-400"></span>
            <span class="color-info-500"></span>
            <span class="color-info-600"></span>
            <span class="color-info-700"></span>
            <span class="color-info-800"></span>
            <span class="color-info-900"></span>
            <span class="color-danger-50"></span>
            <span class="color-danger-100"></span>
            <span class="color-danger-200"></span>
            <span class="color-danger-300"></span>
            <span class="color-danger-400"></span>
            <span class="color-danger-500"></span>
            <span class="color-danger-600"></span>
            <span class="color-danger-700"></span>
            <span class="color-danger-800"></span>
            <span class="color-danger-900"></span>
            <span class="color-warning-50"></span>
            <span class="color-warning-100"></span>
            <span class="color-warning-200"></span>
            <span class="color-warning-300"></span>
            <span class="color-warning-400"></span>
            <span class="color-warning-500"></span>
            <span class="color-warning-600"></span>
            <span class="color-warning-700"></span>
            <span class="color-warning-800"></span>
            <span class="color-warning-900"></span>
            <span class="color-success-50"></span>
            <span class="color-success-100"></span>
            <span class="color-success-200"></span>
            <span class="color-success-300"></span>
            <span class="color-success-400"></span>
            <span class="color-success-500"></span>
            <span class="color-success-600"></span>
            <span class="color-success-700"></span>
            <span class="color-success-800"></span>
            <span class="color-success-900"></span>
            <span class="color-fusion-50"></span>
            <span class="color-fusion-100"></span>
            <span class="color-fusion-200"></span>
            <span class="color-fusion-300"></span>
            <span class="color-fusion-400"></span>
            <span class="color-fusion-500"></span>
            <span class="color-fusion-600"></span>
            <span class="color-fusion-700"></span>
            <span class="color-fusion-800"></span>
            <span class="color-fusion-900"></span>
        </p>
        <!-- END Color profile -->
        <script src="<?= base_url('assets') ?>/js/vendors.bundle.js"></script>
        <script src="<?= base_url('assets') ?>/js/app.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/select2/select2.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/notifications/toastr/toastr.js"></script>
<!--        <script>
            $("#js-login-btn").click(function (event) {

                // Fetch form to apply custom Bootstrap validation
                var form = $("#js-login")

                if (form[0].checkValidity() === false) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.addClass('was-validated');
                // Perform ajax submit here...
            });
        </script>-->
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 100,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $(document).ready(function () {
<?= $add_message; ?>
                $("#country_id").select2({
                    placeholder: "Select country",
                    allowClear: true,
                    width: '100%'
                });
                $("#state").select2({
                    placeholder: "Select state",
                    allowClear: true,
                    width: '100%'
                });
                $("#city").select2({
                    placeholder: "Select city",
                    allowClear: true,
                    width: '100%'
                });
                $('#registerCustomer').validate({
                    validClass: "is-valid",
                    errorClass: "is-invalid",
                    rules: {
                        client_name: {
                            remote: {
                                url: "<?= base_url('/Auth/checkClientName') ?>",
                                type: "get"
                            }
                        },
                        client_email: {
                            remote: {
                                url: "<?= base_url('/Auth/checkcCientEmail') ?>",
                                type: "get"
                            }
                        },
                        mobile: {
                            remote: {
                                url: "<?= base_url('/Auth/checkcClientPhone') ?>",
                                type: "get"
                            }
                        }
                    },
                    messages: {
                        client_name: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        client_email: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        mobile: {
                            remote: jQuery.validator.format("{0} is already in use")
                        }
                    },
                    submitHandler: function (form) {
                        form.submit();
                    },
                    errorPlacement: function (error, element) {
                        return true;
                    }
                });
            });

            $(document).on('change', '#state', function () {
                $('#city').html('');
                var city = '<option value="">Select</option>';
                var state_id = $(this).val();
                if (state_id != '' && state_id != undefined) {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('Auth/getCityByState') ?>',
                        data: {state_id: state_id},
                        success: function (returnData) {
                            var data = JSON.parse(returnData);
                            console.log(data);
                            if (data.result) {
                                if (data.city.length > 0) {
                                    data.city.forEach(function (val, key) {
                                        city += '<option value="' + val.id + '">' + val.city_name + '</option>';
                                    });
                                }
                            }
                            $('#city').append(city);
                        }
                    });
                } else {
                    $('#city').append(city);
                }
            });
        </script>
    </body>
</html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            Login - Design Factory Interior
        </title>
        <meta name="description" content="Login Design Factory Interior">
        <meta name="author" content="Weborative IT Consultancy LLP">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Design Factory Interior" />
        <meta property="og:description" content="" />
        <meta property="og:site_name" content="Design Factory Interior" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>favicon-16x16.png">
        <link rel="mask-icon" href="<?= base_url('assets') ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?= base_url('assets') ?>/css/fa-brands.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/formplugins/select2/select2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/notifications/toastr/toastr.css">
        <style>
            .bg-transparent{
                background-color: #5b5b5b !important;
            }
        </style>
    </head>
    <body>
        <div class="page-wrapper">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="flex-1" style="background: url(<?= base_url('assets') ?>/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">
                        <div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
                            <div class="row">
                                <div class="col-xl-4 ml-auto mr-auto">
                                    <!--<div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4" style="background-color: #fff;">-->
                                    <div class="m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
                                        <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                                            <img src="<?= base_url() ?><?= isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? $company_data->company_logo : '' ?>" alt="Design Factory Client Panel" aria-roledescription="logo" style="margin: 0 auto;">
                                        </a>
                                    </div>
                                    <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0 bg-brand-gradient">
                                        <?= form_open(base_url('Auth/send_sms'), 'class="needs-validation" novalidate') ?>
                                        <div class="form-group">
                                            <label class="form-label" for="username" style="color: white;">Username</label>
                                            <input type="text" id="username" name="username" class="form-control" placeholder="your email or phone number"required="">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="password" style="color: white;">Password</label>
                                            <input type="password" id="password" name="password" class="form-control" placeholder="password" required="">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-md-12 text-center">
                                                <a href="<?= base_url('Auth/registerCustomer') ?>" class="btn btn-danger waves-effect waves-themed btn-sm mt-3 mr-2"><span class="fal fa-user mr-1"></span>Register Account</a>
                                                <button type="submit" class="btn btn-success waves-effect waves-themed btn-sm mt-3"><span class="fal fa-sign-in mr-1"></span>Secure login</button>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-md-12 text-center mt-3">
                                                <a href="<?= base_url() ?>Auth/forgotPassword" class="text-danger"><strong>Forgot Password</strong></a>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN Color profile -->
        <!-- this area is hidden and will not be seen on screens or screen readers -->
        <!-- we use this only for CSS color refernce for JS stuff -->
        <p id="js-color-profile" class="d-none">
            <span class="color-primary-50"></span>
            <span class="color-primary-100"></span>
            <span class="color-primary-200"></span>
            <span class="color-primary-300"></span>
            <span class="color-primary-400"></span>
            <span class="color-primary-500"></span>
            <span class="color-primary-600"></span>
            <span class="color-primary-700"></span>
            <span class="color-primary-800"></span>
            <span class="color-primary-900"></span>
            <span class="color-info-50"></span>
            <span class="color-info-100"></span>
            <span class="color-info-200"></span>
            <span class="color-info-300"></span>
            <span class="color-info-400"></span>
            <span class="color-info-500"></span>
            <span class="color-info-600"></span>
            <span class="color-info-700"></span>
            <span class="color-info-800"></span>
            <span class="color-info-900"></span>
            <span class="color-danger-50"></span>
            <span class="color-danger-100"></span>
            <span class="color-danger-200"></span>
            <span class="color-danger-300"></span>
            <span class="color-danger-400"></span>
            <span class="color-danger-500"></span>
            <span class="color-danger-600"></span>
            <span class="color-danger-700"></span>
            <span class="color-danger-800"></span>
            <span class="color-danger-900"></span>
            <span class="color-warning-50"></span>
            <span class="color-warning-100"></span>
            <span class="color-warning-200"></span>
            <span class="color-warning-300"></span>
            <span class="color-warning-400"></span>
            <span class="color-warning-500"></span>
            <span class="color-warning-600"></span>
            <span class="color-warning-700"></span>
            <span class="color-warning-800"></span>
            <span class="color-warning-900"></span>
            <span class="color-success-50"></span>
            <span class="color-success-100"></span>
            <span class="color-success-200"></span>
            <span class="color-success-300"></span>
            <span class="color-success-400"></span>
            <span class="color-success-500"></span>
            <span class="color-success-600"></span>
            <span class="color-success-700"></span>
            <span class="color-success-800"></span>
            <span class="color-success-900"></span>
            <span class="color-fusion-50"></span>
            <span class="color-fusion-100"></span>
            <span class="color-fusion-200"></span>
            <span class="color-fusion-300"></span>
            <span class="color-fusion-400"></span>
            <span class="color-fusion-500"></span>
            <span class="color-fusion-600"></span>
            <span class="color-fusion-700"></span>
            <span class="color-fusion-800"></span>
            <span class="color-fusion-900"></span>
        </p>
        <!-- END Color profile -->
        <script src="<?= base_url('assets') ?>/js/vendors.bundle.js"></script>
        <script src="<?= base_url('assets') ?>/js/app.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/select2/select2.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/notifications/toastr/toastr.js"></script>
<!--        <script>
            $("#js-login-btn").click(function (event) {

                // Fetch form to apply custom Bootstrap validation
                var form = $("#js-login")

                if (form[0].checkValidity() === false) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.addClass('was-validated');
                // Perform ajax submit here...
            });
        </script>-->
        <script type="text/javascript">
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 100,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $(document).ready(function () {
<?= $add_message; ?>
            });
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', function () {
                    navigator.serviceWorker.register('/service-worker.js').then(function (registration) {
                        // Registration was successful
                        console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    }, function (err) {
                        // registration failed :(
                        console.log('ServiceWorker registration failed: ', err);
                    });
                });
            }
        </script>
    </body>
</html>
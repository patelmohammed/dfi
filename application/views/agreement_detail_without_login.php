<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            <?= isset($this->page_title) && !empty($this->page_title) ? $this->page_title : '' ?>
        </title>
        <meta name="description" content="Design Factory Interior">
        <meta name="author" content="Weborative IT Consultancy LLP">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Design Factory Interior" />
        <meta property="og:description" content="" />
        <!--<meta property="og:image" content="http://fdmaster.foodmohalla.in/assets/images/logo_640.png" />-->
        <meta property="og:site_name" content="Design Factory Interior" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>favicon-16x16.png">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/miscellaneous/reactions/reactions.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/miscellaneous/fullcalendar/fullcalendar.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/miscellaneous/jqvmap/jqvmap.bundle.css">

        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/fa-solid.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/formplugins/select2/select2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/datagrid/datatables/datatables.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/formplugins/bootstrap-datepicker/bootstrap-datepicker.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/notifications/toastr/toastr.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/notifications/sweetalert2/sweetalert2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/formplugins/summernote/summernote.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/themes/cust-theme-7.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/papyrus.css">

        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/page-invoice.css">

        <script src="<?= base_url() ?>assets/js/vendors.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/notifications/sweetalert2/sweetalert2.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/summernote/summernote.js"></script>

        <style>
            .signature-pad--body {
                border-radius: 4px;
                position: relative;
                -webkit-box-flex: 1;
                -ms-flex: 1;
                flex: 1;
                border: 1px solid #c0cbda;
            }
            .font_papyrus{
                font-family: 'Papyrus';
            }
        </style>

    </head>
    <body>
        <!-- BEGIN Page Wrapper -->
        <div class="page-wrapper alt">
            <!-- BEGIN Page Content -->
            <!-- the #js-page-content id is needed for some plugins to initialize -->
            <main id="js-page-content" role="main" class="page-content">
                <div class="subheader mb-3">
                    <h1 class="subheader-title">
                        <i class='subheader-icon fal fa-handshake'></i> Agreement
                    </h1>
                    <div class="d-flex mr-0 hidden-sm-down">
                        <a href="<?= base_url() . 'Without_login/printAgreement/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : '') ?>" class="btn btn-danger ml-auto waves-effect waves-themed mr-3 btn-sm"><span class="fal fa-file-pdf mr-1"></span>Download</a>
                        <?php
                        if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) && $agreement_data->third_party_signed == 1) {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-sm"><span class="fal fa-file-signature mr-1"></span>Signed</a>
                            <?php
                        } else {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-sm" data-toggle="modal" data-target="#sign_modal"><span class="fal fa-file-signature mr-1"></span>Sign</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center mr-0 hidden-sm-up mb-2">
                        <a href="<?= base_url() . 'Without_login/printAgreement/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : '') ?>" class="btn btn-danger ml-auto waves-effect waves-themed mr-3 btn-xs"><span class="fal fa-file-pdf mr-1"></span>Download</a>
                        <?php
                        if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) && $agreement_data->third_party_signed == 1) {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-xs"><span class="fal fa-file-signature mr-1"></span>Signed</a>
                            <?php
                        } else {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-xs" data-toggle="modal" data-target="#sign_modal"><span class="fal fa-file-signature mr-1"></span>Sign</a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-xl-12">
                        <div class="panel-container show">
                            <div class="panel-content">
                                <div data-size="A4">
                                    <div class="row">
                                        <div class="col-sm-12 d-flex">
                                            <div class="table-responsive">
                                                <table class="table table-clean table-sm align-self-end" style="width: 100%;background-color: #5b5b5b;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 100%;text-align: center;" colspan="2">
                                                                <img src="<?= base_url() . (isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? $company_data->company_logo : '') ?>" alt="preview not available" style="opacity: 1;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;color: white;text-align: center;font-size: 24px;" class="font_papyrus" colspan="2">
                                                                <?= isset($company_data->tag_line) && !empty($company_data->tag_line) ? $company_data->tag_line : '' ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;color: white;text-align: right;" class="font_papyrus">
                                                                <span style="margin-right: 10px;"><?= isset($company_data->email) && !empty($company_data->email) ? $company_data->email : '' ?></span>
                                                            </td>
                                                            <td style="width: 50%;color: white;text-align: left;border-left: 1px solid white;" class="font_papyrus">
                                                                <span style="margin-left: 10px;"><?= isset($company_data->mobile) && !empty($company_data->mobile) ? $company_data->mobile : '' ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;color: white;text-align: right;" class="font_papyrus">
                                                                <span style="margin-right: 10px;"><?= isset($company_data->email_alt) && !empty($company_data->email_alt) ? $company_data->email_alt : '' ?></span>
                                                            </td>
                                                            <td style="width: 50%;color: white;text-align: left;border-left: 1px solid white;" class="font_papyrus">
                                                                <span style="margin-left: 10px;"><?= isset($company_data->mobile_alt) && !empty($company_data->mobile_alt) ? $company_data->mobile_alt : '' ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;color: white;text-align: center;font-size: 15px;" class="font_papyrus" colspan="2">
                                                                <?= isset($company_data->address) && !empty($company_data->address) ? $company_data->address : '' ?>
                                                                <?= isset($company_data->company_city) && !empty($company_data->company_city) ? ', ' . getCityNameById($company_data->company_city)->city_name : '' ?>
                                                                <?= isset($company_data->company_pincode) && !empty($company_data->company_pincode) ? ' - ' . $company_data->company_pincode : '' ?>
                                                                <?= isset($company_data->company_state) && !empty($company_data->company_state) ? ', ' . getStateNameById($company_data->company_state)->state_name : '' ?>
                                                                <?= isset($company_data->company_country) && !empty($company_data->company_country) ? ', ' . getCountry($company_data->company_country)->name : '' ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 d-flex">
                                            <div class="table-responsive">
                                                <table class="table table-clean table-sm align-self-end">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-left keep-print-font">
                                                                <h5 class="m-0 fw-500 h3 keep-print-font">Agreement Date:</h5>
                                                            </td>
                                                            <td class="text-right keep-print-font">
                                                                <h5 class="m-0 fw-700 h3 keep-print-font"><?= isset($agreement_data->agreement_date) && !empty($agreement_data->agreement_date) ? date('d/m/Y', strtotime($agreement_data->agreement_date)) : '' ?></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left keep-print-font">
                                                                <h5 class="m-0 fw-500 h3 keep-print-font color-primary-500">Agreement Value:</h5>
                                                            </td>
                                                            <td class="text-right keep-print-font">
                                                                <h5 class="m-0 fw-700 h3 keep-print-font text-danger"><?= isset($agreement_data->contract_value) && !empty($agreement_data->contract_value) ? '&#8377; ' . $agreement_data->contract_value : '' ?></h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 ml-sm-auto">
                                            <div class="table-responsive">
                                                <table class="table table-sm table-clean text-right">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <strong>Agreement to&nbsp;:&nbsp;<?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?><?= isset($client_data->company_name) && !empty($client_data->company_name) ? ' (' . $client_data->company_name . ')' : '' ?></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                $address = array();
                                                                if (isset($client_data->client_address) && !empty($client_data->client_address)) {
                                                                    array_push($address, $client_data->client_address);
                                                                }
                                                                if (isset($client_data->client_city) && !empty($client_data->client_city)) {
                                                                    $city_name = getCityNameById($client_data->client_city);
                                                                    array_push($address, (isset($city_name->city_name) && !empty($city_name->city_name) ? $city_name->city_name : ''));
                                                                }
                                                                if (isset($client_data->client_state) && !empty($client_data->client_state)) {
                                                                    $client_state = getStateNameById($client_data->client_state);
                                                                    array_push($address, (isset($client_state->state_name) && !empty($client_state->state_name) ? $client_state->state_name : ''));
                                                                }
                                                                if (isset($client_data->client_pincode) && !empty($client_data->client_pincode)) {
                                                                    array_push($address, $client_data->client_pincode);
                                                                }
                                                                echo implode(', ', $address);
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table class="table mt-5">
                                                    <tbody>
                                                        <?= isset($agreement_data->description) && !empty($agreement_data->description) ? $agreement_data->description : '' ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <table style="width: 90%;">
                                                                <tr>
                                                                    <td style="width: 100%; height: 130px;">
                                                                        <?php
                                                                        if (isset($agreement_data->admin_signed) && !empty($agreement_data->admin_signed) && $agreement_data->admin_signed == 1) {
                                                                            ?>
                                                                            <img src="<?= base_url() . (isset($agreement_data->admin_signature_path) && !empty($agreement_data->admin_signature_path) && file_exists($agreement_data->admin_signature_path) ? $agreement_data->admin_signature_path : '') ?>" style="width: 100%;">
                                                                            <?php
                                                                        } else {
                                                                            echo "&nbsp;";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">
                                                                        <?= isset($agreement_data->admin_acceptance_firstname) && !empty($agreement_data->admin_acceptance_firstname) ? $agreement_data->admin_acceptance_firstname : '' ?>
                                                                        <?= isset($agreement_data->admin_acceptance_lastname) && !empty($agreement_data->admin_acceptance_lastname) ? $agreement_data->admin_acceptance_lastname : '' ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;"><?= isset($agreement_data->admin_acceptance_date) && !empty($agreement_data->admin_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->admin_acceptance_date)) : '' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 12%;"></td>
                                                        <td style="width: 26%;">
                                                            <?php if (isset($agreement_data->third_party_id) && !empty($agreement_data->third_party_id)) { ?>
                                                                <table style="width: 90%;">
                                                                    <tr>
                                                                        <td style="width: 100%; height: 130px;">
                                                                            <?php
                                                                            if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) && $agreement_data->third_party_signed == 1) {
                                                                                ?>
                                                                                <img src="<?= base_url() . (isset($agreement_data->third_party_signature_path) && !empty($agreement_data->third_party_signature_path) && file_exists($agreement_data->third_party_signature_path) ? $agreement_data->third_party_signature_path : '') ?>" style="width: 100%;">
                                                                                <?php
                                                                            } else {
                                                                                echo "&nbsp;";
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;">
                                                                            <?= isset($agreement_data->third_party_acceptance_firstname) && !empty($agreement_data->third_party_acceptance_firstname) ? $agreement_data->third_party_acceptance_firstname : '' ?>
                                                                            <?= isset($agreement_data->third_party_acceptance_lastname) && !empty($agreement_data->third_party_acceptance_lastname) ? $agreement_data->third_party_acceptance_lastname : '' ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;"><?= isset($agreement_data->third_party_acceptance_date) && !empty($agreement_data->third_party_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->third_party_acceptance_date)) : '' ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                                    </tr>
                                                                </table>
                                                            <?php } ?>
                                                        </td>
                                                        <td style="width: 12%;"></td>
                                                        <td style="width: 25%;">
                                                            <table style="width: 90%;">
                                                                <tr>
                                                                    <td style="width: 100%; height: 130px;">
                                                                        <?php
                                                                        if (isset($agreement_data->signed) && !empty($agreement_data->signed) && $agreement_data->signed == 1) {
                                                                            ?>
                                                                            <img src="<?= base_url() . (isset($agreement_data->signature_path) && !empty($agreement_data->signature_path) && file_exists($agreement_data->signature_path) ? $agreement_data->signature_path : '') ?>" style="width: 100%;">
                                                                            <?php
                                                                        } else {
                                                                            echo "&nbsp;";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">
                                                                        <?= isset($agreement_data->acceptance_firstname) && !empty($agreement_data->acceptance_firstname) ? $agreement_data->acceptance_firstname : '' ?>
                                                                        <?= isset($agreement_data->acceptance_lastname) && !empty($agreement_data->acceptance_lastname) ? $agreement_data->acceptance_lastname : '' ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;"><?= isset($agreement_data->acceptance_date) && !empty($agreement_data->acceptance_date) ? date('d/m/Y', strtotime($agreement_data->acceptance_date)) : '' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>          
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php
            if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed)) {
                
            } else {
                ?>
                <div class="modal fade" id="sign_modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">
                                    Signature & Confirmation Of Identity
                                </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                </button>
                            </div>
                            <?php echo form_open(base_url() . 'Without_login/agreementSign/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : ''), array('id' => 'identityConfirmationForm', 'class' => 'form-horizontal')); ?>
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label class="form-label" for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="first_name" id="first_name" placeholder="First Name" required>
                                        <div class="invalid-feedback">
                                            First Name Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label class="form-label" for="last_name">Last Name <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="last_name" id="last_name" placeholder="Last Name" required>
                                        <div class="invalid-feedback">
                                            Last Name Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="email" class="form-control" name="email" id="email" placeholder="Email" required value="<?= $this->session->userdata('user_email') ?>">
                                        <div class="invalid-feedback">
                                            Email Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label id="signatureLabel">Signature</label>
                                        <div class="signature-pad--body">
                                            <canvas id="signature" height="130" width="450"></canvas>
                                        </div>
                                        <input type="text" style="width:1px; height:1px; border:0px;" tabindex="-1" name="signature" id="signatureInput">
                                        <div class="invalid-feedback">
                                            Please sign the document.
                                        </div>
                                        <div class="dispay-block">
                                            <button type="button" class="btn btn-default btn-xs clear" tabindex="-1" data-action="clear">Clear</button>
                                            <button type="button" class="btn btn-default btn-xs" tabindex="-1" data-action="undo">Undo</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" autocomplete="off" data-form="#identityConfirmationForm">Save changes</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div> <!-- END Page Content -->
            <!-- END Page Content -->
            <!-- BEGIN Page Footer -->
            <footer class="page-footer" role="contentinfo">
                <div class="d-flex align-items-center flex-1 text-muted">
                    <span class="hidden-md-down fw-700"><?= '2020' . ' - ' . date('Y', strtotime('+1 year')) ?> © <?= COMPANY_NAME ?> powered by&nbsp;<a href='https://www.weborative.com' class='text-primary fw-500' title='weborative.com' target='_blank'>weborative.com</a></span>
                </div>
            </footer>
            <!-- END Page Footer -->
        </div>
        <!-- END Page Wrapper -->
        <!-- BEGIN Quick Menu -->
        <!-- to add more items, please make sure to change the variable '$menu-items: number;' in your _page-components-shortcut.scss -->
        <!-- END Quick Menu -->
        <!-- BEGIN Color profile -->
        <!-- this area is hidden and will not be seen on screens or screen readers -->
        <!-- we use this only for CSS color refernce for JS stuff -->
        <p id="js-color-profile" class="d-none">
            <span class="color-primary-50"></span>
            <span class="color-primary-100"></span>
            <span class="color-primary-200"></span>
            <span class="color-primary-300"></span>
            <span class="color-primary-400"></span>
            <span class="color-primary-500"></span>
            <span class="color-primary-600"></span>
            <span class="color-primary-700"></span>
            <span class="color-primary-800"></span>
            <span class="color-primary-900"></span>
            <span class="color-info-50"></span>
            <span class="color-info-100"></span>
            <span class="color-info-200"></span>
            <span class="color-info-300"></span>
            <span class="color-info-400"></span>
            <span class="color-info-500"></span>
            <span class="color-info-600"></span>
            <span class="color-info-700"></span>
            <span class="color-info-800"></span>
            <span class="color-info-900"></span>
            <span class="color-danger-50"></span>
            <span class="color-danger-100"></span>
            <span class="color-danger-200"></span>
            <span class="color-danger-300"></span>
            <span class="color-danger-400"></span>
            <span class="color-danger-500"></span>
            <span class="color-danger-600"></span>
            <span class="color-danger-700"></span>
            <span class="color-danger-800"></span>
            <span class="color-danger-900"></span>
            <span class="color-warning-50"></span>
            <span class="color-warning-100"></span>
            <span class="color-warning-200"></span>
            <span class="color-warning-300"></span>
            <span class="color-warning-400"></span>
            <span class="color-warning-500"></span>
            <span class="color-warning-600"></span>
            <span class="color-warning-700"></span>
            <span class="color-warning-800"></span>
            <span class="color-warning-900"></span>
            <span class="color-success-50"></span>
            <span class="color-success-100"></span>
            <span class="color-success-200"></span>
            <span class="color-success-300"></span>
            <span class="color-success-400"></span>
            <span class="color-success-500"></span>
            <span class="color-success-600"></span>
            <span class="color-success-700"></span>
            <span class="color-success-800"></span>
            <span class="color-success-900"></span>
            <span class="color-fusion-50"></span>
            <span class="color-fusion-100"></span>
            <span class="color-fusion-200"></span>
            <span class="color-fusion-300"></span>
            <span class="color-fusion-400"></span>
            <span class="color-fusion-500"></span>
            <span class="color-fusion-600"></span>
            <span class="color-fusion-700"></span>
            <span class="color-fusion-800"></span>
            <span class="color-fusion-900"></span>
        </p>
        <!-- END Color profile -->
        <!-- base vendor bundle: 
                         DOC: if you remove pace.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations 
                                                + pace.js (recommended)
                                                + jquery.js (core)
                                                + jquery-ui-cust.js (core)
                                                + popper.js (core)
                                                + bootstrap.js (core)
                                                + slimscroll.js (extension)
                                                + app.navigation.js (core)
                                                + ba-throttle-debounce.js (core)
                                                + waves.js (extension)
                                                + smartpanels.js (extension)
                                                + src/../jquery-snippets.js (core) -->
        <script src="<?= base_url() ?>assets/js/app.bundle.js"></script>
        <!-- The order of scripts is irrelevant. Please check out the plugin pages for more details about these plugins below: -->
        <script src="<?= base_url() ?>assets/js/dependency/moment/moment.js"></script>
        <script src="<?= base_url() ?>assets/js/miscellaneous/fullcalendar/fullcalendar.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/statistics/sparkline/sparkline.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/statistics/easypiechart/easypiechart.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/statistics/flot/flot.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/miscellaneous/jqvmap/jqvmap.bundle.js"></script>

        <script src="<?= base_url() ?>assets/js/dependency/moment/moment-timezone-with-data-1970-2030.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/select2/select2.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/jquery-validate/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/datagrid/datatables/datatables.bundle.js"></script>
        <script src="<?= base_url() ?>assets/js/datagrid/datatables/datatables.export.js"></script>
        <script src="<?= base_url() ?>assets/js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?= base_url() ?>assets/js/notifications/toastr/toastr.js"></script>
        <script src="<?= base_url() ?>assets/plugins/signature-pad/signature_pad.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var agreement_third_party_signed = '<?= isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) ? $agreement_data->third_party_signed : 0 ?>';
                $(document).on('keypress', '.textonly', function (e) {
                    var keyCode = e.keyCode || e.which;
                    var regex = /^[A-Za-z0-9 ]+$/;
                    var isValid = regex.test(String.fromCharCode(keyCode));
                    return isValid;
                });

                if (agreement_third_party_signed == 0) {
                    $(function () {
                        SignaturePad.prototype.toDataURLAndRemoveBlanks = function () {
                            var canvas = this._ctx.canvas;
                            // First duplicate the canvas to not alter the original
                            var croppedCanvas = document.createElement('canvas'),
                                    croppedCtx = croppedCanvas.getContext('2d');

                            croppedCanvas.width = canvas.width;
                            croppedCanvas.height = canvas.height;
                            croppedCtx.drawImage(canvas, 0, 0);

                            // Next do the actual cropping
                            var w = croppedCanvas.width,
                                    h = croppedCanvas.height,
                                    pix = {
                                        x: [],
                                        y: []
                                    },
                                    imageData = croppedCtx.getImageData(0, 0, croppedCanvas.width, croppedCanvas.height),
                                    x, y, index;

                            for (y = 0; y < h; y++) {
                                for (x = 0; x < w; x++) {
                                    index = (y * w + x) * 4;
                                    if (imageData.data[index + 3] > 0) {
                                        pix.x.push(x);
                                        pix.y.push(y);

                                    }
                                }
                            }
                            pix.x.sort(function (a, b) {
                                return a - b
                            });
                            pix.y.sort(function (a, b) {
                                return a - b
                            });
                            var n = pix.x.length - 1;

                            w = pix.x[n] - pix.x[0];
                            h = pix.y[n] - pix.y[0];
                            var cut = croppedCtx.getImageData(pix.x[0], pix.y[0], w, h);

                            croppedCanvas.width = w;
                            croppedCanvas.height = h;
                            croppedCtx.putImageData(cut, 0, 0);

                            return croppedCanvas.toDataURL();
                        };


                        function signaturePadChanged() {

                            var input = document.getElementById('signatureInput');
                            var $signatureLabel = $('#signatureLabel');
                            $signatureLabel.removeClass('text-danger');

                            if (signaturePad.isEmpty()) {
                                $signatureLabel.addClass('text-danger');
                                input.value = '';
                                return false;
                            }

                            $('#signatureInput-error').remove();
                            var partBase64 = signaturePad.toDataURLAndRemoveBlanks();
                            partBase64 = partBase64.split(',')[1];
                            input.value = partBase64;
                        }

                        var canvas = document.getElementById("signature");
                        var clearButton = $("[data-action=clear]");
                        var undoButton = $("[data-action=undo]");
                        var identityFormSubmit = document.getElementById('identityConfirmationForm');

                        var signaturePad = new SignaturePad(canvas, {
                            maxWidth: 2,
                            onEnd: function () {
                                signaturePadChanged();
                            }
                        });

                        clearButton.on("click", function (event) {
                            signaturePad.clear();
                            signaturePadChanged();
                        });

                        undoButton.on("click", function (event) {
                            var data = signaturePad.toData();
                            if (data) {
                                data.pop(); // remove the last dot or line
                                signaturePad.fromData(data);
                                signaturePadChanged();
                            }
                        });

                        $('#identityConfirmationForm').validate({
                            validClass: "is-valid",
                            errorClass: "is-invalid",
                            rules: {
                                first_name: 'required',
                                last_name: 'required',
                                signature: 'required',
                                email: {
                                    email: true,
                                    required: true
                                }
                            },
                            messages: {
                                signature: {
                                    remote: jQuery.validator.format("{0} is already in use")
                                },
                            },
                            submitHandler: function (form) {
                                form.submit();
                            },
                            errorPlacement: function (error, element) {
                                return true;
                            }
                        });
                    });
                }
            });


        </script>
    </body>
</html>

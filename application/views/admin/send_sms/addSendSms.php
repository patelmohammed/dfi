<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-comment-alt-dots'></i> Send SMS
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Send_sms">Send SMS List</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Send_sms/addEditSendSms', $arrayName = array('id' => 'addEditSendSms')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="client_id">Client <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="client_id[]" id="client_id" required="" multiple="">
                                    <option></option>
                                    <?php
                                    if (isset($client_data) && !empty($client_data)) {
                                        foreach ($client_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->client_id ?>"><?= isset($value1->client_name) && !empty($value1->client_name) ? $value1->client_name : '' ?><?= (isset($value1->company_name) && !empty($value1->company_name) ? ' (' . $value1->company_name . ')' : '') ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Client Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="client_id">Message <span class="text-danger">*</span></label>
                                <textarea name="message_body" id="message_body" class="form-control textonly" rows="3"></textarea>
                                <div class="invalid-feedback">
                                    Client Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="send_me_copy">&nbsp;</label>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="send_me_copy" name="send_me_copy">
                                    <label class="custom-control-label" for="send_me_copy">Send me copy</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {

        $("#client_id").select2({
            placeholder: "Select client",
            allowClear: true,
            width: '100%'
        });

        $('#addEditSendSms').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
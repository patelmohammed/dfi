<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-comment-alt-dots'></i> SMS
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Send_sms/addEditSendSms">Send SMS</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Contact</th>
                                    <th>Message</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($sms_data) && !empty($sms_data)) {
                                    $sn = 0;
                                    foreach ($sms_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->InsDateTime) && !empty($value->InsDateTime) ? date('d-m-Y H:i', strtotime($value->InsDateTime)) : '' ?></td>
                                            <td><?= isset($value->contact_number) && !empty($value->contact_number) ? '+' . $value->contact_number : '' ?></td> 
                                            <td><?= isset($value->message) && !empty($value->message) ? $value->message : '' ?></td> 
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
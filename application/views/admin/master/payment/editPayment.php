<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-money-bill-wave'></i> Edit Payment Method
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/paymentMethod">Payment Method</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Master/addEditPaymentMethod/' . $encrypted_id, $arrayName = array('id' => 'addEditPaymentMethod')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_name">Payment Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="payment_name" id="payment_name" placeholder="Payment Name" required value="<?= isset($payment_data->payment_name) && !empty($payment_data->payment_name) ? $payment_data->payment_name : '' ?>">
                                <div class="invalid-feedback">
                                    Payment Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_desc">Description</label>
                                <input tabindex="2" type="text" class="form-control textonly" name="payment_desc" id="payment_desc" placeholder="Description" value="<?= isset($payment_data->payment_desc) && !empty($payment_data->payment_desc) ? $payment_data->payment_desc : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($payment_data->payment_id) && !empty($payment_data->payment_id) ? $payment_data->payment_id : '' ?>';
    $(document).ready(function () {
        $('#addEditPaymentMethod').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                payment_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkPaymentName/') ?>" + id,
                        type: "get"
                    }
                }
            },
            messages: {
                payment_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
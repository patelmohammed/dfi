<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Third Party
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?= base_url() ?>admin/Master/addEditThirdParty">Add Third Party</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Company Name</th>
                                    <th>Contact Number</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($user_data) && !empty($user_data)) {
                                    $sn = 0;
                                    foreach ($user_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= $value->third_party_name ?></td> 
                                            <td><?= $value->company_name ?></td> 
                                            <td><?= isset($value->country_id) && !empty($value->country_id) ? '+' . getCountryCode($value->country_id) . ' ' : '' ?><?= $value->third_party_number ?></td>
                                            <td><?= isset($value->third_party_email) && !empty($value->third_party_email) ? $value->third_party_email : '' ?></td> 
                                            <td>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href="javascript:void(0);" data-id="<?= $value->third_party_id ?>" data-url="<?= base_url() ?>admin/Master/activeDeactiveThirdParty" class="activeDeactive" data-is_active="<?= $value->status ?>">
                                                        <?php if ($value->status == 1) { ?>
                                                            <span class="badge badge-success badge-pill">Active</span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger badge-pill">Deactive</span>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php
                                                    if ($this->role == 'Admin' || $value->InsUser == $this->user_id) {
                                                        if ($menu_rights['edit_right']) {
                                                            ?>
                                                            <a href='<?php echo base_url() ?>admin/Master/addEditThirdParty/<?= $value->third_party_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                                <i class="fal fa-edit"></i>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($menu_rights['delete_right']) { ?>
                                                            <a href='javascript:void(0);' data-id="<?= $value->third_party_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                                <i class="fal fa-times"></i>
                                                            </a>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Master/deleteThirdParty') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>
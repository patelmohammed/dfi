<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Third Party
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/thirdParty">Third Party</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Master/addEditThirdParty/' . $encrypted_id, $arrayName = array('id' => 'addEditThirdParty', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="third_party_name">Third Party Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="third_party_name" id="third_party_name" placeholder="Third Party Name" required value="<?= isset($user_data->third_party_name) && !empty($user_data->third_party_name) ? $user_data->third_party_name : '' ?>">
                                <div class="invalid-feedback">
                                    Third Party Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="company_name">Company Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="company_name" id="company_name" placeholder="Company Name" required value="<?= isset($user_data->company_name) && !empty($user_data->company_name) ? $user_data->company_name : '' ?>">
                                <div class="invalid-feedback">
                                    Company Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="third_party_designation">Designation <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="third_party_designation" id="third_party_designation" placeholder="Designation" required value="<?= isset($user_data->third_party_designation) && !empty($user_data->third_party_designation) ? $user_data->third_party_designation : '' ?>">
                                <div class="invalid-feedback">
                                    Designation Required
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="country_id">Country Code <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="country_id" id="country_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($country_data) && !empty($country_data)) {
                                        foreach ($country_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($user_data->country_id) && !empty($user_data->country_id) ? set_selected($user_data->country_id, $v1->id) : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Country Code Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required value="<?= isset($user_data->third_party_number) && !empty($user_data->third_party_number) ? $user_data->third_party_number : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="third_party_email">Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="email" class="form-control" name="third_party_email" id="third_party_email" placeholder="Email" required value="<?= isset($user_data->third_party_email) && !empty($user_data->third_party_email) ? $user_data->third_party_email : '' ?>">
                                <div class="invalid-feedback">
                                    Email Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                <input type="text" tabindex="4" class="form-control address" name="address" id="address" required="" placeholder="address" value="<?= isset($user_data->third_party_address) && !empty($user_data->third_party_address) ? $user_data->third_party_address : '' ?>">
                                <div class="invalid-feedback">
                                    Address Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="pincode">Pincode <span class="text-danger">*</span></label>
                                <input type="text" tabindex="4" class="form-control numbersonly" name="pincode" id="pincode" required="" placeholder="Pincode" value="<?= isset($user_data->third_party_pincode) && !empty($user_data->third_party_pincode) ? $user_data->third_party_pincode : '' ?>">
                                <div class="invalid-feedback">
                                    Pincode Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="state">State <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="state" id="state" required="">
                                    <option></option>
                                    <?php
                                    if (isset($state) && !empty($state)) {
                                        foreach ($state as $key => $state_val) {
                                            ?>
                                            <option value="<?= $state_val->id ?>" <?= isset($user_data->third_party_state) && !empty($user_data->third_party_state) ? set_selected($user_data->third_party_state, $state_val->id) : '' ?>><?= $state_val->state_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    State Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="city">City <span class="text-danger">*</span></label>
                                <select  tabindex="8" class="select2 form-control" name="city" id="city" required="">
                                    <option></option>
                                </select>
                                <div class="invalid-feedback">
                                    City Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($user_data->third_party_id) && !empty($user_data->third_party_id) ? $user_data->third_party_id : '' ?>';
    $(document).ready(function () {
        var state_id = '<?= isset($user_data->third_party_state) && !empty($user_data->third_party_state) ? $user_data->third_party_state : '' ?>';
        var city_id = '<?= isset($user_data->third_party_city) && !empty($user_data->third_party_city) ? $user_data->third_party_city : '' ?>';
        cityByState(state_id, city_id);
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $("#state").select2({
            placeholder: "Select state",
            allowClear: true,
            width: '100%'
        });
        $("#city").select2({
            placeholder: "Select city",
            allowClear: true,
            width: '100%'
        });
        $('#addEditThirdParty').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                third_party_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkThirdPartyName/') ?>" + id,
                        type: "get"
                    }
                },
                mobile: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkcThirdPartyPhone/') ?>" + id,
                        type: "get"
                    }
                }
            },
            messages: {
                third_party_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                mobile: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('change', '#state', function () {
        var state_id = $(this).val();
        cityByState(state_id);
    });

    function cityByState(state_id, city_id = '') {
        $('#city').html('');
        var city = '<option value="">Select</option>';
        if (state_id != '' && state_id != undefined) {
            $.ajax({
                type: "POST",
                url: '<?= base_url('admin/Master/getCityByState') ?>',
                data: {state_id: state_id},
                success: function (returnData) {
                    var data = JSON.parse(returnData);
                    if (data.result) {
                        if (data.city.length > 0) {
                            data.city.forEach(function (val, key) {
                                city += '<option value="' + val.id + '" ' + (city_id == val.id ? 'selected' : '') + '>' + val.city_name + '</option>';
                            });
                        }
                    }
                    $('#city').append(city);
                }
            });
        } else {
            $('#city').append(city);
    }
    }
</script>
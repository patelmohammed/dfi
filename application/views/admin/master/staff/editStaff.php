<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Staff
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/staff">Staff</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Master/addEditStaff/' . $encrypted_id, $arrayName = array('id' => 'addEditStaff', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_name">Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="user_name" id="user_name" placeholder="Name" required value="<?= isset($user_data->user_name) && !empty($user_data->user_name) ? $user_data->user_name : '' ?>">
                                <div class="invalid-feedback">
                                    Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="country_id">Country Code <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="country_id" id="country_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($country_data) && !empty($country_data)) {
                                        foreach ($country_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($user_data->country_id) && !empty($user_data->country_id) ? set_selected($user_data->country_id, $v1->id) : (isset($v1->id) && !empty($v1->id) ? ($v1->id == 99 ? 'selected' : '') : '') ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Country Code Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required value="<?= isset($user_data->user_number) && !empty($user_data->user_number) ? $user_data->user_number : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_email">Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="email" class="form-control" name="user_email" id="user_email" placeholder="Email" value="<?= isset($user_data->user_email) && !empty($user_data->user_email) ? $user_data->user_email : '' ?>">
                                <div class="invalid-feedback">
                                    Email Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="password">Password</label>
                                <input tabindex="2" type="password" class="form-control" name="password" id="password" placeholder="Password">
                                <div class="invalid-feedback">
                                    Password Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="confirm_password">Confirm Password</label>
                                <input tabindex="2" type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                <div class="invalid-feedback">
                                    Confirm Password Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                <input type="text" tabindex="4" class="form-control address" name="address" id="address" required="" placeholder="address" value="<?= isset($user_data->user_address) && !empty($user_data->user_address) ? $user_data->user_address : '' ?>">
                                <div class="invalid-feedback">
                                    Address Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="pincode">Pincode <span class="text-danger">*</span></label>
                                <input type="text" tabindex="4" class="form-control numbersonly" name="pincode" id="pincode" required="" placeholder="Pincode" value="<?= isset($user_data->user_pincode) && !empty($user_data->user_pincode) ? $user_data->user_pincode : '' ?>">
                                <div class="invalid-feedback">
                                    Pincode Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="state">State <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="state" id="state" required="">
                                    <option></option>
                                    <?php
                                    if (isset($state) && !empty($state)) {
                                        foreach ($state as $key => $state_val) {
                                            ?>
                                            <option value="<?= $state_val->id ?>" <?= isset($user_data->user_state) && !empty($user_data->user_state) ? set_selected($user_data->user_state, $state_val->id) : '' ?>><?= $state_val->state_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    State Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="city">City <span class="text-danger">*</span></label>
                                <select  tabindex="8" class="select2 form-control" name="city" id="city" required="">
                                    <option></option>
                                </select>
                                <div class="invalid-feedback">
                                    City Required
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 0px;margin-bottom: 0px;padding: 0px;margin-bottom: 6px;border-bottom: 1px dashed #886ab5;">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="checkall_side_menu">Side Menu Access</label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="checkall_side_menu">
                                        <label class="custom-control-label" for="checkall_side_menu">Select All</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="table-responsive-sm">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Menu</th>
                                                <th>Full Access</th>
                                                <th>View</th>
                                                <th>Add</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($side_menu)) {
                                                foreach ($side_menu as $key => $value) {
                                                    if ($value->menu_url == '#') {
                                                        ?>
                                                        <tr>
                                                            <td colspan="6"><?= $value->menu_name ?></td>
                                                        </tr>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td><?= $value->menu_name ?></td>
                                                            <td>
                                                                <div class="frame-wrap">
                                                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                        <input type="checkbox" class="custom-control-input check_full_access check_all" id="full_access_<?= $value->menu_id ?>" name="full_access[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->full_access) && $value->full_access == 1 ? 'checked=""' : '' ?>>
                                                                        <label class="custom-control-label" for="full_access_<?= $value->menu_id ?>"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="frame-wrap">
                                                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                        <input type="checkbox" class="custom-control-input check_child" id="view_<?= $value->menu_id ?>" name="view[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->view_right) && $value->view_right == 1 ? 'checked=""' : '' ?>>
                                                                        <label class="custom-control-label" for="view_<?= $value->menu_id ?>"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="frame-wrap">
                                                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                        <input type="checkbox" class="custom-control-input check_child" id="add_<?= $value->menu_id ?>" name="add[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->add_right) && $value->add_right == 1 ? 'checked=""' : '' ?>>
                                                                        <label class="custom-control-label" for="add_<?= $value->menu_id ?>"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="frame-wrap">
                                                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                        <input type="checkbox" class="custom-control-input check_child" id="edit_<?= $value->menu_id ?>" name="edit[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->edit_right) && $value->edit_right == 1 ? 'checked="checked"' : '' ?>>
                                                                        <label class="custom-control-label" for="edit_<?= $value->menu_id ?>"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="frame-wrap">
                                                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                        <input type="checkbox" class="custom-control-input check_child" id="delete_<?= $value->menu_id ?>" name="delete[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->delete_right) && $value->delete_right == 1 ? 'checked="checked"' : '' ?>>
                                                                        <label class="custom-control-label" for="delete_<?= $value->menu_id ?>"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    if (!empty($value->sub_menu)) {
                                                        $data['subMenuData'] = $value->sub_menu;
                                                        echo $this->load->view('admin/master/staff/edit_sub_menu_rights', $data, true);
                                                    }
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($user_data->user_id) && !empty($user_data->user_id) ? $user_data->user_id : '' ?>';
    $(document).ready(function () {
        var state_id = '<?= isset($user_data->user_state) && !empty($user_data->user_state) ? $user_data->user_state : '' ?>';
        var city_id = '<?= isset($user_data->user_city) && !empty($user_data->user_city) ? $user_data->user_city : '' ?>';
        cityByState(state_id, city_id);
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $("#state").select2({
            placeholder: "Select state",
            allowClear: true,
            width: '100%'
        });
        $("#city").select2({
            placeholder: "Select city",
            allowClear: true,
            width: '100%'
        });
        $('#addEditStaff').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                user_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkUserName/') ?>" + id,
                        type: "get"
                    }
                },
                user_email: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkUserEmail/') ?>" + id,
                        type: "get"
                    }
                }
            },
            messages: {
                user_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                user_email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        $(document).on('change', '.check_full_access', function () {
            var i = $(this).data('id');
            if ($(this).is(':checked')) {
                $("#view_" + i).prop("checked", true);
                $("#add_" + i).prop("checked", true);
                $("#edit_" + i).prop("checked", true);
                $("#delete_" + i).prop("checked", true);
            } else {
                $("#view_" + i).prop("checked", false);
                $("#add_" + i).prop("checked", false);
                $("#edit_" + i).prop("checked", false);
                $("#delete_" + i).prop("checked", false);
            }
        });

        $(document).on('change', '#checkall_side_menu', function () {
            if ($(this).is(':checked')) {
                $(".check_all").prop("checked", true);
                $(".check_child").prop("checked", true);
            } else {
                $(".check_all").prop("checked", false);
                $(".check_child").prop("checked", false);
            }
        });

        $(document).on('change', '.check_child', function () {
            var i = $(this).data('id');
            if ($("#view_" + i).is(':checked') && $("#add_" + i).is(':checked') && $("#edit_" + i).is(':checked') && $("#delete_" + i).is(':checked')) {
                $("#full_access_" + i).prop("checked", true);
            } else {
                $("#full_access_" + i).prop("checked", false);
            }
        });
    });

    $(document).on('change', '#state', function () {
        var state_id = $(this).val();
        cityByState(state_id);
    });
    function cityByState(state_id, city_id = '') {
        $('#city').html('');
        var city = '<option value="">Select</option>';
        if (state_id != '' && state_id != undefined) {
            $.ajax({
                type: "POST",
                url: '<?= base_url('admin/Master/getCityByState') ?>',
                data: {state_id: state_id},
                success: function (returnData) {
                    var data = JSON.parse(returnData);
                    if (data.result) {
                        if (data.city.length > 0) {
                            data.city.forEach(function (val, key) {
                                city += '<option value="' + val.id + '" ' + (city_id == val.id ? 'selected' : '') + '>' + val.city_name + '</option>';
                            });
                        }
                    }
                    $('#city').append(city);
                }
            });
        } else {
            $('#city').append(city);
    }
    }
</script>
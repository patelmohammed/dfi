<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-ticket-alt'></i> Add Gift Voucher
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Gift_voucher">Gift Voucher</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Gift_voucher/addEditGiftVoucher', $arrayName = array('id' => 'addEditGiftVoucher')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="date">Date <span class="text-danger">*</span></label>
                                <input tabindex="2" type="datetime-local" class="form-control" name="date" id="date" placeholder="Date" required>
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="date">Voucher For <span class="text-danger">*</span></label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="client" name="gift_voucher_for" checked="" value="client">
                                        <label class="custom-control-label" for="client">Client</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="agency" name="gift_voucher_for" value="agency">
                                        <label class="custom-control-label" for="agency">Agency</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3" id="client_div">
                                <label class="form-label" for="client_id">Client <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="client_id" id="client_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($customer_data) && !empty($customer_data)) {
                                        foreach ($customer_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->client_id ?>"><?= isset($value1->client_name) && !empty($value1->client_name) ? $value1->client_name : '' ?><?= (isset($value1->company_name) && !empty($value1->company_name) ? ' (' . $value1->company_name . ')' : '') ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Client Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3" id="agency_div" style="display: none;">
                                <label class="form-label" for="agency_id">Agency <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="agency_id" id="agency_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($agency_data) && !empty($agency_data)) {
                                        foreach ($agency_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->agency_id ?>"><?= isset($value1->agency_name) && !empty($value1->agency_name) ? $value1->agency_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Agency Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="agreement_id">Agreement</label>
                                <select tabindex="7" class="select2 form-control" name="agreement_id" id="agreement_id">
                                    <option></option>
                                </select>
                                <div class="invalid-feedback">
                                    Agreement Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_id">Payment Method</label>
                                <select tabindex="7" class="select2 form-control" name="payment_id" id="payment_id">
                                    <option></option>
                                    <?php
                                    if (isset($payment_data) && !empty($payment_data)) {
                                        foreach ($payment_data as $key2 => $value2) {
                                            ?>
                                            <option value="<?= $value2->payment_id ?>"><?= isset($value2->payment_name) && !empty($value2->payment_name) ? $value2->payment_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Payment Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="gift_voucher_amount">Gift Voucher Amount</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="gift_voucher_amount" id="gift_voucher_amount" placeholder="Gift Voucher Amount">
                                <div class="invalid-feedback">
                                    Voucher Amount Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description">Description</label>
                                <textarea class="form-control address" name="description" rows="5" id="description" placeholder="Description" maxlength="200" required=""></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {

        $("#client_id").select2({
            placeholder: "Select client",
            allowClear: true,
            width: '100%'
        });
        $("#agency_id").select2({
            placeholder: "Select agency",
            allowClear: true,
            width: '100%'
        });
        $("#agreement_id").select2({
            placeholder: "Select agreement",
            allowClear: true,
            width: '100%'
        });
        $("#payment_id").select2({
            placeholder: "Select payment",
            allowClear: true,
            width: '100%'
        });
        $(document).on('change', '#client_id', function () {
            $('#agreement_id').html('');
            var agreement = '<option value="">Select</option>';
            var client_id = $(this).val();
            if (client_id != '' && client_id != undefined) {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('admin/Voucher/getAgreementByClient') ?>',
                    data: {client_id: client_id},
                    success: function (returnData) {
                        var data = JSON.parse(returnData);
                        console.log(data);
                        if (data.result == true) {
                            if (data.agreement.length > 0) {
                                data.agreement.forEach(function (val, key) {
                                    agreement += '<option value="' + val.agreement_id + '">' + val.subject + '</option>';
                                });
                            }
                        }
                        $('#agreement_id').append(agreement);
                    }
                });
            } else {
                $('#agreement_id').append(agreement);
            }
        });

        $('#addEditGiftVoucher').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('change', '[name="gift_voucher_for"]', function () {
        var gift_voucher_for = $(this).val();
        gift_voucher_for_show_hide(gift_voucher_for);
    });

    function gift_voucher_for_show_hide(gift_voucher_for) {
        if (gift_voucher_for != '' && gift_voucher_for != null && gift_voucher_for != undefined && gift_voucher_for == 'client') {
            $('#client_div').show();
            $('#agency_div').hide();
        } else {
            $('#agency_div').show();
            $('#client_div').hide();
        }
    }
</script>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-ticket-alt'></i> Gift Voucher
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Gift_voucher/addEditGiftVoucher">Add Gift Voucher</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Client Name</th>
                                    <th>Agreement</th>
                                    <th>Payment Method</th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($gift_voucher_data) && !empty($gift_voucher_data)) {
                                    $sn = 0;
                                    foreach ($gift_voucher_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->datetime) && !empty($value->datetime) ? date('d-m-Y H:i', strtotime($value->datetime)) : '' ?></td>
                                            <td><?= (isset($value->gift_voucher_for) && !empty($value->gift_voucher_for) && $value->gift_voucher_for == 'client' ? (isset($value->client_id) && !empty($value->client_id) ? getClientName($value->client_id) : '') : ($value->gift_voucher_for == 'agency' ? (isset($value->agency_id) && !empty($value->agency_id) ? getAgencyName($value->agency_id) : '') : '')) ?></td>
                                            <td><?= isset($value->subject) && !empty($value->subject) ? $value->subject : '' ?></td> 
                                            <td><?= isset($value->payment_name) && !empty($value->payment_name) ? $value->payment_name : '' ?></td> 
                                            <td><?= isset($value->gift_voucher_amount) && !empty($value->gift_voucher_amount) ? $value->gift_voucher_amount : '' ?></td> 
                                            <td><?= isset($value->description) && !empty($value->description) ? $value->description : '' ?></td> 
                                            <td>
                                                <div class='d-flex'>
                                                    <?php
                                                    if ($this->role == 'Admin') {
                                                        if ($menu_rights['edit_right']) {
                                                            ?>
                                                            <a href='<?php echo base_url() ?>admin/Gift_voucher/addEditGiftVoucher/<?= $value->gift_voucher_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                                <i class="fal fa-edit"></i>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($menu_rights['delete_right']) { ?>
                                                            <a href='javascript:void(0);' data-id="<?= $value->gift_voucher_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                                <i class="fal fa-times"></i>
                                                            </a>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <a href='<?= base_url() ?>admin/Gift_voucher/printPdfGiftVoucher/<?= $value->transaction_id ?>' target="_blank" class='btn btn-icon btn-sm hover-effect-dot btn-outline-info mr-2' title='PDF' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'>
                                                        <i class="fal fa-file-pdf"></i>
                                                    </a>
                                                    <a href='javascript:void(0);' data-id="<?= $value->transaction_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2 mail_gift_voucher' title='Mail' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                        <i class="fal fa-envelope"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('admin/Gift_voucher/deleteGiftVoucher') ?>',
                    dataType: 'json',
                    data: {id: id},
                    success: function (returnData) {
                        if (returnData.result == true) {
                            window.location.reload();
                        } else {
                            swalWithBootstrapButtons.fire(
                                    "Something Wrong",
                                    "Your record not deleted :(",
                                    "error"
                                    );
                        }
                        return false;
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                        "Cancelled",
                        "Your record is safe :)",
                        "success"
                        );
            }
        });
    });

    $(document).on('click', '.mail_gift_voucher', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Gift Voucher on mail?",
            text: "Send gift voucher on mail.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, send it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('admin/Gift_voucher/mailGiftVoucher') ?>',
                    dataType: 'json',
                    data: {id: id},
                    success: function (returnData) {
                        if (returnData.result == true) {
                            swalWithBootstrapButtons.fire(
                                    "Mail",
                                    "Gift Voucher mail successfully sent.",
                                    "success"
                                    );
                        } else {
                            swalWithBootstrapButtons.fire(
                                    "Something Wrong",
                                    "something went wront try again :(",
                                    "error"
                                    );
                        }
                        return false;
                    }
                });
            }
        });
    });
</script>
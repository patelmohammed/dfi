<style>
    .bg-info-gradient{
        background-image:linear-gradient(100deg, rgba(9, 96, 165, 0.7), transparent);
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-chart-area'></i> Dashboard
        </h1>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="p-3 bg-info-gradient bg-success-500 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <span class="fw-300">Welcome To Design Factory Interior</span><br>
                        <h4><?= isset($user_detail->customer_name) && !empty($user_detail->customer_name) ? $user_detail->customer_name : '' ?></h4>
                        <?php
                        $address = '';
                        if (isset($user_detail->customer_city) && !empty($user_detail->customer_city)) {
                            $address .= $user_detail->customer_city;
                        }
                        if (isset($user_detail->customer_state) && !empty($user_detail->customer_state)) {
                            if (isset($user_detail->customer_city) && !empty($user_detail->customer_city)) {
                                $address .= ', ';
                            }
                            $address .= $user_detail->customer_state;
                        }
                        if (isset($user_detail->customer_country) && !empty($user_detail->customer_country)) {
                            if ((isset($user_detail->customer_city) && !empty($user_detail->customer_city)) || (isset($user_detail->customer_state) && !empty($user_detail->customer_state))) {
                                $address .= ', ';
                            }
                            $address .= $user_detail->customer_country;
                        }
                        if (isset($user_detail->customer_pincode) && !empty($user_detail->customer_pincode)) {
                            if ((isset($user_detail->customer_city) && !empty($user_detail->customer_city)) || (isset($user_detail->customer_state) && !empty($user_detail->customer_state)) || (isset($user_detail->customer_country) && !empty($user_detail->customer_country))) {
                                $address .= ' - ';
                            }
                            $address .= $user_detail->customer_pincode;
                        }
                        if ((!isset($user_detail->customer_city) || empty($user_detail->customer_city)) || (!isset($user_detail->customer_state) || empty($user_detail->customer_state)) || (!isset($user_detail->customer_country) || empty($user_detail->customer_country)) || (!isset($user_detail->customer_pincode) || empty($user_detail->customer_pincode))) {
                            if (isset($user_detail->customer_address) && !empty($user_detail->customer_address)) {
                                $address .= $user_detail->customer_address;
                            }
                        }
                        echo '<h4>' . $address . '</h4>';
                        ?>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($this->role != 'Admin') {
        ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="panel-container show">
                    <div class="panel-content">
                        <?php
                        if ($attendance_data->today_attendance == 0) {
                            ?>
                            <button type="button" class="btn btn-success ml-auto waves-effect waves-themed fill_attendance">
                                <span class="fal fa-user-check mr-1"></span>
                                Fill Attendance
                            </button>
                            <?php
                        } else {
                            ?>
                            <button type="button" class="btn btn-secondary ml-auto waves-effect waves-themed">
                                <span class="fal fa-user-check mr-1"></span>
                                Attendance Filled
                            </button>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</main>
<script>
    $(document).on('click', '.fill_attendance', function () {
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You want to fill attendance",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, fill it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Dashboard/fillStaffAttendance') ?>',
                            dataType: 'json',
                            data: {},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            returnData.message,
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    }
                });
    });
</script>
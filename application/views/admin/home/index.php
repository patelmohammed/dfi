<style>
    .bg-info-gradient{
        background-image:linear-gradient(100deg, rgba(9, 96, 165, 0.7), transparent);
    }
    .custom_redirect{
        cursor: pointer;
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-chart-area'></i> Dashboard
        </h1>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="p-3 bg-info-gradient bg-success-500 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-5 d-block l-h-n m-0 fw-500">
                        <span class="fw-300 font_papyrus">Welcome To Design Factory Interior</span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($this->role != 'Admin') {
        ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="alert bg-fusion-400 border-0 fade show">
                    <div class="d-flex align-items-center">
                        <div class="alert-icon">
                            <i class="fal fa-user-check text-warning"></i>
                        </div>
                        <div class="flex-1">
                            <span class="h5">Attendance</span>
                            <br>
                            <?php
                            if ($attendance_data->today_attendance == 0) {
                                ?>
                                Today attendance not filled
                                <?php
                            } else {
                                ?>
                                Today attendance already filled
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($attendance_data->today_attendance == 0) {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-warning btn-w-m fw-500 btn-sm <?= $attendance_data->today_attendance == 0 ? 'fill_attendance' : '' ?>" aria-label="Close">Fill Attendance</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <div class="col-sm-6 col-xl-3 redirect_agreement custom_redirect">
            <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($payment_info->AgreementTotal) && !empty($payment_info->AgreementTotal) ? $payment_info->AgreementTotal : 0 ?>
                        <small class="m-0 l-h-n">Total Amount</small>
                    </h3>
                </div>
                <i class="fal fa-handshake position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_voucher custom_redirect">
            <div class="p-3 bg-success-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($payment_info->TotalVoucher) && !empty($payment_info->TotalVoucher) ? $payment_info->TotalVoucher : 0 ?>
                        <small class="m-0 l-h-n">Total Paid Amount</small>
                    </h3>
                </div>
                <i class="fal fa-check-circle position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_create_voucher custom_redirect">
            <div class="p-3 bg-danger-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= (isset($payment_info->AgreementTotal) && !empty($payment_info->AgreementTotal) ? $payment_info->AgreementTotal : 0) - ( isset($payment_info->TotalVoucher) && !empty($payment_info->TotalVoucher) ? $payment_info->TotalVoucher : 0 ) ?>
                        <small class="m-0 l-h-n">Total Pending Amount</small>
                    </h3>
                </div>
                <i class="fal fa-times-circle position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_gift_voucher custom_redirect">
            <div class="p-3 bg-warning-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($payment_info->TotalGiftVoucher) && !empty($payment_info->TotalGiftVoucher) ? $payment_info->TotalGiftVoucher : 0 ?>
                        <small class="m-0 l-h-n">Total Gift Voucher Amount</small>
                    </h3>
                </div>
                <i class="fal fal fa-ticket-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
    </div>
    <div class="row" style="display: none;">
        <div class="col-sm-6 col-md-6">
            <div id="panel-9" class="panel">
                <div class="panel-hdr">
                    <h2>Payment Chart</h2>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <div id="pieChart" style="width:100%; height:300px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).on('click', '.fill_attendance', function () {
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You want to fill attendance",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, fill it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Dashboard/fillStaffAttendance') ?>',
                            dataType: 'json',
                            data: {},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            returnData.message,
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    }
                });
    });

    $('.redirect_agreement').on('click', function () {
        window.location.href = '<?= base_url('admin/Agreement') ?>';
    });
    $('.redirect_voucher').on('click', function () {
        window.location.href = '<?= base_url('admin/Voucher') ?>';
    });
    $('.redirect_create_voucher').on('click', function () {
        window.location.href = '<?= base_url('admin/Voucher/addEditVoucher') ?>';
    });
    $('.redirect_gift_voucher').on('click', function () {
        window.location.href = '<?= base_url('admin/Gift_voucher') ?>';
    });
</script>
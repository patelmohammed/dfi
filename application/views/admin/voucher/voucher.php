<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-ticket-alt'></i> Voucher
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Voucher/addEditVoucher">Add Voucher</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Client Name</th>
                                    <th>Agreement</th>
                                    <th>Payment Method</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($voucher_data) && !empty($voucher_data)) {
                                    $sn = 0;
                                    foreach ($voucher_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->datetime) && !empty($value->datetime) ? date('d-m-Y H:i', strtotime($value->datetime)) : '' ?></td>
                                            <td><?= (isset($value->voucher_for) && !empty($value->voucher_for) && $value->voucher_for == 'client' ? (isset($value->client_id) && !empty($value->client_id) ? getClientName($value->client_id) : '') : ($value->voucher_for == 'agency' ? (isset($value->agency_id) && !empty($value->agency_id) ? getAgencyName($value->agency_id) : '') : '')) ?></td>
                                            <td><?= isset($value->subject) && !empty($value->subject) ? $value->subject : '' ?></td> 
                                            <td><?= isset($value->payment_name) && !empty($value->payment_name) ? $value->payment_name : '' ?></td> 
                                            <td><?= isset($value->voucher_amount) && !empty($value->voucher_amount) ? $value->voucher_amount : '' ?></td> 
                                            <td><?= isset($value->voucher_status) && !empty($value->voucher_status) ? $value->voucher_status : '' ?></td> 
                                            <td>
                                                <div class='d-flex'>
                                                    <?php
                                                    if ($value->voucher_status == 'Pending' || $this->role == 'Admin') {
                                                        if ($menu_rights['edit_right']) {
                                                            ?>
                                                            <a href='<?php echo base_url() ?>admin/Voucher/addEditVoucher/<?= $value->voucher_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                                <i class="fal fa-edit"></i>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($menu_rights['delete_right']) { ?>
                                                            <a href='javascript:void(0);' data-id="<?= $value->voucher_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                                <i class="fal fa-trash"></i>
                                                            </a>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                        <a href='javascript:void(0);' data-id="<?= $value->voucher_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-trash"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($this->role == 'Admin' && ($value->voucher_status == 'Pending' || $value->voucher_status == 'Rejected')) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->voucher_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2 approve_voucher' title='Approve' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                            <i class="fal fa-check"></i>
                                                        </a>
                                                    <?php } if ($this->role == 'Admin' && $value->voucher_status == 'Pending') { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->voucher_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2 reject_voucher' title='Reject' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } if ($value->voucher_status == 'Approved') {
                                                        ?>
                                                        <a href='<?= base_url() ?>admin/Voucher/printPdfVoucher/<?= $value->transaction_id ?>' target="_blank" class='btn btn-icon btn-sm hover-effect-dot btn-outline-info mr-2' title='PDF' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'>
                                                            <i class="fal fa-file-pdf"></i>
                                                        </a>
                                                    <?php } else {
                                                        ?>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='PDF' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-file-pdf"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="rejectVoucher" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 1.25rem 1.25rem 0rem 1.25rem;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
            </div>
            <?= form_open('', $arrayName = array('id' => 'rejectVoucherForm')) ?>
            <div class="modal-body" style="padding: 0rem 1.25rem 0rem 1.25rem;">
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="reject_remark">Remark <span class="text-danger">*</span></label>
                        <input tabindex="2" type="text" class="form-control textonly" name="reject_remark" id="reject_remark" placeholder="Remark" required>
                        <div class="invalid-feedback">
                            Remark Required
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="hidden_voucher_id" id="hidden_voucher_id" value="">
            <div class="modal-footer" style="padding: 0rem 1.25rem 1.25rem 1.25rem;">
                <button type="submit" class="btn btn-primary" id="reject_voucher_submit">Submit</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).ready(function () {
        $('#rejectVoucherForm').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
//                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('admin/Voucher/deleteVoucher') ?>',
                    dataType: 'json',
                    data: {id: id},
                    success: function (returnData) {
                        if (returnData.result == true) {
                            window.location.reload();
                        } else {
                            swalWithBootstrapButtons.fire(
                                    "Something Wrong",
                                    "Your record not deleted :(",
                                    "error"
                                    );
                        }
                        return false;
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                        "Cancelled",
                        "Your record is safe :)",
                        "success"
                        );
            }
        });
    });

    $(document).on('click', '.approve_voucher', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to change status of this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, approve it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('admin/Voucher/approveVoucher') ?>',
                    dataType: 'json',
                    data: {id: id},
                    success: function (returnData) {
                        if (returnData.result == true) {
                            window.location.reload();
                        } else {
                            swalWithBootstrapButtons.fire(
                                    "Something Wrong",
                                    "Your record not approved :(",
                                    "error"
                                    );
                        }
                        return false;
                    },
                    error: function () {
                        swalWithBootstrapButtons.fire(
                                "Something Wrong",
                                "Your record not approved :(",
                                "error"
                                );
                    }
                });
            }
        });
    });

    $(document).on('click', '.reject_voucher', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to change status of this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, reject it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $('#reject_remark').val('');
                $('#hidden_voucher_id').val('');
                $('#hidden_voucher_id').val(id);
                $('#rejectVoucher').modal('show');
            }
        });
    });

    $(document).on('click', '#reject_voucher_submit', function () {
        $.ajax({
            type: 'POST',
            url: '<?= base_url('admin/Voucher/rejectVoucher') ?>',
            dataType: 'json',
            data: $('#rejectVoucherForm').serialize(),
            success: function (returnData) {
                if (returnData.result == true) {
                    window.location.reload(true);
                } else {
                    swalWithBootstrapButtons.fire(
                            "Something Wrong",
                            "Your record not rejected :(",
                            "error"
                            );
                    return false;
                }
            },
            error: function () {
                swalWithBootstrapButtons.fire(
                        "Something Wrong",
                        "Your record not rejected :(",
                        "error"
                        );
            }
        });
    });
</script>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Company
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Setting', $arrayName = array('id' => 'addEditCompany', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#company_setting">Company Setting</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#smtp_setting">SMTP Setting</a></li>
                        </ul>
                        <div class="tab-content py-3">
                            <div class="tab-pane fade show active" id="company_setting" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="company_name">Company Name <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="company_name" id="company_name" placeholder="Company Name" required value="<?= isset($company_data->company_name) && !empty($company_data->company_name) ? $company_data->company_name : '' ?>">
                                        <div class="invalid-feedback">
                                            Company Name Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="tag_line">Tag Line <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control address" name="tag_line" id="tag_line" placeholder="Tag Line" required value="<?= isset($company_data->tag_line) && !empty($company_data->tag_line) ? $company_data->tag_line : '' ?>">
                                        <div class="invalid-feedback">
                                            Tag Line Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        <label class="form-label" for="company_country">Country <span class="text-danger">*</span></label>
                                        <select class="select2 form-control" name="company_country" id="company_country" required="">
                                            <option></option>
                                            <?php
                                            if (isset($country_data) && !empty($country_data)) {
                                                foreach ($country_data as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($company_data->company_country) && !empty($company_data->company_country) ? set_selected($company_data->company_country, $v1->id) : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            Country Code Required
                                        </div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required value="<?= isset($company_data->mobile) && !empty($company_data->mobile) ? $company_data->mobile : '' ?>">
                                        <div class="invalid-feedback">
                                            Contact Number Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="email" class="form-control" name="email" id="email" placeholder="Email" required value="<?= isset($company_data->email) && !empty($company_data->email) ? $company_data->email : '' ?>">
                                        <div class="invalid-feedback">
                                            Email Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        <label class="form-label" for="company_country_alt">Country</label>
                                        <select class="select2 form-control" name="company_country_alt" id="company_country_alt">
                                            <option></option>
                                            <?php
                                            if (isset($country_data) && !empty($country_data)) {
                                                foreach ($country_data as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($company_data->company_country_alt) && !empty($company_data->company_country_alt) ? set_selected($company_data->company_country_alt, $v1->id) : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            Country Code Required
                                        </div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label" for="mobile_alt">Contact Number</label>
                                        <input tabindex="2" type="text" class="form-control contactnumber" name="mobile_alt" id="mobile_alt" placeholder="Contact Number" value="<?= isset($company_data->mobile_alt) && !empty($company_data->mobile_alt) ? $company_data->mobile_alt : '' ?>">
                                        <div class="invalid-feedback">
                                            Contact Number Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="email_alt">Email</label>
                                        <input tabindex="2" type="email" class="form-control" name="email_alt" id="email_alt" placeholder="Email" value="<?= isset($company_data->email_alt) && !empty($company_data->email_alt) ? $company_data->email_alt : '' ?>">
                                        <div class="invalid-feedback">
                                            Email Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                        <input type="text" tabindex="4" class="form-control address" name="address" id="address" required="" placeholder="address" value="<?= isset($company_data->address) && !empty($company_data->address) ? $company_data->address : '' ?>">
                                        <div class="invalid-feedback">
                                            Address Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="pincode">Pincode <span class="text-danger">*</span></label>
                                        <input type="text" tabindex="4" class="form-control numbersonly" name="pincode" id="pincode" required="" placeholder="Pincode" value="<?= isset($company_data->company_pincode) && !empty($company_data->company_pincode) ? $company_data->company_pincode : '' ?>">
                                        <div class="invalid-feedback">
                                            Pincode Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="company_state">State <span class="text-danger">*</span></label>
                                        <select tabindex="7" class="select2 form-control" name="company_state" id="company_state" required="">
                                            <option></option>
                                            <?php
                                            if (isset($state) && !empty($state)) {
                                                foreach ($state as $key => $state_val) {
                                                    ?>
                                                    <option value="<?= $state_val->id ?>" <?= isset($company_data->company_state) && !empty($company_data->company_state) ? set_selected($company_data->company_state, $state_val->id) : '' ?>><?= $state_val->state_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            State Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="company_city">City <span class="text-danger">*</span></label>
                                        <select  tabindex="8" class="select2 form-control" name="company_city" id="company_city" required="">
                                            <option></option>
                                        </select>
                                        <div class="invalid-feedback">
                                            City Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="company_code">Company Code <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="company_code" id="company_code" placeholder="Company Code" required value="<?= isset($company_data->company_code) && !empty($company_data->company_code) ? $company_data->company_code : '' ?>">
                                        <div class="invalid-feedback">
                                            Company Code Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="agreement_code">Agreement Code <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control address" name="agreement_code" id="agreement_code" placeholder="Agreement Code" required value="<?= isset($company_data->agreement_code) && !empty($company_data->agreement_code) ? $company_data->agreement_code : '' ?>">
                                        <div class="invalid-feedback">
                                            Agreement Code Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="voucher_code">Voucher Code <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control address" name="voucher_code" id="voucher_code" placeholder="Voucher Code" required value="<?= isset($company_data->voucher_code) && !empty($company_data->voucher_code) ? $company_data->voucher_code : '' ?>">
                                        <div class="invalid-feedback">
                                            Voucher Code Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="gift_voucher_code">Gift Voucher Code <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control address" name="gift_voucher_code" id="gift_voucher_code" placeholder="Gift Voucher Code" required value="<?= isset($company_data->gift_voucher_code) && !empty($company_data->gift_voucher_code) ? $company_data->gift_voucher_code : '' ?>">
                                        <div class="invalid-feedback">
                                            Gift Voucher Code Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <?php $image_url = (isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? $company_data->company_logo : 'assets/img/no_image.jpg'); ?>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Company Logo <i class="text-danger">(File in PNG, JPG)</i></label>
                                        <div class="custom-file" id="file_upload">
                                            <input type="file" name="company_logo" class="custom-file-input" id="company_logo" <?= isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="company_logo">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3 mt-3">
                                        <img src="<?= base_url() . $image_url ?>" class="m-2" style="height: 100%;">
                                        <input type="hidden" name="hidden_company_logo" id="hidden_company_logo" value="<?= $image_url ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <?php $image_url = (isset($company_data->pdf_header_image) && !empty($company_data->pdf_header_image) && file_exists($company_data->pdf_header_image) ? $company_data->pdf_header_image : 'assets/img/no_image.jpg'); ?>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">PDF / Email Header Image <i class="text-danger">(File in PNG, JPG)</i></label>
                                        <div class="custom-file" id="file_upload">
                                            <input type="file" name="pdf_header_image" class="custom-file-input" id="pdf_header_image" <?= isset($company_data->pdf_header_image) && !empty($company_data->pdf_header_image) && file_exists($company_data->pdf_header_image) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="pdf_header_image">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3 mt-3">
                                        <img src="<?= base_url() . $image_url ?>" class="m-2" style="height: 100px;">
                                        <input type="hidden" name="hidden_pdf_header_image" id="hidden_pdf_header_image" value="<?= $image_url ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <?php $image_url = (isset($company_data->gift_card_bg_image) && !empty($company_data->gift_card_bg_image) && file_exists($company_data->gift_card_bg_image) ? $company_data->gift_card_bg_image : 'assets/img/no_image.jpg'); ?>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Gift Card Background Image <i class="text-danger">(File in PNG, JPG)</i></label>
                                        <div class="custom-file" id="file_upload">
                                            <input type="file" name="gift_card_bg_image" class="custom-file-input" id="gift_card_bg_image" <?= isset($company_data->gift_card_bg_image) && !empty($company_data->gift_card_bg_image) && file_exists($company_data->gift_card_bg_image) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="gift_card_bg_image">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3 mt-3">
                                        <img src="<?= base_url() . $image_url ?>" class="m-2" style="height: 100px;">
                                        <input type="hidden" name="hidden_gift_card_bg_image" id="hidden_gift_card_bg_image" value="<?= $image_url ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="smtp_setting" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="smtp_host">SMTP Host <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="smtp_host" id="smtp_host" placeholder="SMTP Host" required value="<?= isset($company_data->smtp_host) && !empty($company_data->smtp_host) ? $company_data->smtp_host : '' ?>">
                                        <div class="invalid-feedback">
                                            SMTP Host Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="smtp_user">SMTP User <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="smtp_user" id="smtp_user" placeholder="SMTP User" required value="<?= isset($company_data->smtp_user) && !empty($company_data->smtp_user) ? $company_data->smtp_user : '' ?>">
                                        <div class="invalid-feedback">
                                            SMTP User Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="smtp_pass">SMTP Password <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="smtp_pass" id="smtp_pass" placeholder="SMTP Password" required value="<?= isset($company_data->smtp_pass) && !empty($company_data->smtp_pass) ? $company_data->smtp_pass : '' ?>">
                                        <div class="invalid-feedback">
                                            SMTP Password Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="smtp_port">SMTP Port <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="smtp_port" id="smtp_port" placeholder="SMTP Port" required value="<?= isset($company_data->smtp_port) && !empty($company_data->smtp_port) ? $company_data->smtp_port : '' ?>">
                                        <div class="invalid-feedback">
                                            SMTP Port Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="smtp_crypto">SMTP Encryption <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="smtp_crypto" id="smtp_crypto" placeholder="SMTP Crypto" required value="<?= isset($company_data->smtp_crypto) && !empty($company_data->smtp_crypto) ? $company_data->smtp_crypto : '' ?>">
                                        <div class="invalid-feedback">
                                            SMTP Encryption Required
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($company_data->company_id) && !empty($company_data->company_id) ? $company_data->company_id : '' ?>';
    $(document).ready(function () {
        var state_id = '<?= isset($company_data->company_state) && !empty($company_data->company_state) ? $company_data->company_state : '' ?>';
        var city_id = '<?= isset($company_data->company_city) && !empty($company_data->company_city) ? $company_data->company_city : '' ?>';
        cityByState(state_id, city_id);
        $("#company_country").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $("#company_country_alt").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $("#company_state").select2({
            placeholder: "Select state",
            allowClear: true,
            width: '100%'
        });
        $("#city").select2({
            placeholder: "Select city",
            allowClear: true,
            width: '100%'
        });
        $('#addEditCompany').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('change', '#company_state', function () {
        var state_id = $(this).val();
        cityByState(state_id);
    });

    function cityByState(state_id, city_id = '') {
        $('#company_city').html('');
        var city = '<option value="">Select</option>';
        if (state_id != '' && state_id != undefined) {
            $.ajax({
                type: "POST",
                url: '<?= base_url('admin/Master/getCityByState') ?>',
                data: {state_id: state_id},
                success: function (returnData) {
                    var data = JSON.parse(returnData);
                    if (data.result) {
                        if (data.city.length > 0) {
                            data.city.forEach(function (val, key) {
                                city += '<option value="' + val.id + '" ' + (city_id == val.id ? 'selected' : '') + '>' + val.city_name + '</option>';
                            });
                        }
                    }
                    $('#company_city').append(city);
                }
            });
        } else {
            $('#company_city').append(city);
    }
    }
</script>
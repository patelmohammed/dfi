<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user-times'></i> Leave Note
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Leave_note/addEditLeaveNote">Add Leave Note</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Staff Name</th>
                                    <th>Leave Date</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                    <th>Reject Remark</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($leave_note_all_data) && !empty($leave_note_all_data)) {
                                    $sn = 0;
                                    foreach ($leave_note_all_data as $key => $value) {
                                        $sn++;
                                        ?>
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->ref_staff_id) && !empty($value->ref_staff_id) ? getUserName($value->ref_staff_id) : '' ?></td> 
                                            <td><?= isset($value->leave_date) && !empty($value->leave_date) ? date('d-m-Y', strtotime($value->leave_date)) : '' ?></td>
                                            <td><?= isset($value->staff_reason) && !empty($value->staff_reason) ? $value->staff_reason : '' ?></td>
                                            <td class="<?= isset($value->status) && !empty($value->status) ? ($value->status == 'Pending' ? 'text-warning' : ($value->status == 'Approved' ? 'text-success' : ($value->status == 'Rejected' ? 'text-danger' : ''))) : '' ?>">
                                                <?= isset($value->status) && !empty($value->status) ? $value->status : '' ?>
                                            </td>
                                            <td><?= isset($value->reject_remark) && !empty($value->reject_remark) ? $value->reject_remark : '' ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php
                                                    if ($menu_rights['edit_right'] && $value->status == 'Pending') {
                                                        ?>
                                                        <a href='<?php echo base_url() ?>admin/Leave_note/addEditLeaveNote/<?= $value->leave_note_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                        <?php
                                                    }
                                                    if ($menu_rights['delete_right'] && $value->status == 'Pending') {
                                                        ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->leave_note_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_leave_note' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-trash"></i>
                                                        </a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href='javascript:void(0);' class='btn btn-icon btn-sm hover-effect-dot btn-outline-secondary mr-2' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-secondary-500"></div></div>'>
                                                            <i class="fal fa-trash"></i>
                                                        </a>
                                                        <?php
                                                    }
                                                    if ($value->status == 'Pending' && $this->role == 'Admin') {
                                                        ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->leave_note_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2 approve_leave_note' title='Approve' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                            <i class="fal fa-check"></i>
                                                        </a>
                                                        <a href='javascript:void(0);' data-id="<?= $value->leave_note_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2 reject_leave_note' title='Reject' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="rejectLeaveNote" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 1.25rem 1.25rem 0rem 1.25rem;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
            </div>
            <?= form_open('', $arrayName = array('id' => 'rejectLeaveNoteForm')) ?>
            <div class="modal-body" style="padding: 0rem 1.25rem 0rem 1.25rem;">
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="reject_remark">Remark <span class="text-danger">*</span></label>
                        <input tabindex="2" type="text" class="form-control textonly" name="reject_remark" id="reject_remark" placeholder="Remark" required>
                        <div class="invalid-feedback">
                            Remark Required
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="hidden_leave_note_id" id="hidden_leave_note_id" value="">
            <div class="modal-footer" style="padding: 0rem 1.25rem 1.25rem 1.25rem;">
                <button type="button" class="btn btn-primary" id="reject_leave_note_submit">Submit</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
    $(document).on('click', '.delete_leave_note', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Leave_note/deleteLeaveNote') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    }
                });
    });

    $(document).on('click', '.approve_leave_note', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You want to approve leave note!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, approve it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Leave_note/approveLeaveNote') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Attendance not approved :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    }
                });
    });

    $(document).on('click', '.reject_leave_note', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to change status of this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, reject it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $('#reject_remark').val('');
                $('#hidden_leave_note_id').val('');
                $('#hidden_leave_note_id').val(id);
                $('#rejectLeaveNote').modal('show');
            }
        });
    });

    $(document).on('click', '#reject_leave_note_submit', function () {
        $('#rejectLeaveNote').modal('hide');
        $.ajax({
            type: 'POST',
            url: '<?= base_url('admin/Leave_note/rejectLeaveNote') ?>',
            dataType: 'json',
            data: $('#rejectLeaveNoteForm').serialize(),
            success: function (returnData) {
                if (returnData.result == true) {
                    window.location.reload(true);
                } else {
                    swalWithBootstrapButtons.fire(
                            "Something Wrong",
                            "Your record not rejected :(",
                            "error"
                            );
                    return false;
                }
            },
            error: function () {
                swalWithBootstrapButtons.fire(
                        "Something Wrong",
                        "Your record not rejected :(",
                        "error"
                        );
            }
        });
    });
</script>
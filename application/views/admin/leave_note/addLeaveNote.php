<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user-times'></i> Add Leave Note
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Leave_note">Leave Note</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Leave_note/addEditLeaveNote', $arrayName = array('id' => 'addEditLeaveNote')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="date">Date <span class="text-danger">*</span></label>
                                <input tabindex="2" type="date" class="form-control" name="date" id="date" placeholder="Date" required>
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                            <?php
                            if ($this->role == 'Admin') {
                                ?>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="staff_id">Staff <span class="text-danger">*</span></label>
                                    <select class="select2 form-control" name="staff_id" id="staff_id" required="">
                                        <option></option>
                                        <?php
                                        if (isset($staff_data) && !empty($staff_data)) {
                                            foreach ($staff_data as $key1 => $value1) {
                                                ?>
                                                <option value="<?= $value1->user_id ?>"><?= isset($value1->user_name) && !empty($value1->user_name) ? $value1->user_name : '' ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div class="invalid-feedback">
                                        Staff Required
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="description">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" id="description" required rows="5"></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#staff_id").select2({
            placeholder: "Select staff",
            allowClear: true,
            width: '100%'
        });

        $('#addEditLeaveNote').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
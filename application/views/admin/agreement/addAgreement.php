<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-handshake'></i> Add Agreement
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Agreement">Agreement</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Agreement/addEditAgreement', $arrayName = array('id' => 'addEditAgreement')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_id">Client <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="client_id" id="client_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($customer_data) && !empty($customer_data)) {
                                        foreach ($customer_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->client_id ?>"><?= isset($value1->client_name) && !empty($value1->client_name) ? $value1->client_name : '' ?><?= (isset($value1->company_name) && !empty($value1->company_name) ? ' (' . $value1->company_name . ')' : '') ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Client Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="subject">Subject <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="subject" id="subject" placeholder="Subject" required>
                                <div class="invalid-feedback">
                                    Subject Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="agreement_value">Agreement Value <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="agreement_value" id="agreement_value" placeholder="Agreement Value" required>
                                <div class="invalid-feedback">
                                    Agreement Value Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="agreement_date">Agreement Date <span class="text-danger">*</span></label>
                                <input tabindex="2" type="datetime-local" class="form-control" name="agreement_date" id="agreement_date" placeholder="Agreement Date" required>
                                <div class="invalid-feedback">
                                    Agreement Date Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="third_party_id">Third Party</label>
                                <select tabindex="7" class="select2 form-control" name="third_party_id" id="third_party_id">
                                    <option></option>
                                    <?php
                                    if (isset($third_party_data) && !empty($third_party_data)) {
                                        foreach ($third_party_data as $key2 => $value2) {
                                            ?>
                                            <option value="<?= $value2->third_party_id ?>"><?= isset($value2->third_party_name) && !empty($value2->third_party_name) ? $value2->third_party_name : '' ?><?= (isset($value2->company_name) && !empty($value2->company_name) ? ' (' . $value2->company_name . ')' : '') ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Client Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="not_visible_to_client">&nbsp;</label>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="not_visible_to_client" name="not_visible_to_client">
                                    <label class="custom-control-label" for="not_visible_to_client">Hide from customer</label>
                                </div>
                                <div class="invalid-feedback">
                                    Agreement Date Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="description">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" id="description" required></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        CKEDITOR.replace('description', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},

                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });

//        $('#agreement_date').datepicker({
//            format: 'dd-mm-yyyy',
//            todayHighlight: true,
//            orientation: "bottom left",
//            templates: controls,
//            autoclose: true,
//            endDate: "nextday",
//            maxDate: today,
//            onClose: function ()
//            {
//                $(this).valid();
//            }
//        });

        $("#client_id").select2({
            placeholder: "Select client",
            allowClear: true,
            width: '100%'
        });
        $("#third_party_id").select2({
            placeholder: "Select third party",
            allowClear: true,
            width: '100%'
        });
        $('#addEditAgreement').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                client_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkClientName') ?>",
                        type: "get"
                    }
                },
                client_email: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkcCientEmail') ?>",
                        type: "get"
                    }
                },
                mobile: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkcClientPhone') ?>",
                        type: "get"
                    }
                }
            },
            messages: {
                client_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                client_email: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                mobile: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
<link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/page-invoice.css">
<style>
    .signature-pad--body {
        border-radius: 4px;
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #c0cbda;
    }
    .font_papyrus{
        font-family: 'Papyrus';
    }
</style>

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-handshake'></i> Agreement
        </h1>
        <div class="d-flex mr-0 hidden-sm-down">
            <a href="<?= base_url() ?>admin/Agreement" class="btn btn-primary ml-auto waves-effect waves-themed mr-3 btn-sm"><span class="fal fa-step-backward mr-1"></span>Back To Portal</a>
            <a href="<?= base_url() . 'Auth/printAgreement/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : '') ?>" class="btn btn-danger ml-auto waves-effect waves-themed mr-3 btn-sm"><span class="fal fa-file-pdf mr-1"></span>Download</a>
            <?php
            if (isset($agreement_data->admin_signed) && !empty($agreement_data->admin_signed) && $agreement_data->admin_signed == 1) {
                ?>
                <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-sm"><span class="fal fa-file-signature mr-1"></span>Signed</a>
                <?php
            } else {
                ?>
                <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-sm" data-toggle="modal" data-target="#sign_modal"><span class="fal fa-file-signature mr-1"></span>Sign</a>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col text-center mr-0 hidden-sm-up">
            <a href="<?= base_url() ?>admin/Agreement" class="btn btn-primary ml-auto waves-effect waves-themed mr-3 btn-xs"><span class="fal fa-step-backward mr-1"></span>Back To Portal</a>
            <a href="<?= base_url() . 'Auth/printAgreement/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : '') ?>" class="btn btn-danger ml-auto waves-effect waves-themed mr-3 btn-xs"><span class="fal fa-file-pdf mr-1"></span>Download</a>
            <?php
            if (isset($agreement_data->admin_signed) && !empty($agreement_data->admin_signed) && $agreement_data->admin_signed == 1) {
                ?>
                <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-xs"><span class="fal fa-file-signature mr-1"></span>Signed</a>
                <?php
            } else {
                ?>
                <a href="javascript:void(0);" class="btn btn-success ml-auto waves-effect waves-themed btn-xs" data-toggle="modal" data-target="#sign_modal"><span class="fal fa-file-signature mr-1"></span>Sign</a>
                <?php
            }
            ?>
        </div>
        <div class="col-xl-12">
            <div class="panel-container show">
                <div class="panel-content">
                    <div data-size="A4">
                        <div class="row">
                            <div class="col-sm-12 d-flex">
                                <div class="table-responsive">
                                    <table class="table table-clean table-sm align-self-end" style="width: 100%;background-color: #5b5b5b;">
                                        <tbody>
                                            <tr>
                                                <td style="width: 100%;text-align: center;" colspan="2">
                                                    <img src="<?= base_url() . (isset($company_data->company_logo) && !empty($company_data->company_logo) && file_exists($company_data->company_logo) ? $company_data->company_logo : '') ?>" alt="preview not available" style="opacity: 1;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%;color: white;text-align: center;font-size: 24px;" class="font_papyrus" colspan="2">
                                                    <?= isset($company_data->tag_line) && !empty($company_data->tag_line) ? $company_data->tag_line : '' ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%;color: white;text-align: right;" class="font_papyrus">
                                                    <span style="margin-right: 10px;"><?= isset($company_data->email) && !empty($company_data->email) ? $company_data->email : '' ?></span>
                                                </td>
                                                <td style="width: 50%;color: white;text-align: left;border-left: 1px solid white;" class="font_papyrus">
                                                    <span style="margin-left: 10px;"><?= isset($company_data->mobile) && !empty($company_data->mobile) ? $company_data->mobile : '' ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%;color: white;text-align: right;" class="font_papyrus">
                                                    <span style="margin-right: 10px;"><?= isset($company_data->email_alt) && !empty($company_data->email_alt) ? $company_data->email_alt : '' ?></span>
                                                </td>
                                                <td style="width: 50%;color: white;text-align: left;border-left: 1px solid white;" class="font_papyrus">
                                                    <span style="margin-left: 10px;"><?= isset($company_data->mobile_alt) && !empty($company_data->mobile_alt) ? $company_data->mobile_alt : '' ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%;color: white;text-align: center;font-size: 15px;" class="font_papyrus" colspan="2">
                                                    <?= isset($company_data->address) && !empty($company_data->address) ? $company_data->address : '' ?>
                                                    <?= isset($company_data->company_city) && !empty($company_data->company_city) ? ', ' . getCityNameById($company_data->company_city)->city_name : '' ?>
                                                    <?= isset($company_data->company_pincode) && !empty($company_data->company_pincode) ? ' - ' . $company_data->company_pincode : '' ?>
                                                    <?= isset($company_data->company_state) && !empty($company_data->company_state) ? ', ' . getStateNameById($company_data->company_state)->state_name : '' ?>
                                                    <?= isset($company_data->company_country) && !empty($company_data->company_country) ? ', ' . getCountry($company_data->company_country)->name : '' ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 d-flex">
                                <div class="table-responsive">
                                    <table class="table table-clean table-sm align-self-end">
                                        <tbody>
                                            <tr>
                                                <td class="text-left keep-print-font">
                                                    <h5 class="m-0 fw-500 h3 keep-print-font">Agreement Date:</h5>
                                                </td>
                                                <td class="text-right keep-print-font">
                                                    <h5 class="m-0 fw-700 h3 keep-print-font"><?= isset($agreement_data->agreement_date) && !empty($agreement_data->agreement_date) ? date('d/m/Y', strtotime($agreement_data->agreement_date)) : '' ?></h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left keep-print-font">
                                                    <h5 class="m-0 fw-500 h3 keep-print-font color-primary-500">Agreement Value:</h5>
                                                </td>
                                                <td class="text-right keep-print-font">
                                                    <h5 class="m-0 fw-700 h3 keep-print-font text-danger"><?= isset($agreement_data->contract_value) && !empty($agreement_data->contract_value) ? '&#8377; ' . $agreement_data->contract_value : '' ?></h5>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4 ml-sm-auto">
                                <div class="table-responsive">
                                    <table class="table table-sm table-clean text-right">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <strong>Agreement to&nbsp;:&nbsp;<?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?><?= isset($client_data->company_name) && !empty($client_data->company_name) ? ' (' . $client_data->company_name . ')' : '' ?></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $address = array();
                                                    if (isset($client_data->client_address) && !empty($client_data->client_address)) {
                                                        array_push($address, $client_data->client_address);
                                                    }
                                                    if (isset($client_data->client_city) && !empty($client_data->client_city)) {
                                                        $city_name = getCityNameById($client_data->client_city);
                                                        array_push($address, (isset($city_name->city_name) && !empty($city_name->city_name) ? $city_name->city_name : ''));
                                                    }
                                                    if (isset($client_data->client_state) && !empty($client_data->client_state)) {
                                                        $client_state = getStateNameById($client_data->client_state);
                                                        array_push($address, (isset($client_state->state_name) && !empty($client_state->state_name) ? $client_state->state_name : ''));
                                                    }
                                                    if (isset($client_data->client_pincode) && !empty($client_data->client_pincode)) {
                                                        array_push($address, $client_data->client_pincode);
                                                    }
                                                    echo implode(', ', $address);
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table mt-5">
                                        <tbody>
                                            <?= isset($agreement_data->description) && !empty($agreement_data->description) ? $agreement_data->description : '' ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 25%;">
                                                <table style="width: 90%;">
                                                    <tr>
                                                        <td style="width: 100%; height: 130px;">
                                                            <?php
                                                            if (isset($agreement_data->admin_signed) && !empty($agreement_data->admin_signed) && $agreement_data->admin_signed == 1) {
                                                                ?>
                                                                <img src="<?= base_url() . (isset($agreement_data->admin_signature_path) && !empty($agreement_data->admin_signature_path) && file_exists($agreement_data->admin_signature_path) ? $agreement_data->admin_signature_path : '') ?>" style="width: 100%;">
                                                                <?php
                                                            } else {
                                                                echo "&nbsp;";
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">
                                                            <?= isset($agreement_data->admin_acceptance_firstname) && !empty($agreement_data->admin_acceptance_firstname) ? $agreement_data->admin_acceptance_firstname : '' ?>
                                                            <?= isset($agreement_data->admin_acceptance_lastname) && !empty($agreement_data->admin_acceptance_lastname) ? $agreement_data->admin_acceptance_lastname : '' ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;"><?= isset($agreement_data->admin_acceptance_date) && !empty($agreement_data->admin_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->admin_acceptance_date)) : '' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 12%;"></td>
                                            <td style="width: 26%;">
                                                <?php if (isset($agreement_data->third_party_id) && !empty($agreement_data->third_party_id)) { ?>
                                                    <table style="width: 90%;">
                                                        <tr>
                                                            <td style="width: 100%; height: 130px;">
                                                                <?php
                                                                if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) && $agreement_data->third_party_signed == 1) {
                                                                    ?>
                                                                    <img src="<?= base_url() . (isset($agreement_data->third_party_signature_path) && !empty($agreement_data->third_party_signature_path) && file_exists($agreement_data->third_party_signature_path) ? $agreement_data->third_party_signature_path : '') ?>" style="width: 100%;">
                                                                    <?php
                                                                } else {
                                                                    echo "&nbsp;";
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <?= isset($agreement_data->third_party_acceptance_firstname) && !empty($agreement_data->third_party_acceptance_firstname) ? $agreement_data->third_party_acceptance_firstname : '' ?>
                                                                <?= isset($agreement_data->third_party_acceptance_lastname) && !empty($agreement_data->third_party_acceptance_lastname) ? $agreement_data->third_party_acceptance_lastname : '' ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;"><?= isset($agreement_data->third_party_acceptance_date) && !empty($agreement_data->third_party_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->third_party_acceptance_date)) : '' ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                        </tr>
                                                    </table>
                                                <?php } ?>
                                            </td>
                                            <td style="width: 12%;"></td>
                                            <td style="width: 25%;">
                                                <table style="width: 90%;">
                                                    <tr>
                                                        <td style="width: 100%; height: 130px;">
                                                            <?php
                                                            if (isset($agreement_data->signed) && !empty($agreement_data->signed) && $agreement_data->signed == 1) {
                                                                ?>
                                                                <img src="<?= base_url() . (isset($agreement_data->signature_path) && !empty($agreement_data->signature_path) && file_exists($agreement_data->signature_path) ? $agreement_data->signature_path : '') ?>" style="width: 100%;">
                                                                <?php
                                                            } else {
                                                                echo "&nbsp;";
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">
                                                            <?= isset($agreement_data->acceptance_firstname) && !empty($agreement_data->acceptance_firstname) ? $agreement_data->acceptance_firstname : '' ?>
                                                            <?= isset($agreement_data->acceptance_lastname) && !empty($agreement_data->acceptance_lastname) ? $agreement_data->acceptance_lastname : '' ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Name</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;"><?= isset($agreement_data->acceptance_date) && !empty($agreement_data->acceptance_date) ? date('d/m/Y', strtotime($agreement_data->acceptance_date)) : '' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%;border-top: 1px solid black;">Date</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php if ($agreement_data->admin_signed == 0) { ?>
    <div class="modal fade" id="sign_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Signature & Confirmation Of Identity
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                    </button>
                </div>
                <?php echo form_open(base_url() . 'admin/Agreement/agreementSign/' . (isset($agreement_data->agreement_id) && !empty($agreement_data->agreement_id) ? $agreement_data->agreement_id : '') . '/' . (isset($agreement_data->hash) && !empty($agreement_data->hash) ? $agreement_data->hash : ''), array('id' => 'identityConfirmationForm', 'class' => 'form-horizontal')); ?>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label class="form-label" for="first_name">First Name <span class="text-danger">*</span></label>
                            <input tabindex="2" type="text" class="form-control textonly" name="first_name" id="first_name" placeholder="First Name" required>
                            <div class="invalid-feedback">
                                First Name Required
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label class="form-label" for="last_name">Last Name <span class="text-danger">*</span></label>
                            <input tabindex="2" type="text" class="form-control textonly" name="last_name" id="last_name" placeholder="Last Name" required>
                            <div class="invalid-feedback">
                                Last Name Required
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="client_id" id="client_id" value="<?= isset($agreement_data->client) && !empty($agreement_data->client) ? $agreement_data->client : '' ?>">
                    <input type="hidden" name="staff_id" id="staff_id" value="<?= isset($agreement_data->staff_id) && !empty($agreement_data->staff_id) ? $agreement_data->staff_id : getUserName($agreement_data->InsUser) ?>">
                    <input type="hidden" name="subject" id="subject" value="<?= isset($agreement_data->subject) && !empty($agreement_data->subject) ? $agreement_data->subject : '' ?>">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                            <input tabindex="2" type="email" class="form-control" name="email" id="email" placeholder="Email" required value="<?= $this->session->userdata('user_email') ?>">
                            <div class="invalid-feedback">
                                Email Required
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label id="signatureLabel">Signature</label>
                            <div class="signature-pad--body">
                                <canvas id="signature" height="130" width="450"></canvas>
                            </div>
                            <input type="text" style="width:1px; height:1px; border:0px;" tabindex="-1" name="signature" id="signatureInput">
                            <div class="invalid-feedback">
                                Please sign the document.
                            </div>
                            <div class="dispay-block">
                                <button type="button" class="btn btn-default btn-xs clear" tabindex="-1" data-action="clear">Clear</button>
                                <button type="button" class="btn btn-default btn-xs" tabindex="-1" data-action="undo">Undo</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" autocomplete="off" data-form="#identityConfirmationForm">Save changes</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
</script>
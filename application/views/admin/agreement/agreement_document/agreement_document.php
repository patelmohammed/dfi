<link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/css/page-invoice.css">
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-pdf'></i> Agreement Document
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Agreement/addEditAgreementDocument/<?= isset($agreement_id) && !empty($agreement_id) ? $agreement_id : '' ?>">Add Agreement Document</a>
            <?php } ?>
        </div>
    </div>
    <?php if (isset($agreement_all_data) && !empty($agreement_all_data)) { ?>
        <div class="row mt-2">
            <div class="col-xl-12">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div data-size="A4" style="webkit-box-shadow: 0 0 0.2cm rgba(0, 0, 0, 0.2);box-shadow: 0 0 0.2cm rgba(0, 0, 0, 0.2);padding: 2rem;">
                            <div class="row">
                                <div class="col-sm-4 d-flex">
                                    <div class="table-responsive">
                                        <table class="table table-clean table-sm align-self-end" style="margin-bottom: 0rem;">
                                            <tbody>
                                                <tr>
                                                    <td class="text-left keep-print-font">
                                                        <h5 class="m-0 fw-500 h3 keep-print-font">Agreement Date:</h5>
                                                    </td>
                                                    <td class="text-right keep-print-font">
                                                        <h5 class="m-0 fw-700 h3 keep-print-font"><?= isset($agreement_all_data->agreement_date) && !empty($agreement_all_data->agreement_date) ? date('d/m/Y', strtotime($agreement_all_data->agreement_date)) : '' ?></h5>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left keep-print-font">
                                                        <h5 class="m-0 fw-500 h3 keep-print-font color-primary-500">Agreement Value:</h5>
                                                    </td>
                                                    <td class="text-right keep-print-font">
                                                        <h5 class="m-0 fw-700 h3 keep-print-font text-danger"><?= isset($agreement_all_data->contract_value) && !empty($agreement_all_data->contract_value) ? '&#8377; ' . $agreement_all_data->contract_value : '' ?></h5>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left keep-print-font">
                                                        <h5 class="m-0 fw-500 h3 keep-print-font color-primary-500">Agreement Subject:</h5>
                                                    </td>
                                                    <td class="text-right keep-print-font">
                                                        <h5 class="m-0 fw-700 h3 keep-print-font text-info"><?= isset($agreement_all_data->subject) && !empty($agreement_all_data->subject) ? $agreement_all_data->subject : '' ?></h5>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-4 ml-sm-auto">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-clean text-right" style="margin-bottom: 0rem;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <strong>Agreement to&nbsp;:&nbsp;<?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?><?= isset($client_data->company_name) && !empty($client_data->company_name) ? ' (' . $client_data->company_name . ')' : '' ?></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $address = array();
                                                        if (isset($client_data->client_address) && !empty($client_data->client_address)) {
                                                            array_push($address, $client_data->client_address);
                                                        }
                                                        if (isset($client_data->client_city) && !empty($client_data->client_city)) {
                                                            $city_name = getCityNameById($client_data->client_city);
                                                            array_push($address, (isset($city_name->city_name) && !empty($city_name->city_name) ? $city_name->city_name : ''));
                                                        }
                                                        if (isset($client_data->client_state) && !empty($client_data->client_state)) {
                                                            $client_state = getStateNameById($client_data->client_state);
                                                            array_push($address, (isset($client_state->state_name) && !empty($client_state->state_name) ? $client_state->state_name : ''));
                                                        }
                                                        if (isset($client_data->client_pincode) && !empty($client_data->client_pincode)) {
                                                            array_push($address, $client_data->client_pincode);
                                                        }
                                                        echo implode(', ', $address);
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Agreement</th>
                                    <th>Document</th>
                                    <th>Document Name</th>
                                    <th>Current Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($agreement_document) && !empty($agreement_document)) {
                                    $sn = 0;
                                    foreach ($agreement_document as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->subject) && !empty($value->subject) ? $value->subject : '' ?></td> 
                                            <td><?= isset($value->document_name) && !empty($value->document_name) ? $value->document_name : '' ?></td> 
                                            <td><?= isset($value->document_view_name) && !empty($value->document_view_name) ? $value->document_view_name : '' ?></td> 
                                            <td>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href="javascript:void(0);" data-id="<?= $value->ducument_id ?>" data-url="<?= base_url() ?>admin/Agreement/activeDeactiveAgreementDocument" class="activeDeactive" data-is_active="<?= $value->is_active ?>">
                                                        <?php if ($value->is_active == 1) { ?>
                                                            <span class="badge badge-success badge-pill">Active</span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger badge-pill">Inactive</span>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <a href="<?= isset($value->document_path) && !empty($value->document_path) && file_exists($value->document_path) ? base_url() . $value->document_path : 'javascript:void(0);' ?>" target="_blank" class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2' title='Download Document' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                        <i class="fal fa-eye"></i>
                                                    </a>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->ducument_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_agreement_document' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
    $(document).on('click', '.delete_agreement_document', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Agreement/deleteAgreementDocument') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>
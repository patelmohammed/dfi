<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-pdf'></i> Add Agreement Document
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Agreement/agreementDocument/<?= isset($agreement_id) && !empty($agreement_id) ? $agreement_id : '' ?>">Agreement Document</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Agreement/addEditAgreementDocument', $arrayName = array('id' => 'addEditAgreementDocument', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="agreement_id">Agreement</label>
                                <select tabindex="7" class="select2 form-control" name="agreement_id" id="agreement_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($agreement) && !empty($agreement)) {
                                        foreach ($agreement as $key => $value) {
                                            ?>
                                            <option value="<?= $value->agreement_id ?>" <?= isset($agreement_id) && !empty($agreement_id) ? set_selected($agreement_id, $value->agreement_id) : '' ?>><?= $value->subject ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Agreement Required
                                </div>
                            </div>
                        </div>
                        <div id="agreement_document_div">

                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = 0;
    $(document).ready(function () {
        agreement_document();
        $("#agreement_id").select2({
            placeholder: "Select agreement",
            allowClear: true,
            width: '100%'
        });
        $('#addEditAgreementDocument').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function agreement_document() {
        suffix++;
        var row = '';
        row += '<div class="form-row agreement_document_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label">Document <i class="text-danger">(File in PDF)</i></label>';
        row += '<div class="custom-file">';
        row += '<input type="file" name="document_name[' + suffix + ']" class="custom-file-input" id="document_name_' + suffix + '" required="">';
        row += '<label class="custom-file-label" for="document_name_' + suffix + '">Choose file</label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="document_view_name_' + suffix + '">Document Name <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input type="text" name="document_view_name[' + suffix + ']" id="document_view_name_' + suffix + '" class="form-control" required="" placeholder="Document Name" value="">';
        row += '<div class="input-group-append">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_' + suffix + '" name="is_active[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="agreement_document()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_agreement_document" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '<div class="invalid-feedback">';
        row += 'Document Name Required';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';

        $('#agreement_document_div').append(row);
    }

    $(document).on('click', '.remove_agreement_document', function () {
        var id = $(this).data('id');
        if ($('.agreement_document_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-handshake'></i> Agreement
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Agreement/addEditAgreement">Add Agreement</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Client Name</th>
                                    <th>Agreement Subject</th>
                                    <th>Agreement Value</th>
                                    <th>Agreement Date</th>
                                    <th>Hide From Customer</th>
                                    <th>Client Signed</th>
                                    <th>Your Sign</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($agreement_all_data) && !empty($agreement_all_data)) {
                                    $sn = 0;
                                    foreach ($agreement_all_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->client) && !empty($value->client) ? getClientName($value->client) : '' ?></td> 
                                            <td><a href="<?= base_url() ?>admin/Agreement/agreementDetail/<?= $value->agreement_id . '/' . $value->hash ?>" class="text-danger"><?= $value->subject ?></a></td> 
                                            <td><?= $value->contract_value ?></td>
                                            <td><?= isset($value->agreement_date) && !empty($value->agreement_date) ? date('d-m-Y', strtotime($value->agreement_date)) : '' ?></td>
                                            <td><?= $value->not_visible_to_client == 0 ? 'No' : 'Yes' ?></td>
                                            <td><?= $value->signed == 0 ? 'No' : 'Yes' ?></td>
                                            <td><?= $value->admin_signed == 0 ? 'No' : 'Yes' ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Agreement/addEditAgreement/<?= $value->agreement_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->agreement_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <a href='<?= base_url() ?>admin/Agreement/agreementDetail/<?= $value->agreement_id . '/' . $value->hash ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2' title='View' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                        <i class="fal fa-eye"></i>
                                                    </a>
                                                    <a href='<?= base_url() ?>admin/Agreement/agreementDocument/<?= $value->agreement_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2 position-relative js-waves-off' title='Upload Document' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>'>
                                                        <i class="fal fa-upload"></i>
                                                        <span class="badge border border-light rounded-pill bg-info-700 position-absolute pos-top pos-right"><?= isset($value->agreement_document_cnt) && !empty($value->agreement_document_cnt) ? $value->agreement_document_cnt : 0 ?></span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Agreement/deleteAgreement') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>
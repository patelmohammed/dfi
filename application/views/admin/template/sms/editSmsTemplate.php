<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-comment-alt-dots'></i> Edit SMS Template
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Template/smsTemplate">SMS Template</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Template/addEditSmsTemplate/' . $encrypted_id, $arrayName = array('id' => 'addEditSmsTemplate', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="type">Type <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="type" id="type" placeholder="Type" required value="<?= isset($template_data->template_for) && !empty($template_data->template_for) ? $template_data->template_for : '' ?>" readonly="">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="flow_id">Flow ID <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="flow_id" id="flow_id" placeholder="Flow ID" required value="<?= isset($template_data->flow_id) && !empty($template_data->flow_id) ? $template_data->flow_id : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row" id="sms_variable_div">
                            <?php
                            if (isset($template_variable_data) && !empty($template_variable_data)) {
                                foreach ($template_variable_data as $key => $value) {
                                    ?>
                                    <div class="col-md-12 mb-3 sms_variable" id="row_<?= $key ?>">
                                        <div class="input-group input-group-multi-transition">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Variable: </span>
                                            </div>
                                            <input tabindex="2" type="text" class="form-control" name="variable[<?= $key ?>]" id="variable_<?= $key ?>" placeholder="Variable Name" required value="<?= isset($value->variable_name) && !empty($value->variable_name) ? $value->variable_name : '' ?>" data-type="variable_name">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Value: </span>
                                            </div>
                                            <input tabindex="2" type="text" class="form-control" name="variable_value[<?= $key ?>]" id="variable_value_<?= $key ?>" placeholder="Value" required value="<?= isset($value->variable_value) && !empty($value->variable_value) ? $value->variable_value : '' ?>" data-type="variable_value">
                                            <div class="input-group-append">
                                                <?php if ($key != 0) { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_sms_variable" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                        <i class="fal fa-minus"></i>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-secondary" title="You cant delete this" data-toggle="tooltip">
                                                        <i class="fal fa-minus"></i>
                                                    </a>
                                                <?php } ?>
                                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="sms_variable()" title="Add" data-toggle="tooltip">
                                                    <i class="fal fa-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($template_variable_data) && !empty($template_variable_data) ? count($template_variable_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            sms_variable();
        }

        $('#addEditSmsTemplate').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                if (element.attr('data-type') == 'variable_name' || element.attr('data-type') == 'variable_value') {
                } else {
                    error.insertAfter(element.next());
                    element.next().next().addClass('text-danger');
                }
            }
        });
    });

    function sms_variable() {
        var row = '';
        row += '<div class="col-md-12 mb-3 sms_variable" id="row_' + suffix + '">';
        row += '<div class="input-group input-group-multi-transition">';
        row += '<div class="input-group-prepend">';
        row += '<span class="input-group-text">Variable: </span>';
        row += '</div>';
        row += '<input tabindex="2" type="text" class="form-control" name="variable[' + suffix + ']" id="variable_' + suffix + '" placeholder="Variable Name" required data-type="variable_name">';
        row += '<div class="input-group-append">';
        row += '<span class="input-group-text">Value: </span>';
        row += '</div>';
        row += '<input tabindex="2" type="text" class="form-control" name="variable_value[' + suffix + ']" id="variable_value_' + suffix + '" placeholder="Value" required data-type="variable_value">';
        row += '<div class="input-group-append">';
        if (suffix != 0) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_sms_variable" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-secondary" title="Delete" data-toggle="tooltip">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="sms_variable()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#sms_variable_div').append(row);
        suffix++;
    }

    $(document).on('click', '.remove_sms_variable', function () {
        var id = $(this).data('id');
        if ($('.sms_variable').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>
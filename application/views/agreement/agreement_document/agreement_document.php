<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-pdf'></i> Agreement Document
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Agreement</th>
                                    <th>Document Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($agreement_document) && !empty($agreement_document)) {
                                    $sn = 0;
                                    foreach ($agreement_document as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->subject) && !empty($value->subject) ? $value->subject : '' ?></td>
                                            <td><?= isset($value->document_view_name) && !empty($value->document_view_name) ? $value->document_view_name : '' ?></td> 
                                            <td>
                                                <div class='d-flex'>
                                                    <a href="<?= isset($value->document_path) && !empty($value->document_path) && file_exists($value->document_path) ? base_url() . $value->document_path : 'javascript:void(0);' ?>" target="_blank" class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2' title='Download Document' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                        <i class="fal fa-eye"></i>
                                                    </a>
                                                    <a href="<?= isset($value->document_path) && !empty($value->document_path) && file_exists($value->document_path) ? base_url() . $value->document_path : 'javascript:void(0);' ?>" download="" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2' title='Download Document' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                        <i class="fal fa-download"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
</script>
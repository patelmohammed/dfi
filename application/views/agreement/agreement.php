<style>
    .signature-pad--body {
        border-radius: 4px;
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #c0cbda;
    }
</style>

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-handshake'></i> Agreement
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Agreement Subject</th>
                                    <th>Agreement Value</th>
                                    <th>Agreement Date</th>
                                    <th>Your Sign</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($agreement_all_data) && !empty($agreement_all_data)) {
                                    $sn = 0;
                                    foreach ($agreement_all_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><a href="<?= base_url() ?>Agreement/agreementDetail/<?= $value->agreement_id . '/' . $value->hash ?>" class="text-danger"><?= $value->subject ?></a></td> 
                                            <td><?= $value->contract_value ?></td>
                                            <td><?= isset($value->agreement_date) && !empty($value->agreement_date) ? date('d-m-Y', strtotime($value->agreement_date)) : '' ?></td>
                                            <td><?= $value->signed == 0 ? 'No' : 'Yes' ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <a href='<?= base_url() ?>Agreement/agreementDetail/<?= $value->agreement_id . '/' . $value->hash ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-success mr-2' title='View' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'>
                                                        <i class="fal fa-eye"></i>
                                                    </a>
                                                    <a href='<?= base_url() ?>Agreement/agreementDocument/<?= $value->agreement_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2 position-relative js-waves-off' title='View Document' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>'>
                                                        <i class="fal fa-file-pdf"></i>
                                                        <span class="badge border border-light rounded-pill bg-info-700 position-absolute pos-top pos-right"><?= isset($value->agreement_document_cnt) && !empty($value->agreement_document_cnt) ? $value->agreement_document_cnt : 0 ?></span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
</script>
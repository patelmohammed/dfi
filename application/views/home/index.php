<style>
    .bg-info-gradient{
        background-image:linear-gradient(100deg, rgba(9, 96, 165, 0.7), transparent);
    }
    .redirect_agreement{
        cursor: pointer;
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-chart-area'></i> Dashboard
        </h1>
    </div>
    <?php if (isset($client_data) && !empty($client_data) && isset($client_data->client_email) && !empty($client_data->client_email) && $client_data->is_email_verified == 0) { ?>
        <div class="row">
            <div class="col-sm-12 col-xl-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="fal fa-times-square"></i></span>
                    </button>
                    <strong>Verify your email address as soon as possible.</strong>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="text-danger" id="resend_verification_email">Resend Verification Mail</a>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($agreement_count_data->total_agreement) && !empty($agreement_count_data->total_agreement) ? $agreement_count_data->total_agreement : 0 ?>
                        <small class="m-0 l-h-n">Your Agreement</small>
                    </h3>
                </div>
                <i class="fal fa-handshake position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-danger-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($agreement_count_data->total_pending_agreement) && !empty($agreement_count_data->total_pending_agreement) ? $agreement_count_data->total_pending_agreement : 0 ?>
                        <small class="m-0 l-h-n">Pending Agreement</small>
                    </h3>
                </div>
                <i class="fal fa-pen position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-success-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($agreement_count_data->total_signed_agreement) && !empty($agreement_count_data->total_signed_agreement) ? $agreement_count_data->total_signed_agreement : 0 ?>
                        <small class="m-0 l-h-n">Signed Agreement</small>
                    </h3>
                </div>
                <i class="fal fa-check-circle position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-success-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($voucher_count_data->total_voucher) && !empty($voucher_count_data->total_voucher) ? $voucher_count_data->total_voucher : 0 ?>
                        <small class="m-0 l-h-n">Your Voucher</small>
                    </h3>
                </div>
                <i class="fal fa-ticket-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-warning-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($voucher_count_data->total_pending) && !empty($voucher_count_data->total_pending) ? $voucher_count_data->total_pending : 0 ?>
                        <small class="m-0 l-h-n">Pending Voucher</small>
                    </h3>
                </div>
                <i class="fal fa-pen position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($voucher_count_data->total_approved) && !empty($voucher_count_data->total_approved) ? $voucher_count_data->total_approved : 0 ?>
                        <small class="m-0 l-h-n">Approved Voucher</small>
                    </h3>
                </div>
                <i class="fal fa-check-circle position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 redirect_agreement">
            <div class="p-3 bg-danger-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($voucher_count_data->total_rejected) && !empty($voucher_count_data->total_rejected) ? $voucher_count_data->total_rejected : 0 ?>
                        <small class="m-0 l-h-n">Rejected Voucher</small>
                    </h3>
                </div>
                <i class="fal fa-times-circle position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 200,
            "hideDuration": 100,
            "timeOut": 4000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
        }
    });
    $('.redirect_agreement').on('click', function () {
        window.location.href = '<?= base_url('Agreement') ?>';
    });

    $(document).on('click', '#resend_verification_email', function () {
        $.ajax({
            url: '<?= base_url() ?>Auth/resendVarificationMail',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (response) {
                if (response.result == true) {
                    toastr["success"](response.message);
                } else {
                    toastr["error"](response.message);
                }
            }
        });
    });
</script>
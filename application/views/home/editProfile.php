<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Profile
        </h1>
        <div class="d-flex mr-0">

        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'Dashboard/addEditProfile', $arrayName = array('id' => 'addEditProfile', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_name">Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="user_name" id="user_name" placeholder="Name" required value="<?= isset($user_data->user_name) && !empty($user_data->user_name) ? $user_data->user_name : '' ?>">
                                <div class="invalid-feedback">
                                    Name Required
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="country_id">Country Code <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="country_id" id="country_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($country_data) && !empty($country_data)) {
                                        foreach ($country_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($user_data->country_id) && !empty($user_data->country_id) ? set_selected($user_data->country_id, $v1->id) : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Country Code Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="user_number">Contact Number</label>
                                <input type="text" name="user_number" id="user_number" class="form-control numbersonly" placeholder="Contact Number" value="<?= isset($user_data->user_number) && !empty($user_data->user_number) ? $user_data->user_number : '' ?>" required="">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="old_password">Old Password</label>
                                <input tabindex="2" type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="new_password">New Password</label>
                                <input tabindex="2" type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password" value="">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="confirm_new_password">Confirm New Password</label>
                                <input tabindex="2" type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Profile Photo <i class="text-danger">(File in JPG,PNG) File size 48X48</i></label>
                                <div class="custom-file">
                                    <input type="file" name="profile_photo" class="custom-file-input image_validation_1mb" id="profile_photo">
                                    <label class="custom-file-label" for="profile_photo">Choose file</label>
                                </div>
                                <div class="invalid-feedback">
                                    Profile Photo Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <img src="<?= isset($user_data->profile_photo) && !empty($user_data->profile_photo) && file_exists($user_data->profile_photo) ? base_url() . $user_data->profile_photo : base_url('assets/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($user_data->profile_photo) && !empty($user_data->profile_photo) ? $user_data->profile_photo : '' ?>" name="hidden_profile_photo"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $('#addEditProfile').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
<?php
if (isset($notification_data) && !empty($notification_data)) {
    foreach ($notification_data as $key => $value) {
        ?>
        <li <?= $key % 2 == 0 ? '' : 'class="unread"' ?>>
            <div class="d-flex align-items-center">
                <span class="d-flex flex-column flex-1 ml-1">
                    <span class="name">
                        <?= isset($value->ref_staff_name) && !empty($value->ref_staff_name) ? $value->ref_staff_name . ' Agreement Signed' : '' ?>
                        <span class="btn btn-outline-danger btn-sm btn-icon rounded-circle waves-effect waves-themed fw-n position-absolute pos-top pos-right mt-1 delete_notification" data-url="<?= base_url() ?>Dashboard/deleteNotification" data-id="<?= $value->id ?>"><i class="fal fa-times"></i></span>
                    </span>
                    <span class="msg-a fs-sm">Agreement: <?= isset($value->ref_agreement_subject) && !empty($value->ref_agreement_subject) ? $value->ref_agreement_subject : '' ?></span>
                    <span class="msg-b fs-xs"><?= $value->contract_value ?></span>
                    <span class="fs-nano text-muted mt-1"><?= getFuzzy(strtotime($value->datetime)) ?></span>
                </span>
            </div>
        </li>
        <?php
    }
}
?>
<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).on('click', '.delete_notification', function () {
        var url = $(this).data('url');
        var id = $(this).data('id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    }
                });
    });
</script>
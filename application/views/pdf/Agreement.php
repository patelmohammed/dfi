<page backtop="50mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <style>
        p{
            padding: 0;
            line-height: normal;
            margin: 0;
        }
    </style>
    <page_header>
        <img src="<?= base_url() . (isset($company_data->pdf_header_image) && !empty($company_data->pdf_header_image) && file_exists($company_data->pdf_header_image) ? $company_data->pdf_header_image : 'assets/img/header.png') ?>" alt="preview not available" style="width: 100%;">
    </page_header>
    <page_footer>
        <p style="text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></p>
    </page_footer>
    <br>
    <?= isset($agreement_data->description) && !empty($agreement_data->description) ? $agreement_data->description : '' ?>
    <br>
    <br>
    <table style="width: 100%;">
        <tr>
            <td style="width: 25%;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; height: 130px;">
                            <?php
                            if (isset($agreement_data->admin_signed) && !empty($agreement_data->admin_signed) && $agreement_data->admin_signed == 1) {
                                ?>
                                <img src="<?= base_url() . (isset($agreement_data->admin_signature_path) && !empty($agreement_data->admin_signature_path) && file_exists($agreement_data->admin_signature_path) ? $agreement_data->admin_signature_path : '') ?>" style="width: 100%;">
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">
                            <?= isset($agreement_data->admin_acceptance_firstname) && !empty($agreement_data->admin_acceptance_firstname) ? $agreement_data->admin_acceptance_firstname : '' ?>
                            <?= isset($agreement_data->admin_acceptance_lastname) && !empty($agreement_data->admin_acceptance_lastname) ? $agreement_data->admin_acceptance_lastname : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;"><?= isset($agreement_data->staff_id) && !empty($agreement_data->staff_id) ? getUserName($agreement_data->staff_id) : getUserName($agreement_data->InsUser) ?></td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;"><?= isset($agreement_data->admin_acceptance_date) && !empty($agreement_data->admin_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->admin_acceptance_date)) : '' ?></td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;">Date</td>
                    </tr>
                </table>
            </td>
            <td style="width: 12%;"></td>
            <td style="width: 26%;">
                <?php if (isset($agreement_data->third_party_id) && !empty($agreement_data->third_party_id)) { ?>
                    <table style="width: 90%;">
                        <tr>
                            <td style="width: 100%; height: 130px;">
                                <?php
                                if (isset($agreement_data->third_party_signed) && !empty($agreement_data->third_party_signed) && $agreement_data->third_party_signed == 1) {
                                    ?>
                                    <img src="<?= base_url() . (isset($agreement_data->third_party_signature_path) && !empty($agreement_data->third_party_signature_path) && file_exists($agreement_data->third_party_signature_path) ? $agreement_data->third_party_signature_path : '') ?>" style="width: 100%;">
                                    <?php
                                } else {
                                    echo "&nbsp;";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <?= isset($agreement_data->third_party_acceptance_firstname) && !empty($agreement_data->third_party_acceptance_firstname) ? $agreement_data->third_party_acceptance_firstname : '' ?>
                                <?= isset($agreement_data->third_party_acceptance_lastname) && !empty($agreement_data->third_party_acceptance_lastname) ? $agreement_data->third_party_acceptance_lastname : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;border-top: 1px solid black;">Name</td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100%;"><?= isset($agreement_data->third_party_acceptance_date) && !empty($agreement_data->third_party_acceptance_date) ? date('d/m/Y', strtotime($agreement_data->third_party_acceptance_date)) : '' ?></td>
                        </tr>
                        <tr>
                            <td style="width: 100%;border-top: 1px solid black;">Date</td>
                        </tr>
                    </table>
                <?php } ?>
            </td>
            <td style="width: 12%;"></td>
            <td style="width: 25%;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; height: 130px;">
                            <?php
                            if (isset($agreement_data->signed) && !empty($agreement_data->signed) && $agreement_data->signed == 1) {
                                ?>
                                <img src="<?= base_url() . (isset($agreement_data->signature_path) && !empty($agreement_data->signature_path) && file_exists($agreement_data->signature_path) ? $agreement_data->signature_path : '') ?>" style="width: 100%;">
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;">Signature</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">
                            <?= isset($agreement_data->acceptance_firstname) && !empty($agreement_data->acceptance_firstname) ? $agreement_data->acceptance_firstname : '' ?>
                            <?= isset($agreement_data->acceptance_lastname) && !empty($agreement_data->acceptance_lastname) ? $agreement_data->acceptance_lastname : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;"><?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?><?= isset($client_data->company_name) && !empty($client_data->company_name) ? ' (' . $client_data->company_name . ')' : '' ?></td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 100%;"><?= isset($agreement_data->acceptance_date) && !empty($agreement_data->acceptance_date) ? date('d/m/Y', strtotime($agreement_data->acceptance_date)) : '' ?></td>
                    </tr>
                    <tr>
                        <td style="width: 100%;border-top: 1px solid black;">Date</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</page>
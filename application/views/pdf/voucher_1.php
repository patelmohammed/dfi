<html>
    <head>
        <title>Receipt</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            /* CLIENT-SPECIFIC STYLES */
            body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
            table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
            img { -ms-interpolation-mode: bicubic; }

            /* RESET STYLES */
            img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
            table { border-collapse: collapse !important; }
            body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

            /* iOS BLUE LINKS */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            /* MEDIA QUERIES */
            @media screen and (max-width: 480px) {
            }
            div[style*="margin: 16px 0;"] { margin: 0 !important; }
        </style>
    </head>
    <body>
        <table style="border: 2px solid #e48228; width: 100%;">
            <tr>
                <td style="width:100%;" colspan="2">
                    <img src="<?= base_url() . (isset($company_data->pdf_header_image) && !empty($company_data->pdf_header_image) && file_exists($company_data->pdf_header_image) ? $company_data->pdf_header_image : 'assets/img/header.png') ?>" alt="preview not available" style="width: 100%;">
                </td>
            </tr>
            <tr>
                <td style="width:100%;" colspan="2">
                    <table style="text-align: right; width: 100%;">
                        <tr>
                            <td style="text-align: left;" style="width: 100%;"><b>Voucher Date : </b><?= isset($payment_data->datetime) && !empty($payment_data->datetime) ? date('d-m-Y', strtotime($payment_data->datetime)) : '' ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" style="width: 100%;"><b>Voucher No : </b><?= isset($payment_data->voucher_no) && !empty($payment_data->voucher_no) ? $payment_data->voucher_no : '' ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;border-bottom: 1px solid black;border-top: 1px solid black;width: 100%;">Voucher</td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: right; width: 15%;">Received From </td>
                            <td style="width: 2%;">:</td>
                            <td style="border-bottom: 1px dashed gray; width: 63%;"><?= isset($customer_detail->client_name) && !empty($customer_detail->client_name) ? $customer_detail->client_name : '' ?></td>
                            <td style="text-align: left;width: 20%;">&nbsp;&nbsp;Amount : <b><?= isset($payment_data->voucher_amount) && !empty($payment_data->voucher_amount) ? number_format($payment_data->voucher_amount, 2) : '' ?></b></td>
                        </tr>
                        <tr><td colspan="4">&nbsp;</td></tr>
                        <tr>
                            <td style="text-align: right;width: 15%;">Amount &nbsp;&nbsp;</td>
                            <td style="width: 2%;">:</td>
                            <td style="border-bottom: 1px dashed gray;width: 63%;" ><?= isset($payment_data->voucher_amount) && !empty($payment_data->voucher_amount) ? getIndianCurrency($payment_data->voucher_amount) : '' ?></td>
                            <td style="text-align: left;width: 20%;" ></td>
                        </tr>
                        <tr><td colspan="4" style="width: 100%;">&nbsp;</td></tr>
                        <tr>
                            <td style="text-align: right;width: 15%;">For &nbsp;&nbsp;</td>
                            <td style="width: 2%;">:</td>
                            <td style="border-bottom: 1px dashed gray;width: 63%;">
                                <?php
                                $str = '';
                                if (isset($payment_data->description) && !empty($payment_data->description)) {
                                    $str = preg_replace("/(.{80})/", "$1<br/>", $payment_data->description);
                                }
                                echo $str;
                                ?>
                            </td>
                            <td style="width: 20%;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 60%;">
                                <table style="width: 100%;">
<!--                                    <tr>
                                        <td style="width: 100%;">
                                            <table style="width: 100%;">-->
                                    <?php
                                    if (isset($payment_method) && !empty($payment_method)) {
                                        $payment_metthod_cnt = count($payment_method);
                                        $tmp_cnt = 0;
                                        foreach ($payment_method as $key => $value) {
                                            $tmp_cnt++;
                                            if ($key % 3 == 0) {
                                                echo "<tr>";
                                            }
                                            ?>
                                            <td style="width: 33%;">
                                                <img src="<?= base_url() . 'assets/img/' . (isset($payment_data->ref_payment_id) && !empty($payment_data->ref_payment_id) ? ($payment_data->ref_payment_id == $value->payment_id ? 'checked_checkbox.png' : 'unchecked_checkbox.png') : 'unchecked_checkbox.png') ?>" style="width: 11%;">
                                                <?= isset($value->payment_name) && !empty($value->payment_name) ? $value->payment_name : '' ?>
                                            </td>
                                            <?php
                                            if ($key % 3 == 2 || ($payment_metthod_cnt == $tmp_cnt)) {
                                                echo "</tr>";
                                            }
                                        }
                                    }
                                    ?>
                                    <!--                                            </table>
                                                                            </td>
                                                                        </tr>-->
                                </table>
                            </td>
                            <td style="width: 40%;">
                                <table style="width: 100%;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;">
                                    <tr>
                                        <td style="width: 50%;">&nbsp;Current Payment</td>
                                        <td style="width: 5%;">:</td>
                                        <td style="text-align: right;width: 45%;"><?= isset($payment_data->voucher_amount) && !empty($payment_data->voucher_amount) ? number_format($payment_data->voucher_amount, 2) : '' ?> &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <?php
                                        if (($totalDue->AgreementTotal + $totalDue->CostTotal) > $totalDue->VoucherTotal) {
                                            $final_label = 'Previous Due';
                                            $final_total = number_format((($totalDue->AgreementTotal + $totalDue->CostTotal) - $totalDue->VoucherTotal), 2);
                                        } else if (($totalDue->AgreementTotal + $totalDue->CostTotal) === $totalDue->VoucherTotal) {
                                            $final_label = 'Previous Due';
                                            $final_total = '0.00';
                                        } else {
                                            $final_label = 'Previous Credit';
                                            $final_total = number_format(abs($totalDue->VoucherTotal - ($totalDue->AgreementTotal + $totalDue->CostTotal)), 2);
                                        }
                                        ?>
                                        <td style="width: 50%;">&nbsp;<?= $final_label ?> </td>
                                        <td style="width: 5%;">:</td>
                                        <td style="text-align: right;width: 45%;"><?= $final_total ?> &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <?php
                                        if (($totalDue->AgreementTotal + $totalDue->CostTotal) <= $totalDue->VoucherTotal) {
                                            $Grandlabel = 'Credit';
                                            $DueBalance = number_format($totalDue->VoucherTotal - ($totalDue->AgreementTotal + $totalDue->CostTotal) + ($transaction_type->voucher_status == 'Approved' ? $payment_data->voucher_amount : 0), 2);
                                        } else {
                                            $Grandlabel = 'Due';
                                            $DueBalance = number_format(($totalDue->AgreementTotal + $totalDue->CostTotal) - $totalDue->VoucherTotal - ($transaction_type->voucher_status == 'Approved' ? $payment_data->voucher_amount : 0), 2);
                                        }
                                        ?>
                                        <td style="width: 100%;border-top: 1px solid black;" colspan="3">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 50%;">&nbsp;<?= $Grandlabel ?></td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="text-align: right;width: 45%;"><?= $DueBalance ?> &nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
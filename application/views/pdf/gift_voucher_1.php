<?php
$str = '';
$str .= (isset($customer_detail->client_name) && !empty($customer_detail->client_name) ? $customer_detail->client_name : '');
$str .= ' you’ve won a free ';
$str .= (isset($payment_data->gift_voucher_amount) && !empty($payment_data->gift_voucher_amount) ? $payment_data->gift_voucher_amount : '');
$str .= 'gift card voucher and your voucher code is ';
$str .= (isset($payment_data->redeem_code) && !empty($payment_data->redeem_code) ? $payment_data->redeem_code : '');
?>
<page backtop="5mm" backbottom="0mm" backleft="5mm" backright="5mm" backcolor="#a5a0a0" backimg="<?= base_url() . (file_exists('assets/img/giftcard.jpg') ? 'assets/img/giftcard.jpg' : '') ?>"><!--backcolor="#a5a0a0"-->
    <table style="width: 100%;background-color: white;" cellspacing="-1" cellpadding="0">
        <tr>
            <td style="width: 100%;">
                <img src="<?= base_url() . (isset($company_data->pdf_header_image) && !empty($company_data->pdf_header_image) && file_exists($company_data->pdf_header_image) ? $company_data->pdf_header_image : 'assets/img/header.png') ?>" alt="preview not available" style="width: 100%;">
            </td>
        </tr>
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="<?= base_url('assets/img/giftcard-dfi.jpg') ?>" alt="preview not available" style="width: 100%; margin-top: -1pt;">
            </td>
        </tr>
<!--        <tr>
            <td style="width: 100%;text-align: center;font-size: 16pt;">
                <span style="color: #5b5b5b;">
                    Congratulations!
                </span>
            </td>
        </tr>-->
        <tr>
            <!--<td style="width: 100%;text-align: center;font-size: 16pt; height: 113pt;vertical-align: middle;">-->
            <td style="width: 100%;text-align: center;font-size: 16pt; height: 138pt;vertical-align: middle;">
                <span style="color: #5b5b5b;">
                    Congratulations!<br><br>
                </span>
                <span style="color: #5b5b5b;">
                    <span style="color: #e48228;">
                        <?= isset($customer_detail->client_name) && !empty($customer_detail->client_name) ? $customer_detail->client_name : '' ?>
                    </span>
                    you’ve won a free 
                    <span style="color: #e48228;">
                        <?php
                        if (isset($payment_data->gift_voucher_amount) && !empty($payment_data->gift_voucher_amount) && $payment_data->gift_voucher_amount != 0) {
                            echo $payment_data->gift_voucher_amount;
                        } else {
                            echo (isset($payment_data->description) && !empty($payment_data->description) ? $payment_data->description : '');
                        }
                        ?>
                    </span>
                    gift card voucher and your voucher code is 
                    <span style="color: #e48228;">
                        <?= isset($payment_data->redeem_code) && !empty($payment_data->redeem_code) ? $payment_data->redeem_code : '' ?>
                    </span> 
                </span>
            </td>
        </tr>
        <?php
//        for ($i = 1; $i < (9 - (ceil(strlen($str) / 72))); $i++) {
        ?>
<!--            <tr>
                <td style="width: 100%;">&nbsp;</td>
            </tr>-->
        <?php
//        }
        ?>
    </table>
</page>
<page backtop="5mm" backbottom="0mm" backleft="5mm" backright="5mm" backcolor="#a5a0a0" backimg="<?= base_url() . (file_exists($company_data->gift_card_bg_image) ? $company_data->gift_card_bg_image : '') ?>"><!--backcolor="#a5a0a0"-->
    <table style="width: 100%;" cellspacing="-1" cellpadding="0">
        <tr>
            <td style="width: 100%;text-align: center;font-size: 16pt;padding-top: 250pt;">
                <span style="color: #5b5b5b;">
                    Congratulations!<br><br>
                </span>
                <span style="color: #5b5b5b;">
                    <span style="color: #e48228;">
                        <?= isset($customer_detail->client_name) && !empty($customer_detail->client_name) ? $customer_detail->client_name : '' ?>
                    </span>
                    you’ve won a free 
                    <span style="color: #e48228;">
                        <?php
                        if (isset($payment_data->gift_voucher_amount) && !empty($payment_data->gift_voucher_amount) && $payment_data->gift_voucher_amount != 0) {
                            echo $payment_data->gift_voucher_amount;
                        } else {
                            echo (isset($payment_data->description) && !empty($payment_data->description) ? $payment_data->description : '');
                        }
                        ?>
                    </span>
                    gift voucher.
                </span>
            </td>
        </tr>
    </table>
</page>
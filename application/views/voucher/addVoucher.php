<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-ticket-alt'></i> Add Voucher
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>Voucher">Voucher</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'Voucher/addEditVoucher', $arrayName = array('id' => 'addEditVoucher')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="date">Date <span class="text-danger">*</span></label>
                                <input tabindex="2" type="datetime-local" class="form-control" name="date" id="date" placeholder="Date" required>
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="agreement_id">Agreement</label>
                                <select tabindex="7" class="select2 form-control" name="agreement_id" id="agreement_id">
                                    <option></option>
                                    <?php
                                    if (isset($agreement_data) && !empty($agreement_data)) {
                                        foreach ($agreement_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->agreement_id ?>"><?= isset($value1->subject) && !empty($value1->subject) ? $value1->subject : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Agreement Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_id">Payment Method <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="payment_id" id="payment_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($payment_data) && !empty($payment_data)) {
                                        foreach ($payment_data as $key2 => $value2) {
                                            ?>
                                            <option value="<?= $value2->payment_id ?>"><?= isset($value2->payment_name) && !empty($value2->payment_name) ? $value2->payment_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Payment Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="voucher_amount">Voucher Amount <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="voucher_amount" id="voucher_amount" placeholder="Voucher Amount" required>
                                <div class="invalid-feedback">
                                    Voucher Amount Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description">Description</label>
                                <textarea class="form-control address" name="description" rows="5" id="description" placeholder="Description" maxlength="200"></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
//        $('#date').datepicker({
//            format: 'dd-mm-yyyy',
//            todayHighlight: true,
//            orientation: "bottom left",
//            templates: controls,
//            autoclose: true,
//            endDate: "nextday",
//            maxDate: today,
//            onClose: function ()
//            {
//                $(this).valid();
//            }
//        });

        $("#agreement_id").select2({
            placeholder: "Select agreement",
            allowClear: true,
            width: '100%'
        });
        $("#payment_id").select2({
            placeholder: "Select payment",
            allowClear: true,
            width: '100%'
        });
        $(document).on('change', '#staff_id', function () {
            $('#agreement_id').html('');
            var agreement = '<option value="">Select</option>';
            var staff_id = $(this).val();
            var staff_role = $(this).find(':selected').attr('data-role');
            if (staff_id != '' && staff_id != undefined) {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('Voucher/getAgreementByUser') ?>',
                    data: {staff_id: staff_id, staff_role: staff_role},
                    success: function (returnData) {
                        var data = JSON.parse(returnData);
                        if (data.result == true) {
                            if (data.agreement.length > 0) {
                                data.agreement.forEach(function (val, key) {
                                    agreement += '<option value="' + val.agreement_id + '">' + val.subject + '</option>';
                                });
                            }
                        }
                        $('#agreement_id').append(agreement);
                    }
                });
            } else {
                $('#agreement_id').append(agreement);
            }
        });

        $('#addEditVoucher').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>
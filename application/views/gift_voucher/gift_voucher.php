<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-ticket-alt'></i> Gift Voucher
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Client Name</th>
                                    <th>Agreement</th>
                                    <th>Payment Method</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($voucher_data) && !empty($voucher_data)) {
                                    $sn = 0;
                                    foreach ($voucher_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->datetime) && !empty($value->datetime) ? date('d-m-Y', strtotime($value->datetime)) : '' ?></td>
                                            <td><?= isset($value->client_id) && !empty($value->client_id) ? getClientName($value->client_id) : '' ?></td> 
                                            <td><?= isset($value->subject) && !empty($value->subject) ? $value->subject : '' ?></td> 
                                            <td><?= isset($value->payment_name) && !empty($value->payment_name) ? $value->payment_name : '' ?></td> 
                                            <td><?= isset($value->gift_voucher_amount) && !empty($value->gift_voucher_amount) ? $value->gift_voucher_amount : '' ?></td> 
                                            <td>
                                                <div class='d-flex'>
                                                    <a href='<?= base_url() ?>Voucher/printPdfGiftVoucher/<?= $value->transaction_id ?>' target="_blank" class='btn btn-icon btn-sm hover-effect-dot btn-outline-info mr-2' title='PDF' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'>
                                                        <i class="fal fa-file-pdf"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('Voucher/deleteVoucher') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>
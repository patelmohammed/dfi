<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function dateprint($dt = '') {
    if ($dt == '') {
        return date('d-m-Y');
    } else {
        return date('d-m-Y', strtotime($dt));
    }
}

function dbdate($dt = '') {
    if ($dt == '') {
        return date('Y-m-d');
    } else {
        $old = explode('-', $dt);
        return date('Y-m-d', strtotime($old[2] . '-' . $old[1] . '-' . $old[0]));
    }
}

function preprint($val, $flag = TRUE) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';

    if ($flag) {
        die;
    }
}

function getSuffixById($key) {
    $suffix = array(
        '1' => 'Mr. ',
        '2' => 'Miss. ',
        '3' => 'Mrs. ',
        '4' => 'Ms. ',
        '5' => 'Dr. '
    );
    return $suffix[$key];
}

function getMonthName($key) {
    $month = array(
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
    );
    return $month[$key];
}

function cleanString($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
    return preg_replace('/-+/', '-', $string);
}

function set_selected($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' selected="selected"';
    }
}

function set_checked($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' checked="checked"';
    }
}

function generator($lenth, $table_name = '', $field_name = '') {
    $CI = & get_instance();
    $CI->load->model('Auth_model');

    $number = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "N", "M", "O", "P", "Q", "R", "S", "U", "V", "T", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    for ($i = 0; $i < $lenth; $i++) {
        $rand_value = rand(0, 34);
        $rand_number = $number["$rand_value"];

        if (empty($con)) {
            $con = $rand_number;
        } else {
            $con = "$con" . "$rand_number";
        }
    }

    $existing_id = $CI->Auth_model->check_existing_generator_id($table_name, $field_name, $con);

    if (isset($existing_id) && !empty($existing_id)) {
        $this->generator(10, $table_name, $field_name);
    } else {
        return $con;
    }
}

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return (($Rupees ? $Rupees . '' : '') . $paise) . ' only';
}

function getFuzzy($str_time) {
    $_time_formats = array(
        array(60, 'just now'),
        array(90, '1 minute'),
        array(3600, 'minutes', 60),
        array(5400, '1 hour'),
        array(86400, 'hours', 3600),
        array(129600, '1 day'),
        array(604800, 'days', 86400),
        array(907200, '1 week'),
        array(2628000, 'weeks', 604800),
        array(3942000, '1 month'),
        array(31536000, 'months', 2628000),
        array(47304000, '1 year'),
        array(3153600000, 'years', 31536000)
    );
    $diff = time() - $str_time;
    $val = '';
    if ($str_time <= 0) {
        $val = 'a long time ago';
    } else if ($diff < 0) {
        $val = 'in the future';
    } else {
        foreach ($_time_formats as $format) {
            if ($diff < $format[0]) {
                if (count($format) == 2) {
                    $val = $format[1] . ($format[0] === 60 ? '' : ' ago');
                    break;
                } else {
                    $val = ceil($diff / $format[2]) . ' ' . $format[1] . ' ago';
                    break;
                }
            }
        }
    }
    return $val;
}

function getCityNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT * FROM all_cities WHERE id = '$id'";
    return $CI->db->query($sql)->row();
}

function getStateNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT * FROM all_states WHERE id = '$id'";
    return $CI->db->query($sql)->row();
}

function getUserState($id) {
    $CI = & get_instance();
    $sql = "SELECT oi.state AS state FROM outlet_information oi WHERE oi.outlet_id = '$id' ";
    if (is_numeric($id)) {
        $sql .= " UNION( SELECT ci.customer_state AS state FROM customer_information ci WHERE ci.customer_id = '$id')";
    }
    $sql .= " UNION( SELECT si.state AS state FROM supplier_information si WHERE si.supplier_id = '$id')";
    $result = $CI->db->query($sql)->row();

    return isset($result->state) && !empty($result->state) ? $result->state : '';
}

function getCountryCode($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(phonecode, 0) AS phonecode
            FROM tbl_country c 
            WHERE c.id = '$id' ";
    $phonecode = $CI->db->query($sql)->row();
    return isset($phonecode->phonecode) && !empty($phonecode->phonecode) ? $phonecode->phonecode : 0;
}

function getCountry($id) {
    $CI = & get_instance();
    $sql = "SELECT * 
            FROM tbl_country c 
            WHERE c.id = '$id' ";
    $phonecode = $CI->db->query($sql)->row();
    return isset($phonecode) && !empty($phonecode) ? $phonecode : '';
}

function getClientName($id) {
    $CI = & get_instance();
    $sql = "SELECT ci.client_name 
            FROM tbl_client_info ci 
            WHERE ci.client_id = '$id' ";
    $name = $CI->db->query($sql)->row();
    return isset($name->client_name) && !empty($name->client_name) ? $name->client_name : '';
}

function getAgencyName($id) {
    $CI = & get_instance();
    $sql = "SELECT ai.agency_name 
            FROM tbl_agency_info ai  
            WHERE ai.agency_id = '$id' ";
    $name = $CI->db->query($sql)->row();
    return isset($name->agency_name) && !empty($name->agency_name) ? $name->agency_name : '';
}

function getUserName($id) {
    $CI = & get_instance();
    $sql = "SELECT ci.user_name 
            FROM tbl_user_info ci 
            WHERE ci.user_id = '$id' ";
    $name = $CI->db->query($sql)->row();
    return isset($name->user_name) && !empty($name->user_name) ? $name->user_name : '';
}

function process_digital_signature_image($partBase64, $path) {
    $data = array();
    if (empty($partBase64)) {
        return false;
    }

    _maybe_create_upload_path($path);
    $filename = unique_filename($path, 'signature.png');

    $decoded_image = base64_decode($partBase64);

    $data['retval'] = false;

    $path = rtrim($path, '/') . '/' . $filename;

    $fp = fopen($path, 'w+');

    if (fwrite($fp, $decoded_image)) {
        $data['retval'] = true;
        $data['filename'] = $filename;
        $data['filename_path'] = $path;
    }

    fclose($fp);

    return $data;
}

function _maybe_create_upload_path($path) {
    if (!is_dir($path)) {
        if (!mkdir($path, 0777, true)) {
            die('Not Created');
        }
    }
}

function unique_filename($dir, $filename) {
//    Separate the filename into a name and extension .
    $info = pathinfo($filename);
    $ext = !empty($info['extension']) ? '.' . $info['extension'] : '';
    $filename = sanitize_file_name($filename);
    $number = '';
    // Change '.ext' to lower case.
    if ($ext && strtolower($ext) != $ext) {
        $ext2 = strtolower($ext);
        $filename2 = preg_replace('|' . preg_quote($ext) . '$|', $ext2, $filename);
        // Check for both lower and upper case extension or image sub-sizes may be overwritten.
        while (file_exists($dir . "/$filename") || file_exists($dir . "/$filename2")) {
            $filename = str_replace([
                "-$number$ext",
                "$number$ext",
                    ], "-$new_number$ext", $filename);
            $filename2 = str_replace([
                "-$number$ext2",
                "$number$ext2",
                    ], "-$new_number$ext2", $filename2);
            $number = $new_number;
        }

        return $filename2;
    }
    while (file_exists($dir . "/$filename")) {
        if ('' == "$number$ext") {
            $filename = "$filename-" . ++$number;
        } else {
            $filename = str_replace([
                "-$number$ext",
                "$number$ext",
                    ], '-' . ++$number . $ext, $filename);
        }
    }

//    $filename = 'signature.png';
    return $filename;
}

function sanitize_file_name($filename) {
    $CI = & get_instance();
    $filename = $CI->security->sanitize_filename($filename);

    return $filename;
}

function redeeme_code_generat($length = 15) {
    $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $res = "";
    for ($i = 0; $i < $length; $i++) {
        $res .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $res;
}

function isJSON($string) {
    return is_string($string) && is_array(json_decode($string, true)) ? true : false;
}

<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getUserDataById($id) {
        $sql = "SELECT ui.user_id, ui.country_id, ui.user_number, ui.user_name, ui.user_email, ui.user_password, ui.profile_photo 
                FROM tbl_user_info ui
                WHERE ui.user_id = '$id' AND ui.del_status = 'Live'";
        return $this->db->query($sql)->row();
    }

    public function getClientDataById($id) {
        $sql = "SELECT ui.client_id AS user_id, ui.country_id, ui.client_number AS user_number, ui.client_name AS user_name, ui.client_email AS user_email, ui.client_password AS user_password, ui.profile_photo, '' AS user_role, ui.is_email_verified 
                FROM tbl_client_info ui
                WHERE ui.client_id = '$id' AND ui.del_status = 'Live'";
        return $this->db->query($sql)->row();
    }

    public function getOrderNotificationDetail($id, $noti_from) {
        if ($noti_from == 'staff') {
            $this->db->where('ref_client_id', $id)->where('noti_from', $noti_from)->update('sign_notification', array('is_read_client' => '0'));
        } else if ($noti_from == 'client') {
            $this->db->where('ref_staff_id', $id)->where('noti_from', $noti_from)->update('sign_notification', array('is_read_staff' => '0'));
        }

        $sql = "SELECT ono.*,ag.contract_value
                FROM sign_notification ono
                INNER JOIN tbl_contracts ag ON ag.agreement_id = ono.ref_agreement_id AND ag.del_status = 'Live'
                WHERE ono.is_deleted = 'Live' ";
        if ($noti_from == 'staff') {
            $sql .= " AND ono.ref_client_id = '$id' AND ono.noti_from = '$noti_from' ";
        } else if ($noti_from == 'client') {
            $sql .= " AND ono.ref_staff_id = '$id' AND ono.noti_from = '$noti_from' ";
        }
        return $this->db->query($sql)->result();
    }

    public function deleteNotification($id) {
        $this->db->where('id', $id)->update('sign_notification', array('is_deleted' => 'Deleted'));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getotificationCount($id, $noti_from) {
        $sql = "SELECT IFNULL(COUNT(ono.id), 0) AS notification_count
                FROM sign_notification ono
                WHERE  1 = 1 ";
        if ($noti_from == 'staff') {
            $sql .= " AND ono.is_read_client = '1' AND ono.ref_client_id = '$id' AND ono.noti_from = '$noti_from' ";
        } else if ($noti_from == 'client') {
            $sql .= " AND ono.is_read_staff = '1' AND ono.ref_staff_id = '$id' AND ono.noti_from = '$noti_from' ";
        }
        $notification_count = $this->db->query($sql)->row();
        return isset($notification_count->notification_count) && !empty($notification_count->notification_count) ? $notification_count->notification_count : 0;
    }

    public function agreementCount() {
        $sql = "SELECT COUNT(*) AS total_agreement, (SELECT COUNT(*) FROM tbl_contracts tc WHERE tc.del_status = 'Live' AND tc.not_visible_to_client = '0' AND tc.client = '$this->user_id' AND tc.signed = 0) AS total_pending_agreement, (SELECT COUNT(*) FROM tbl_contracts tc WHERE tc.del_status = 'Live' AND tc.not_visible_to_client = '0' AND tc.client = '$this->user_id' AND tc.signed = 1) AS total_signed_agreement
                FROM tbl_contracts tc
                WHERE tc.del_status = 'Live' AND tc.not_visible_to_client = '0' AND tc.client = '$this->user_id'";
        return $this->db->query($sql)->row();
    }

    public function voucherCount() {
        $sql = "SELECT COUNT(*) AS total_voucher, (SELECT COUNT(*) FROM tbl_voucher  tv1 WHERE tv1.del_status = 'Live' AND tv1.client_id = '$this->user_id' AND tv1.voucher_status = 'Pending') AS total_pending, (SELECT COUNT(*) FROM tbl_voucher  tv1 WHERE tv1.del_status = 'Live' AND tv1.client_id = '$this->user_id' AND tv1.voucher_status = 'Approved') AS total_approved, (SELECT COUNT(*) FROM tbl_voucher  tv1 WHERE tv1.del_status = 'Live' AND tv1.client_id = '$this->user_id' AND tv1.voucher_status = 'Rejected') AS total_rejected
                FROM tbl_voucher tv 
                WHERE tv.del_status = 'Live' AND tv.client_id = '$this->user_id'";
        return $this->db->query($sql)->row();
    }

    public function getAttendanceData() {
        $sql = "SELECT IFNULL(COUNT(*), 0) AS today_attendance
                FROM attendance a 
                WHERE a.ref_staff_id = $this->user_id AND DATE_FORMAT(attendance_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')";
        return $this->db->query($sql)->row();
    }

    public function getPaymentInfoForPieChart() {
        $sql = "SELECT IFNULL(SUM(tc.contract_value), 0) AS AgreementTotal, 
                       (SELECT IFNULL(SUM(cost_value), 0) FROM tbl_cost tco INNER JOIN tbl_client_info tci ON tci.client_id = tco.ref_client_id AND tci.del_status = 'Live' WHERE tco.del_status = 'Live') AS TotalCost, 
                       (SELECT IFNULL(SUM(tv.voucher_amount), 0) FROM tbl_voucher tv WHERE tv.del_status = 'Live' AND tv.voucher_status = 'Approved') AS TotalVoucher, 
                       (SELECT IFNULL(SUM(tgv.gift_voucher_amount), 0) FROM tbl_gift_voucher tgv INNER JOIN tbl_client_info tci ON tci.client_id = tgv.client_id AND tci.del_status = 'Live' WHERE tgv.del_status = 'Live') AS TotalGiftVoucher 
                FROM tbl_contracts tc 
                INNER JOIN tbl_client_info tci ON tci.client_id = tc.client AND tci.del_status = 'Live'
                WHERE tc.del_status = 'Live' ";
        return $this->db->query($sql)->row();
    }

}

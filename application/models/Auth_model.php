<?php

class Auth_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getUserByEmail($username) {
        $sql = "SELECT ui.user_id AS  user_id, ui.user_password AS password, ui.user_name AS name, ui.user_email AS email, ui.user_type, user_role, ui.country_id, ui.user_number AS contact_number, ui.profile_photo 
                FROM tbl_user_info ui 
                WHERE ui.status = 1 AND ui.del_status = 'Live' AND (ui.user_email = '$username' OR ui.user_number = '$username') LIMIT 1 ";
        return $this->db->query($sql)->row();
    }

    function getUserByEmailClient($username) {
        $sql = "SELECT ui.client_id AS user_id, ui.client_password AS password, ui.client_name AS name, ui.client_email AS email, '' AS user_role, ui.is_message_verified, ui.country_id, ui.client_number AS contact_number, ui.is_email_verified, ui.email_token, ui.company_name, ui.profile_photo 
                FROM tbl_client_info ui 
                WHERE ui.status = 1 AND ui.del_status = 'Live' AND (ui.client_email = '$username' OR ui.client_number = '$username') LIMIT 1 ";
        return $this->db->query($sql)->row();
    }

    public function check_existing_generator_id($table_name, $field_name, $value) {
        $existing_id = '';
        if ($table_name != '' && $field_name != '') {
            $this->db->select($field_name);
            $this->db->from($table_name);
            $this->db->where($field_name, "$value");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $existing_id = $query->row();
            } else {
                $existing_id = '';
            }
        }

        return $existing_id;
    }

    function get_assigned_menu($user_id, $role, $access_for) {
        if ($role == 'Admin') {
            $sql = "SELECT mm.*, 1 AS full_access, 1 AS view_right, 1 AS add_right, 1 AS edit_right, 1 AS delete_right
                FROM tbl_side_menu mm
                WHERE mm.del_status = 'Live' AND mm.menu_for = '$access_for' 
                ORDER BY ref_menu_id ASC , menu_order_no ASC";
        } else {
            $sql = "SELECT mm.*, full_access, umr.view_right, umr.add_right, umr.edit_right, umr.delete_right
                FROM tbl_side_menu mm
                INNER JOIN tbl_side_menu_rights umr ON umr.ref_menu_id = mm.menu_id AND umr.access_for = '$access_for' 
                WHERE umr.ref_user_id = '$user_id' AND mm.del_status = 'Live' AND mm.menu_for = '$access_for' 
                UNION
                (SELECT *, '1' AS full_access,'1' AS view_right,'1' AS add_right,'1' AS edit_right,'1' AS delete_right 
                FROM tbl_side_menu 
                WHERE menu_id IN 
                (SELECT DISTINCT mm1.ref_menu_id FROM tbl_side_menu mm1
                INNER JOIN tbl_side_menu_rights umr1 ON umr1.ref_menu_id = mm1.menu_id AND umr1.access_for = '$access_for' 
                WHERE umr1.ref_user_id = '$user_id' AND mm1.ref_menu_id > 0 AND mm1.menu_for = '$access_for') AND del_status = 'Live') 
                ORDER BY ref_menu_id ASC , menu_order_no ASC";
        }

        $str = $this->db->query($sql);
        return $str->result();
    }

    public function getMenuMSTData($menu_for) {
        $sql = "SELECT * FROM tbl_side_menu tm 
                WHERE tm.ref_menu_id IS NULL AND del_status = 'Live' AND tm.menu_for = '$menu_for' 
                ORDER BY tm.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_add_sub_menu($value->menu_id, $menu_for);
                }
            }
        }
        return $res;
    }

    function get_add_sub_menu($MenuID, $menu_for) {
        $sql = "SELECT * FROM tbl_side_menu m 
                WHERE m.ref_menu_id = '" . $MenuID . "' AND del_status = 'Live' AND m.menu_for = '$menu_for' 
                ORDER BY m.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_add_sub_menu($value->menu_id, $menu_for);
                }
            }
        }
        return $res;
    }

    public function insertSideMenuAccess($user_menu_data, $user_id) {
        if (!empty($user_menu_data)) {
            foreach ($user_menu_data as $ke => $val) {
                $user_menu_data[$ke]['ref_user_id'] = $user_id;
                $this->db->insert('tbl_side_menu_rights', $user_menu_data[$ke]);
            }
        }
    }

    function getMenuAccessById($ref_user_id, $access_for) {
        $sql = "SELECT * FROM tbl_side_menu tm 
                LEFT JOIN tbl_side_menu_rights um ON um.ref_menu_id = tm.menu_id AND um.ref_user_id = $ref_user_id AND um.access_for = '$access_for' 
                WHERE tm.ref_menu_id IS NULL AND tm.del_status = 'Live' AND tm.menu_for = '$access_for' 
                GROUP BY tm.menu_id 
                ORDER BY tm.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_edit_sub_menu($value->menu_id, $ref_user_id, $access_for);
                }
            }
        }
        return $res;
    }

    function get_edit_sub_menu($MenuID, $ref_user_id, $access_for) {
        $sql = "SELECT * FROM tbl_side_menu m 
                LEFT JOIN tbl_side_menu_rights um ON um.ref_menu_id = m.menu_id AND um.ref_user_id = $ref_user_id AND um.access_for = '$access_for' 
                WHERE m.ref_menu_id = '" . $MenuID . "' AND m.del_status = 'Live' AND m.menu_for = '$access_for' 
                GROUP BY m.menu_id
                ORDER BY m.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_edit_sub_menu($value->menu_id, $ref_user_id, $access_for);
                }
            }
        }
        return $res;
    }

    public function updateSideMenuAccess($user_menu_data, $user_id, $access_for) {
        $this->db->where('ref_user_id', $user_id)->where('access_for', $access_for)->delete('tbl_side_menu_rights');
        if (!empty($user_menu_data)) {
            foreach ($user_menu_data as $ke => $val) {
                $user_menu_data[$ke]['ref_user_id'] = $user_id;
                $this->db->insert('tbl_side_menu_rights', $user_menu_data[$ke]);
            }
        }
    }

    public function send_otp_message($contact_number, $email_address = NULL) {
        $curl = curl_init();
        $url = "https://api.msg91.com/api/v5/otp?authkey=341380A94flRwVU5f5b3930P1";
        if (isset($email_address) && !empty($email_address)) {
            $url .= "&email=" . $email_address;
        }
        $url .= "&mobile=" . $contact_number . "&template_id=5f6302e863583b5a106feb47";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if (isset($response) && !empty($response)) {
            $insert_data = array();
            $insert_data['contact_number'] = $contact_number;
            $insert_data['message'] = 'OTP Message';
            $insert_data['response'] = $response;
            $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
            $this->db->insert('tbl_sms_history', $insert_data);
        }

        return $response;
    }

    public function resend_otp($contact_number) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v5/otp/retry?authkey=341380A94flRwVU5f5b3930P1&mobile=" . $contact_number . "&retrytype=voice",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if (isset($response) && !empty($response)) {
            $insert_data = array();
            $insert_data['contact_number'] = $contact_number;
            $insert_data['message'] = 'Resend OTP';
            $insert_data['response'] = $response;
            $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
            $this->db->insert('tbl_sms_history', $insert_data);
        }

        return $response;
    }

    public function otp_verify($contact_number, $otp) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v5/otp/verify?otp=" . $otp . "&authkey=341380A94flRwVU5f5b3930P1&mobile=" . $contact_number,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}

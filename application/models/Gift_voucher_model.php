<?php

class Gift_voucher_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAgreementByClient($client_id) {
        $sql = "SELECT * 
                FROM tbl_contracts tc 
                WHERE tc.del_status = 'Live' AND tc.client = '$client_id' ";
        return $this->db->query($sql)->result();
    }

    public function getGiftVoucher() {
        $sql = "SELECT tv.*, pm.payment_name, tc.subject 
                FROM tbl_gift_voucher tv 
                LEFT JOIN tbl_payment_method pm ON pm.payment_id = tv.ref_payment_id AND pm.del_status = 'Live' 
                LEFT JOIN tbl_contracts tc ON tc.agreement_id = tv.agreement_id AND tc.del_status = 'Live' 
                WHERE tv.del_status = 'Live' ";
        if ($this->user_type == 'client') {
            $sql .= " AND tv.client_id = '$this->user_id' ";
        }
        $sql .= " GROUP BY tv.gift_voucher_id";
        return $this->db->query($sql)->result();
    }

    public function getStaffForClient() {
        $sql = "SELECT ui.* 
                FROM tbl_user_info ui 
                WHERE ui.del_status = 'Live' AND ui.user_id != 1 ";
        return $this->db->query($sql)->result();
    }

    public function getGiftVoucherDataById($transaction_id) {
        $sql = "SELECT tv.*, IF(tv.gift_voucher_for = 'client', tc.client_name, ta.agency_name) AS client_name, IF(tv.gift_voucher_for = 'client', tc.client_id, ta.agency_id) AS client_id 
                FROM tbl_gift_voucher tv 
                LEFT JOIN tbl_client_info tc ON tc.client_id = tv.client_id 
                LEFT JOIN tbl_agency_info ta ON ta.agency_id = tv.agency_id 
                WHERE tv.transaction_id = '$transaction_id'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    public function selected_transaction_type($trans) {
        $this->db->select('*');
        $this->db->from('tbl_gift_voucher');
        $this->db->where('transaction_id', $trans);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    public function getDueChargesById($id, $Rec_Date, $type = 'client') {
        if (isset($type) && !empty($type) && $type == 'client') {
            $sql = "SELECT IFNULL(SUM(tc.contract_value), 0) AS AgreementTotal, IFNULL((SELECT SUM(tv.gift_voucher_amount) FROM tbl_gift_voucher tv WHERE tv.client_id = '$id' AND tv.gift_voucher_status = 'Approved' AND DATE_FORMAT(tv.datetime, '%Y-%m-%d %H:%i:%s') < '$Rec_Date' AND tv.del_status = 'Live'), 0) AS VoucherTotal
                    FROM tbl_client_info tci
                        LEFT JOIN tbl_contracts tc ON tc.client = tci.client_id AND tc.del_status = 'Live' 
                    WHERE tci.client_id = '$id' AND DATE_FORMAT(tc.agreement_date, '%Y-%m-%d %H:%i:%s') < '$Rec_Date'";
            $result = $this->db->query($sql);
            if ($data = $result->row()) {
                return $data;
            }
            return false;
        } else if (isset($type) && !empty($type) && $type == 'agency') {
            $sql = "SELECT 0 AS AgreementTotal, IFNULL((SELECT SUM(tv.gift_voucher_amount) FROM tbl_gift_voucher tv WHERE tv.agency_id = '$id' AND tv.gift_voucher_status = 'Approved' AND DATE_FORMAT(tv.datetime, '%Y-%m-%d %H:%i:%s') < '$Rec_Date' AND tv.del_status = 'Live'), 0) AS VoucherTotal
                    FROM tbl_agency_info ai
                    WHERE ai.agency_id = '$id' AND DATE_FORMAT(tc.agreement_date, '%Y-%m-%d %H:%i:%s') < '$Rec_Date'";
            $result = $this->db->query($sql);
            if ($data = $result->row()) {
                return $data;
            }
            return false;
        }
        return false;
    }

}

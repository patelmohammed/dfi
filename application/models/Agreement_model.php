<?php

class Agreement_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getClient() {
        $sql = "SELECT ci.* 
                FROM tbl_client_info ci 
                WHERE ci.del_status = 'Live' AND ci.is_message_verified = '1'";
        return $this->db->query($sql)->result();
    }

    public function getAgreement() {
        $sql = "SELECT *, IFNULL((SELECT COUNT(*) FROM tbl_agreement_document WHERE ref_agreement_id = tc.agreement_id AND del_status = 'Live'), 0) AS agreement_document_cnt 
                FROM tbl_contracts tc 
                WHERE tc.del_status = 'Live'";
        return $this->db->query($sql)->result();
    }

    public function geClientAgreement() {
        $sql = "SELECT *, IFNULL((SELECT COUNT(*) FROM tbl_agreement_document WHERE ref_agreement_id = tc.agreement_id AND is_active = 1 AND del_status = 'Live'), 0) AS agreement_document_cnt 
                FROM tbl_contracts tc 
                WHERE tc.del_status = 'Live' AND tc.not_visible_to_client = '0' AND tc.client = '$this->user_id'";
        return $this->db->query($sql)->result();
    }

    public function getAgreementbyId($id, $hash) {
        $sql = "SELECT tc.* 
                FROM tbl_contracts tc 
                WHERE tc.del_status = 'Live' AND tc.agreement_id = '$id' AND tc.hash = '$hash' AND tc.not_visible_to_client = '0' ";
        return $this->db->query($sql)->row();
    }

    public function getAgreementDocument($agreement_id = NULL) {
        $sql = "SELECT ad.*, tc.subject 
                FROM tbl_agreement_document ad
                INNER JOIN tbl_contracts tc ON tc.agreement_id = ad.ref_agreement_id AND tc.del_status = 'Live' 
                WHERE ad.del_status = 'Live' ";
        if (isset($agreement_id) && !empty($agreement_id)) {
            $sql .= " AND ad.ref_agreement_id = '$agreement_id' ";
        }
        return $this->db->query($sql)->result();
    }

    public function getAgreementDocumentClient($agreement_id = NULL) {
        $sql = "SELECT ad.*, tc.subject 
                FROM tbl_agreement_document ad
                INNER JOIN tbl_contracts tc ON tc.agreement_id = ad.ref_agreement_id AND tc.del_status = 'Live'
                WHERE ad.del_status = 'Live' AND ad.is_active = '1' AND tc.client = '$this->user_id' ";
        if (isset($agreement_id) && !empty($agreement_id)) {
            $sql .= " AND ad.ref_agreement_id = '$agreement_id' ";
        }
        return $this->db->query($sql)->result();
    }

}

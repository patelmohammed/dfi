<?php

class Common_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertInformation($data, $table_name) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function updateInformation($data, $id, $table_name) {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
    }

    public function updateInformation2($data, $column_name, $id, $table_name, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status == 'Live') {
            $this->db->where('del_status', 'Live');
        }

        $this->db->where($column_name, $id);
        if (!empty($column_name_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }

        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function deleteStatusChange($id, $table_name, $column_name) {
        $this->db->set('del_status', "Deleted");
        $this->db->where($column_name, $id);
        $this->db->update($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecord($id, $table_name, $column_name, $condition = NULL) {
        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecordUpdateStatus($id, $table_name, $column_name, $condition = NULL, $status = NULL) {
        if (isset($status) && !empty($status)) {
            $set = explode('||', $status);
            $this->db->set($set[0], $set[1]);
        }

        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }

        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataById($table_id, $column_name, $id, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND del_status = '$status'";
        } else {
            $sql .= " AND del_status = 'Live'";
        }
        if ((isset($column_name_2) && !empty($column_name_2)) && (isset($id_2) && !empty($id_2))) {
            $sql .= " AND $column_name_2 = '$id_2'";
        }
        return $this->db->query($sql)->result();
    }

    public function getDataById2($table_id, $column_name, $id, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            if ($status == '1') {
                $sql .= " AND status = '1'";
            } elseif ($status == 'Live') {
                $sql .= " AND del_status = 'Live'";
            }
        }
        if ((isset($column_name_2) && !empty($column_name_2)) && (isset($id_2) && !empty($id_2))) {
            $sql .= " AND $column_name_2 = '$id_2'";
        }
        return $this->db->query($sql)->row();
    }

    public function getDataByIdStatus($table_id, $column_name, $id, $status = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = $id ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status";
        } else {
            $sql .= " AND status = '1'";
        }
        return $this->db->query($sql)->row();
    }

    public function geAlldata($table_name) {
        $sql = "SELECT * FROM $table_name WHERE del_status = 'Live'";
        return $this->db->query($sql)->result();
    }

    public function geAlldataById($table_name, $column_name_1 = NULL, $id_1 = NULL, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status ";
        } else {
            $sql .= " AND del_status = 'Live'";
        }

        if (isset($column_name_1) && !empty($column_name_1) && isset($id_1) && !empty($id_1)) {
            $sql .= " AND $column_name_1 = '$id_1' ";
        }
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $sql .= " AND $column_name_2 = '$id_2' ";
        }
        return $this->db->query($sql)->result();
    }

    public function deleteInformation($column_name, $id, $table_name) {
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        return true;
    }

    public function getCompanyInformation() {
        $sql = "SELECT * FROM company_information";
        return $this->db->query($sql)->row();
    }

    public function generateNoWithCompanyCode($column_name, $table_name) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE status= '1' ";
        return $this->db->query($sql)->row()->Number;
    }

    public function getCompanyCode($column_name = 'company_code') {
        $sql = "SELECT IF($column_name != '', $column_name, company_code) AS company_code FROM company_information WHERE del_status = 'Live' LIMIT 1";
        $code = $this->db->query($sql)->row();
        return isset($code->company_code) && !empty($code->company_code) ? $code->company_code : '';
    }

    public function chkUniqueCode($table_name, $column_name, $code, $status = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$code");
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }
        if (!empty($status) && $status == 'Live') {
            $this->db->where('del_status', 'Live');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function chkUniqueData($table_name, $column_name, $value, $status = '', $column_name_2 = '', $value_2 = '', $status_2 = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$value");
        if (isset($status) && !empty($status)) {
            $this->db->where('del_status', 'Live');
        }

        if (isset($column_name_2) && !empty($column_name_2) && isset($value_2) && !empty($value_2)) {
            $this->db->where("$column_name_2", "$value_2");
        }
        if (isset($status_2) && !empty($status_2)) {
            if (!empty($status_2) && $status_2 == 1) {
                $this->db->where('status', '1');
            } else if (!empty($status_2) && $status_2 == 'Live') {
                $this->db->where('status', 'Live');
            }
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function generateNoIndividual($column_name, $table_name, $primary_id, $status = NULL) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $where = explode('||', $status);
            if (isset($where[0]) && isset($where[1]) && !empty($where[1]) && !empty($where[1])) {
                $sql .= " AND $where[0] = '$where[1]'";
            }
        }
        if (isset($primary_id) && !empty($primary_id)) {
            $sql .= " AND client = '$primary_id'";
        }
        return $this->db->query($sql)->row()->Number;
    }

    public function generateNoIndividualVoucher($column_name, $table_name, $primary_id = NULL, $primary_column = NULL, $status = NULL) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $where = explode('||', $status);
            if (isset($where[0]) && isset($where[1]) && !empty($where[1]) && !empty($where[1])) {
                $sql .= " AND $where[0] = '$where[1]'";
            }
        }
        if (isset($primary_id) && !empty($primary_id) && isset($primary_column) && !empty($primary_column)) {
            $sql .= " AND $primary_column = '$primary_id'";
        }
        return $this->db->query($sql)->row()->Number;
    }

    public function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80) {
        list($origWidth, $origHeight, $type) = getimagesize($sourceImage);

        if ($type == 1) {
            header('Content-Type: image/gif');
            $image = imagecreatefromgif($sourceImage);
        } elseif ($type == 2) {
            header('Content-Type: image/jpeg');
            $image = imagecreatefromjpeg($sourceImage);
        } elseif ($type == 3) {
            header('Content-Type: image/png');
            $image = imagecreatefrompng($sourceImage);
        } else {
            header('Content-Type: image/x-ms-bmp');
            $image = imagecreatefromwbmp($sourceImage);
        }

        if ($maxWidth == 0) {
            $maxWidth = $origWidth;
        }

        if ($maxHeight == 0) {
            $maxHeight = $origHeight;
        }

// Calculate ratio of desired maximum sizes and original sizes.
        $widthRatio = $maxWidth / $origWidth;
        $heightRatio = $maxHeight / $origHeight;

// Ratio used for calculating new image dimensions.
        $ratio = min($widthRatio, $heightRatio);

// Calculate new image dimensions.
        $newWidth = (int) $origWidth * $ratio;
        $newHeight = (int) $origHeight * $ratio;

// Create final image with new dimensions.
// if($type==1 or $type==3)
// {
//    $newImage = imagefill($newImage,0,0,0x7fff0000);
// }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);

        $transparent = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
        imagefill($newImage, 0, 0, $transparent);
        imagesavealpha($newImage, true);


        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
        imagepng($newImage, $targetImage);

// Free up the memory.
        imagedestroy($image);
        imagedestroy($newImage);

        return true;
    }

    public function check_menu_access($constant, $type) {
        $data = array();
        $user_assigned_menus = $this->session->userdata('side_menu');
        $return = false;
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    if ($value->view_right >= 1 && $type == 'VIEW') {
                        $return = TRUE;
                    }
                    if ($value->add_right >= 1 && $type == 'ADD') {
                        $return = TRUE;
                    }
                    if ($value->edit_right >= 1 && $type == 'EDIT') {
                        $return = TRUE;
                    }
                    if ($value->delete_right >= 1 && $type == 'DELETE') {
                        $return = TRUE;
                    }
                }
            }
        }
        if (!$return) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(['res' => 'error', 'result' => 'error', 'msg' => 'Unauthorized access']);
                die;
            } else {
                redirect('admin/Auth/Unauthorized');
            }
        }
        return $return;
    }

    public function get_menu_rights($constant) {
        $user_assigned_menus = $this->session->userdata('side_menu');
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    return (array) $value;
                }
            }
        } else {
            redirect('admin/Auth/Unauthorized');
        }
    }

    public function getEmailTemplate($type) {
        $sql = "SELECT mt.* 
                FROM tbl_mail_template mt 
                WHERE mt.type = '$type' AND mt.del_status = 'Live' AND mt.active = 1 ";
        return $this->db->query($sql)->row();
    }

    public function getSmsTemplate($type) {
        $sql = "SELECT mt.* 
                FROM tbl_sms_template mt 
                WHERE mt.template_for = '$type' AND mt.del_status = 'Live' AND mt.is_active = 1 ";
        return $this->db->query($sql)->row();
    }

    public function insertClientSideMenuRight($id) {
        $sql = "INSERT INTO tbl_side_menu_rights 
               (id, ref_user_id, ref_menu_id, full_access, view_right, add_right, edit_right, delete_right, access_for) 
                VALUES (NULL, '$id', '5', '1', '1', '1', '1', '1', 'customer'), 
                       (NULL, '$id', '10', '0', '1', '0', '0', '0', 'customer'), 
                       (NULL, '$id', '18', '1', '1', '1', '1', '1', 'customer'), 
                       (NULL, '$id', '19', '1', '1', '1', '1', '1', 'customer'), 
                       (NULL, '$id', '23', '0', '1', '0', '0', '0', 'customer'), 
                       (NULL, '$id', '33', '0', '1', '0', '0', '0', 'customer') ";
        $this->db->query($sql);
    }

    public function send_message($contact_number, $message, $country_id = NULL) {
//        if (isset($country_id) && !empty($country_id)) {
//            $country_code = getCountryCode($country_id);
//        } else {
//            $country_code = '91';
//        }
//        $url = "https://api.msg91.com/api/sendhttp.php?authkey=341380A94flRwVU5f5b3930P1&mobiles=" . $contact_number . "&country=" . $country_code . "&message=" . $message . "&sender=WEBOIT&route=4";
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_HTTPHEADER => array(
//                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        curl_close($curl);
//
//        if (isset($response) && !empty($response)) {
//            $insert_data = array();
//            $insert_data['contact_number'] = $country_code . ' ' . $contact_number;
//            $insert_data['message'] = urldecode($message);
//            $insert_data['response'] = $response;
//            $insert_data['InsUser'] = (isset($this->user_id) && !empty($this->user_id) ? $this->user_id : NULL);
//            $insert_data['InsTerminal'] = $this->input->ip_address();
//            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
//            $this->insertInformation($insert_data, 'tbl_sms_history');
//        }
//
//        return $response;
        return '';
    }

    public function getStaff() {
        $sql = "SELECT * 
                FROM tbl_user_info ui 
                WHERE ui.del_status = 'Live' AND ui.user_role IS NULL AND ui.user_type = 2 ";
        return $this->db->query($sql)->result();
    }

    public function getClientOrAgency($type, $id) {
        $result = array();
        if (isset($type) && !empty($type) && $type == 'client') {
            $sql = "SELECT ci.client_id As client_id, ci.client_name AS client_name, ci.company_name AS company_name, ci.country_id AS country_id, ci.client_number AS client_number, ci.client_email AS client_email, ci.client_address AS client_address, ci.client_city AS client_city, ci.client_state AS client_state, ci.client_pincode AS client_pincode, 'client' AS type 
                    FROM tbl_client_info ci 
                    WHERE ci.client_id = $id";
            $result = $this->db->query($sql)->row();
        } else if (isset($type) && !empty($type) && $type == 'agency') {
            $sql = "SELECT ai.agency_id As client_id, ai.agency_name AS client_name, '' AS company_name, ai.country_id AS country_id, ai.agency_number AS client_number, ai.agency_email AS client_email, ai.agency_address AS client_address, ai.agency_city AS client_city, ai.agency_state AS client_state, ai.agency_pincode AS client_pincode, 'agency' AS type 
                    FROM tbl_agency_info ai 
                    WHERE ai.agency_id = $id";
            $result = $this->db->query($sql)->row();
        }
        return $result;
    }

}

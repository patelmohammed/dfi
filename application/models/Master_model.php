<?php

class Master_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkUserName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_name != '$currentName'" : '');
        $sql = "SELECT user_name FROM tbl_user_info 
                WHERE user_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserName($id) {
        $sql = "SELECT user_name FROM tbl_user_info
                WHERE del_status = 'Live'
                AND user_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_name;
    }

    public function checkUserEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_email != '$currentName'" : '');
        $sql = "SELECT user_email FROM tbl_user_info 
                WHERE user_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserEmail($id) {
        $sql = "SELECT user_email FROM tbl_user_info
                WHERE del_status = 'Live'
                AND user_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_email;
    }

    public function checkUserPhone($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_number != '$currentName'" : '');
        $sql = "SELECT user_number FROM tbl_user_info 
                WHERE user_number = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserPhone($id) {
        $sql = "SELECT user_number FROM tbl_user_info
                WHERE del_status = 'Live'
                AND user_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_number;
    }

    public function getCityByState($state_id) {
        $sql = "SELECT * FROM all_cities WHERE state_code = $state_id";
        return $this->db->query($sql)->result();
    }

    public function checkClientName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND client_name != '$currentName'" : '');
        $sql = "SELECT client_name FROM tbl_client_info 
                WHERE client_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getClientName($id) {
        $sql = "SELECT client_name FROM tbl_client_info
                WHERE del_status = 'Live'
                AND client_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->client_name;
    }

    public function checkcCientEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND client_email != '$currentName'" : '');
        $sql = "SELECT client_email FROM tbl_client_info 
                WHERE client_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getClientEmail($id) {
        $sql = "SELECT client_email FROM tbl_client_info
                WHERE del_status = 'Live'
                AND client_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->client_email;
    }

    public function checkcClientPhone($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND client_number != '$currentName'" : '');
        $sql = "SELECT client_number FROM tbl_client_info 
                WHERE client_number = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getClientPhone($id) {
        $sql = "SELECT client_number FROM tbl_client_info
                WHERE del_status = 'Live'
                AND client_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->client_number;
    }

    public function checkPaymentName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND payment_name != '$currentName'" : '');
        $sql = "SELECT payment_name FROM tbl_payment_method 
                WHERE payment_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getPaymentName($id) {
        $sql = "SELECT payment_name FROM tbl_payment_method
                WHERE del_status = 'Live'
                AND payment_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->payment_name;
    }

    public function getStaff() {
        $sql = "SELECT ui.*, (SELECT IFNULL(COUNT(*), 0) AS today_attendance FROM attendance a WHERE a.ref_staff_id = ui.user_id AND DATE_FORMAT(attendance_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')) AS today_attendance 
                FROM tbl_user_info ui 
                WHERE ui.user_role IS NULL AND ui.user_type = 2 AND ui.del_status = 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function checkAgencyName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND agency_name != '$currentName'" : '');
        $sql = "SELECT agency_name FROM tbl_agency_info 
                WHERE agency_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getAgencyName($id) {
        $sql = "SELECT agency_name FROM tbl_agency_info
                WHERE del_status = 'Live'
                AND agency_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->agency_name;
    }

    public function checkcAgencyEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND agency_email != '$currentName'" : '');
        $sql = "SELECT agency_email FROM tbl_agency_info 
                WHERE agency_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getAgencyEmail($id) {
        $sql = "SELECT agency_email FROM tbl_agency_info
                WHERE del_status = 'Live'
                AND agency_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->agency_email;
    }

    public function checkcAgencyPhone($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND agency_number != '$currentName'" : '');
        $sql = "SELECT agency_number FROM tbl_agency_info 
                WHERE agency_number = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getAgencyPhone($id) {
        $sql = "SELECT agency_number FROM tbl_agency_info
                WHERE del_status = 'Live'
                AND agency_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->agency_number;
    }

    public function checkThirdPartyName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND third_party_name != '$currentName'" : '');
        $sql = "SELECT third_party_name FROM tbl_third_party_info 
                WHERE third_party_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getThirdPartyName($id) {
        $sql = "SELECT third_party_name FROM tbl_third_party_info
                WHERE del_status = 'Live'
                AND third_party_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->third_party_name;
    }

    public function checkcThirdPartyPhone($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND third_party_number != '$currentName'" : '');
        $sql = "SELECT third_party_number FROM tbl_third_party_info 
                WHERE third_party_number = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getThirdPartyPhone($id) {
        $sql = "SELECT third_party_number FROM tbl_third_party_info
                WHERE del_status = 'Live'
                AND third_party_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->third_party_number;
    }

    public function getAgreement() {
        $sql = "SELECT tc.agreement_id, tc.agreement_no, tc.subject 
                FROM tbl_contracts tc 
                WHERE tc.del_status = 'Live'";
        return $this->db->query($sql)->result();
    }

}

<?php

class Cost_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getClient() {
        $sql = "SELECT ci.* 
                FROM tbl_client_info ci 
                WHERE ci.del_status = 'Live' AND ci.is_message_verified = '1'";
        return $this->db->query($sql)->result();
    }

    public function getCost($client_id = NULL) {
        $sql = "SELECT co.*, con.agreement_id, con.subject, con.hash 
                FROM tbl_cost co 
                LEFT JOIN tbl_contracts con ON con.agreement_id = co.ref_agreement_id AND con.del_status = 'Live' 
                WHERE co.del_status = 'Live' ";
        if (isset($client_id) && !empty($client_id)) {
            $sql .= " AND co.ref_client_id = $client_id ";
        }
        return $this->db->query($sql)->result();
    }

}
